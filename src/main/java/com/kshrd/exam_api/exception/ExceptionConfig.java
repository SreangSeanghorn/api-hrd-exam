package com.kshrd.exam_api.exception;

import org.springframework.dao.DataAccessException;
import org.springframework.http.ResponseEntity;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.MissingPathVariableException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.NoHandlerFoundException;

import javax.servlet.http.HttpServletRequest;
import java.sql.SQLException;
import java.sql.Timestamp;

@RestControllerAdvice
public class ExceptionConfig {

    @ExceptionHandler(Exception.class)
    public ResponseEntity<ApiErrorResponse> handleException(HttpServletRequest request, Exception e) {

        ApiErrorResponse response = new ApiErrorResponse();
        response.setTimestamp(new Timestamp(System.currentTimeMillis()));
        response.setStatus(500);
        response.setCode(Thread.currentThread().getStackTrace()[1].getLineNumber());
        response.setMessage(e.getMessage());
        if (response.getMessage().equals("source cannot be null"))
            response.setMessage("Path variable is wrong in this request. Please change it to your request.");
        return ResponseEntity.ok(response);

    }

    @ExceptionHandler(MissingPathVariableException.class)
    public ResponseEntity<ApiErrorResponse> handleMissingPathVariableException(HttpServletRequest request, MissingPathVariableException e) {

        ApiErrorResponse response = new ApiErrorResponse();
        response.setTimestamp(new Timestamp(System.currentTimeMillis()));
        response.setStatus(401);
        response.setCode(Thread.currentThread().getStackTrace()[1].getLineNumber());
        response.setMessage("Required path variable is missing in this request. Please add it to your request.");
        return ResponseEntity.ok(response);

    }

    @ExceptionHandler(NoHandlerFoundException.class)
    public ResponseEntity<ApiErrorResponse> handleNotFoundResourceException(HttpServletRequest request, NoHandlerFoundException e) {

        ApiErrorResponse response = new ApiErrorResponse();
        response.setTimestamp(new Timestamp(System.currentTimeMillis()));
        response.setStatus(404);
        response.setCode(Thread.currentThread().getStackTrace()[1].getLineNumber());
        response.setMessage("Requested resource wasn't found on the server");
        return ResponseEntity.ok(response);

    }


}