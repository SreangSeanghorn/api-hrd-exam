package com.kshrd.exam_api.exception;

import com.fasterxml.jackson.annotation.JsonFormat;

import java.sql.Timestamp;

public class ApiErrorResponse {

    @JsonFormat(pattern="yyyy-MM-dd")
    private Timestamp timestamp;
    private int status;
    private int code;
    private String message;

    public ApiErrorResponse() {
    }

    public ApiErrorResponse(Timestamp timestamp, int status, int code, String message) {
        this.timestamp = timestamp;
        this.status = status;
        this.code = code;
        this.message = message;
    }

    public Timestamp getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Timestamp timestamp) {
        this.timestamp = timestamp;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    @Override
    public String toString() {
        return "ApiErrorResponse{" +
                "timestamp=" + timestamp +
                ", status=" + status +
                ", code=" + code +
                ", message='" + message + '\'' +
                '}';
    }
}