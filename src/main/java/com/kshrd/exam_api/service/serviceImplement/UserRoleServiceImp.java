package com.kshrd.exam_api.service.serviceImplement;


import com.kshrd.exam_api.repository.model.RoleDto;
import com.kshrd.exam_api.repository.rep.UserRoleRepository;
import com.kshrd.exam_api.service.serviceInterface.UserRoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserRoleServiceImp implements UserRoleService {
    private UserRoleRepository userRoleRepository;
    @Autowired
    public void setUserRoleRepository(UserRoleRepository userRoleRepository) {
        this.userRoleRepository = userRoleRepository;
    }

    @Override
    public void updateRole(int teacher_id, int role_id, RoleDto roleDto) {
        userRoleRepository.updateRole(teacher_id,role_id, roleDto);
    }

    @Override
    public Boolean insertUserRole(int teacher_id, RoleDto roleDto) {
        Boolean isInserted=userRoleRepository.insertUserRole(teacher_id, roleDto);
        if(isInserted) return isInserted;
        else  return null;

    }

    @Override
    public Boolean deleteUserRole(int teacher_id, int role_id) {

        Boolean isDeleted=userRoleRepository.deleteUserRole(teacher_id,role_id);
        if(isDeleted) return isDeleted;
        else  return null;
    }
}
