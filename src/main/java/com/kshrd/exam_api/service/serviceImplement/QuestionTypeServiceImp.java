package com.kshrd.exam_api.service.serviceImplement;


import com.kshrd.exam_api.repository.model.QuestionDto;
import com.kshrd.exam_api.repository.model.QuestionTypeDto;
import com.kshrd.exam_api.repository.rep.QuestionTypeRepository;
import com.kshrd.exam_api.service.serviceInterface.QuestionTypeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class QuestionTypeServiceImp implements QuestionTypeService {

    private QuestionTypeRepository questionTypeRepository;

    @Autowired
    public QuestionTypeServiceImp(QuestionTypeRepository questionTypeRepository) {
        this.questionTypeRepository = questionTypeRepository;
    }

    @Override
    public List<QuestionTypeDto> findAll() {
        return questionTypeRepository.findAll();
    }

    @Override
    public Boolean insert(QuestionTypeDto questionTypeDto) {
        return questionTypeRepository.insert(questionTypeDto);
    }

    @Override
    public Boolean update(int id, QuestionTypeDto questionTypeDto) {
        return questionTypeRepository.update(id,questionTypeDto);
    }

    @Override
    public Boolean delete(int id) {
        return questionTypeRepository.delete(id);
    }

    @Override
    public QuestionTypeDto findOne(int id) {
        return questionTypeRepository.findOne(id);
    }
}
