package com.kshrd.exam_api.service.serviceImplement;


import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import com.kshrd.exam_api.configuration.JsonUploadFile;
import com.kshrd.exam_api.controller.request.*;

import com.kshrd.exam_api.controller.response.*;

import com.kshrd.exam_api.controller.response.BaseAPIResponse;
import com.kshrd.exam_api.controller.response.ResultReponse;

import com.kshrd.exam_api.controller.response.messages.BaseMessage;
import com.kshrd.exam_api.controller.response.messages.MessageProperties;
import com.kshrd.exam_api.controller.utils.Paging;
import com.kshrd.exam_api.repository.model.MultiAnswersDto;
import com.kshrd.exam_api.repository.model.OptionDto;
import com.kshrd.exam_api.repository.model.ResultDto;
import com.kshrd.exam_api.repository.model.SingleAnswerDto;
import com.kshrd.exam_api.repository.rep.QuizRepository;
import com.kshrd.exam_api.repository.rep.ResultRepository;
import com.kshrd.exam_api.service.serviceInterface.ResultService;
import net.minidev.json.parser.JSONParser;
import org.json.JSONObject;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.json.GsonJsonParser;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@Service
public class ResultServiceImp implements ResultService {

    private ResultRepository resultRepository;
    @Autowired
    QuizRepository quizRepository;

    QuizServiceImp quizServiceImp;

    @Autowired
    public void setResultRepository(ResultRepository resultRepository) {
        this.resultRepository = resultRepository;
    }

    @Override
    public Boolean insert(ResultRequestDto resultRequestDto) {
        boolean isInsert = resultRepository.Insert(resultRequestDto);
        if(isInsert)
            return isInsert;
        else
            return null;
    }

    @Override
    public List<ResultDto> select() {
        return resultRepository.select();
    }

    @Override
    public CheckingStudentResponse checkingResponse(int student_id, int quiz_id) {

        CheckingStudentResponse response=new CheckingStudentResponse();
        List<InstructionCheckingResponse> lists=resultRepository.selectInstruction(quiz_id);
        List<QuestionFilterResponse> res=new ArrayList<>();
        List<QuestionFilterResponse> res1;
        MultiAnswersDto multiAnswersDto;
        SingleAnswerDto singleAnswerDto;
        //response.setInstructionFilterResponses(lists);
        System.out.println("size of list:"+lists.size());
        List<QuestionFilterResponse> listAdd=new ArrayList<>();
        for(int j=0;j<lists.size();j++){
            res=quizRepository.selectQA(lists.get(j).getInstructionId());
            for(int k=0;k<res.size();k++){
                multiAnswersDto=quizRepository.selectMultiAnswer(res.get(k).getQuestionId());
                singleAnswerDto=quizRepository.selectSingleAnswer(res.get(k).getQuestionId());
                if((multiAnswersDto.getAnswer())!=null){
                    res.get(k).setAnswer(multiAnswersDto.getAnswer());
                    singleAnswerDto.setAnswer(null);
                }
                if((singleAnswerDto.getAnswer())!=null){
                    res.get(k).setAnswer(singleAnswerDto.getAnswer());
                }

            }
            if(res.isEmpty()){
                res1=quizRepository.selectQNoA(lists.get(j).getInstructionId());
                for(int x=0;x<res1.size();x++){
                    res.add(res1.get(x));
                }

            }
            listAdd.addAll(res);
            //System.out.println("listAdd"+listAdd);

        }
        List<ResultJsonContainer> resultStore=new ArrayList<>();
        JsonUploadFile file = new JsonUploadFile();
        List<ResultListResponse> responses=resultRepository.selectResult(student_id,quiz_id);
        ResultJsonContainer resultConvert = new ResultJsonContainer();
        ObjectMapper objectMapper = new ObjectMapper();
        List<QuestionFilterChecking> checking=new ArrayList<>();

        boolean isSelected = false;

        try {
            List<String> results = resultRepository.resultByStudentId(student_id,quiz_id);
            // System.out.println("result:"+results);
            if(results.size()==0){
                isSelected = false;
            }else{
                file.deleted();
                for(Object r : results){
                  //  System.out.println("res:"+r);
                    file.writeFile(r);

                }
                BufferedReader reader;
                try {
                    reader = new BufferedReader(new FileReader("filejson.txt"));
                    String line= reader.readLine();
                    while (line !=null){
                        resultConvert = objectMapper.readValue(line, ResultJsonContainer.class);
                        resultStore.add(resultConvert);
                        line = reader.readLine();
                    }
                    reader.close();
                } catch (FileNotFoundException e) {
                    System.out.println(e.getMessage());
                } catch (IOException e) {
                    System.out.println(e.getMessage());
                }

                for(int i=0;i<resultStore.size();i++){
                    //System.out.println("result:"+resultStore.get(i));
                    responses.get(i).setResult(resultStore.get(i));
                    response.setResultList(responses);
                    isSelected = true;
                }
            }
        }catch (Exception e){
            System.out.println(e.getMessage());
        }
        ModelMapper mapper=new ModelMapper();
        int count=0;
        int score=0;
        ResultListResponse resultRequest=new ResultListResponse();
      //  System.out.println("defaultScore:"+resultRepository.getScore(student_id,quiz_id));
        int defaultScore=resultRepository.getScore(student_id,quiz_id);
        int status=resultRepository.getStatus(student_id,quiz_id);
        for(int z=0;z<lists.size();z++){
           System.out.println("size of List:"+z);
            System.out.println("list:"+lists.get(z));
            //int id=response.getResultList().get(z).getResult().getFilterResponses().get(z).getInstructionId();
            List<ResultListResponse> rd=response.getResultList();
         //System.out.   println("result:"+rd);

            for (int h=0;h<rd.size();h++){
                resultRequest.setResult(rd.get(h).getResult());

                if(resultRequest.getStatus()==0){
                    List<InstructionFilterResponse> filterResponses=rd.get(h).getResult().getFilterResponses();
                    System.out.println("filter:"+filterResponses);
                    System.out.println();
                    for(int c=0;c<filterResponses.size();c++){
                        int instId=lists.get(z).getInstructionId();
                        System.out.println("c:"+c);
                        int id=filterResponses.get(h).getInstructionId();
                        List<QuestionFilterResponse> jsonContainer=filterResponses.get(c).getResponse();
                        Collections.sort(jsonContainer,new QuestionFilterResponse.OrderById());
                        Collections.sort(listAdd,new QuestionFilterResponse.OrderById());
                        System.out.println("after sorting:"+jsonContainer);
                        System.out.println("contianer["+c+"]:"+jsonContainer);
                        if(id==instId){

                            int size=jsonContainer.size();

                            for (int u=0;u<size;u++){
                                System.out.println("list in count["+count+"]"+listAdd.get(count));
                                System.out.println("json in u["+u+"]:"+jsonContainer.get(u));
                                System.out.println("u:"+u);
                                if(jsonContainer.get(u).getQuestionType().trim().equalsIgnoreCase("Question")
                                        ||jsonContainer.get(u).getQuestionType().trim().equalsIgnoreCase("Question and Answer")
                                        ||jsonContainer.get(u).getQuestionType().trim().equalsIgnoreCase("code")
                                        ||jsonContainer.get(u).getQuestionType().trim().equalsIgnoreCase("coding")
                                ){
                                    System.out.println("Coding or Question");
                                    count++;
                                }
                                else {
                                    System.out.println("list Id["+count+"]:"+listAdd.get(count).getQuestionId());
                                   System.out.println("resulst Id["+u+"]:"+jsonContainer.get(u).getQuestionId());
                                    if(listAdd.get(count).getQuestionId()==jsonContainer.get(u).getQuestionId()){
                                        System.out.println("type:"+jsonContainer.get(u).getQuestionType());
                                        jsonContainer.get(u).getQuestionType();
                                        if (jsonContainer.get(u).getQuestionType().trim().equalsIgnoreCase("multiple choice")
                                                ||(jsonContainer.get(u).getQuestionType().trim().equalsIgnoreCase("multiple choices")
                                                ||jsonContainer.get(u).getQuestionType().trim().equalsIgnoreCase("Mutliple Choice"))){


                                            OptionDto optionRes=mapper.map(jsonContainer.get(u).getAnswer(),OptionDto.class);
                                            OptionDto multiAnswer=mapper.map(listAdd.get(count).getAnswer(),OptionDto.class);
                                            System.out.println("Option1:"+optionRes.getOption1().getIsCorrect());
                                            System.out.println("Option2:"+optionRes.getOption2().getIsCorrect());
                                            System.out.println("Option3:"+optionRes.getOption3().getIsCorrect());
                                            System.out.println("Option4:"+optionRes.getOption4().getIsCorrect());

                                            System.out.println("m1:"+multiAnswer.getOption1().getIsCorrect());
                                            System.out.println("m2:"+multiAnswer.getOption2().getIsCorrect());
                                            System.out.println("multiAnswer3:"+multiAnswer.getOption3().getIsCorrect());
                                            System.out.println("multiAnswer4:"+multiAnswer.getOption4().getIsCorrect());

                                            if(optionRes.getOption1().getIsCorrect().equalsIgnoreCase(multiAnswer.getOption1().getIsCorrect())
                                                    &&optionRes.getOption2().getIsCorrect().equalsIgnoreCase(multiAnswer.getOption2().getIsCorrect())
                                                    &&optionRes.getOption3().getIsCorrect().equalsIgnoreCase(multiAnswer.getOption3().getIsCorrect())
                                                    &&optionRes.getOption4().getIsCorrect().equalsIgnoreCase(multiAnswer.getOption4().getIsCorrect())){
                                                score+=listAdd.get(count).getqPoint();
                                                jsonContainer.get(u).setPoint(listAdd.get(count).getqPoint());
                                                System.out.println("get score in mutli:"+score);
                                            }

                                        }
                                        else if(jsonContainer.get(u).getQuestionType().trim().equalsIgnoreCase("fill in the gap")
                                                ||jsonContainer.get(u).getQuestionType().trim().equalsIgnoreCase("Fill in the gaps")
                                        ||jsonContainer.get(u).getQuestionType_id()==3){
                                            System.out.println("Fill in the gap");

                                        }
                                        else {
                                            //AnswerObject answerObject=mapper.map(an,AnswerObject.class);
                                            String ans=jsonContainer.get(u).getAnswer().toString();;
                                            if(listAdd.get(count).getAnswer().toString().equalsIgnoreCase(ans)){
                                                score+=listAdd.get(count).getqPoint();
                                                System.out.println(score);
                                                jsonContainer.get(u).setPoint(listAdd.get(count).getqPoint());

                                            }
                                        }

                                    }
                                    count++;
                                }
                            }
                        }
                    }
                    response.getResultList().get(h).setStatus(1);
                    response.getResultList().get(h).setScore(score);

                    System.out.println("total score:"+score);
                    resultRequest.setScore(score);
                    resultRequest.setStatus(1);
                    resultRepository.updateResult(student_id,quiz_id,resultRequest);
                }
                else{
                    resultRequest.setScore(defaultScore);
                    resultRequest.setStatus(status);
                    resultRepository.updateResult(student_id,quiz_id,resultRequest);
                }

                }
           }



        return response;
    }

    @Override
    public CheckingStudentResponse selectDetailResult(int student_id,int quiz_id) {

        CheckingStudentResponse response=new CheckingStudentResponse();
        List<InstructionCheckingResponse> lists=resultRepository.selectInstruction(quiz_id);
        List<QuestionFilterResponse> res=new ArrayList<>();
        List<QuestionFilterResponse> res1;
        MultiAnswersDto multiAnswersDto;
        SingleAnswerDto singleAnswerDto;
        //response.setInstructionFilterResponses(lists);
        System.out.println("size of list:"+lists.size());
        List<QuestionFilterResponse> listAdd=new ArrayList<>();
        for(int j=0;j<lists.size();j++){
            res=quizRepository.selectQA(lists.get(j).getInstructionId());
            for(int k=0;k<res.size();k++){
                multiAnswersDto=quizRepository.selectMultiAnswer(res.get(k).getQuestionId());
                singleAnswerDto=quizRepository.selectSingleAnswer(res.get(k).getQuestionId());
                if((multiAnswersDto.getAnswer())!=null){
                    res.get(k).setAnswer(multiAnswersDto.getAnswer());
                    singleAnswerDto.setAnswer(null);
                }
                if((singleAnswerDto.getAnswer())!=null){
                    res.get(k).setAnswer(singleAnswerDto.getAnswer());
                }

            }
            if(res.isEmpty()){
                res1=quizRepository.selectQNoA(lists.get(j).getInstructionId());
                for(int x=0;x<res1.size();x++){
                    res.add(res1.get(x));
                }
            }
            listAdd.addAll(res);
            //System.out.println("listAdd"+listAdd);

        }
        List<ResultJsonContainer> resultStore=new ArrayList<>();
        JsonUploadFile file = new JsonUploadFile();
        List<ResultListResponse> responses=resultRepository.selectResult(student_id,quiz_id);
        ResultJsonContainer resultConvert = new ResultJsonContainer();
        ObjectMapper objectMapper = new ObjectMapper();

        boolean isSelected = false;

        try {
            List<String> results = resultRepository.resultByStudentId(student_id,quiz_id);
            // System.out.println("result:"+results);
            if(results.size()==0){
                isSelected = false;
            }else{
                file.deleted();
                for(Object r : results){
                    //  System.out.println("res:"+r);
                    file.writeFile(r);

                }
                BufferedReader reader;
                try {
                    reader = new BufferedReader(new FileReader("filejson.txt"));
                    String line= reader.readLine();
                    while (line !=null){
                        resultConvert = objectMapper.readValue(line, ResultJsonContainer.class);
                        resultStore.add(resultConvert);
                        line = reader.readLine();
                    }
                    reader.close();
                } catch (FileNotFoundException e) {
                    System.out.println(e.getMessage());
                } catch (IOException e) {
                    System.out.println(e.getMessage());
                }

                for(int i=0;i<resultStore.size();i++){
                    //System.out.println("result:"+resultStore.get(i));
                    responses.get(i).setResult(resultStore.get(i));
                    response.setResultList(responses);
                    isSelected = true;
                }
            }
        }catch (Exception e){
            System.out.println(e.getMessage());
        }


        return response;

    }

    @Override
    public List<String> result() {
        return resultRepository.result();
    }

    @Override
    public ResultDto selectByResultId(int id) {
        return resultRepository.selectByResultId(id);
    }

    @Override
    public String GetResultById(int id) {
        return resultRepository.GetResultById(id);
    }

    @Override
    public Boolean UpdateResult(int student_id,int quiz_id, ResultCheckingRequestDto requestDto) {
        System.out.println("in service:"+student_id+quiz_id+requestDto);
        boolean isUpdated = resultRepository.UpdateResult(student_id,quiz_id,requestDto);
        if(isUpdated)
            return isUpdated;
        else
            return null;
    }

    @Override
    public int DeleteResult(int id) {
            return resultRepository.DeleteResult(id);
    }

    @Override
    public List<ResultDto> SelectByClassname(String Classname)
    {

        return resultRepository.SelectByClassname(Classname);
    }

    @Override
    public List<String> GetResultByClassname(String Classname) {

        return resultRepository.GetResultByClassname(Classname);
    }

    @Override
    public ResultDto getResultByStudentID(int student_id,int quiz_id) {
       return resultRepository.getResultByStudentID(student_id,quiz_id);

    }

    @Override
    public List<ResultDto> getResultByClass(int quiz_id, String class_name) {
        System.out.println("in service:"+resultRepository.getResultByClass(quiz_id,class_name).get(0).getStatus());
        return resultRepository.getResultByClass(quiz_id,class_name);
    }

    @Override
    public List<ResultDto> selectResultByQuiz(int quiz_id) {
        return resultRepository.selectResultByQuiz(quiz_id);
    }

    @Override
    public List<ResultReponse> selectAssignQuiz(Paging page) {
        page.setTotalCount(resultRepository.selectCountQuizRecord());
        List<ResultReponse> resultReponses=resultRepository.selectAssignQuiz(page);
        return resultReponses;
    }

    @Override

    public Boolean insertResult(ResultRequest results) {
        System.out.println("result in service:" + results);
        return resultRepository.insertResult(results);
    }
    @Override
    public boolean updateScore(int id, ScoreRequest scoreRequest) {
        scoreRequest.setStatus(true);
        return resultRepository.updateScore(id,scoreRequest);
    }


}
