package com.kshrd.exam_api.service.serviceImplement;


import com.kshrd.exam_api.controller.response.RoleResponse;
import com.kshrd.exam_api.repository.model.RoleDto;
import com.kshrd.exam_api.repository.rep.RoleRepository;
import com.kshrd.exam_api.service.serviceInterface.RoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class RoleServiceImp implements RoleService {
    private RoleRepository roleRepository;
    @Autowired
    public void setRoleRepository(RoleRepository roleRepository) {
        this.roleRepository = roleRepository;
    }

    @Override
    public Boolean insertRole(RoleDto roleDto) {
        Boolean isInserted=roleRepository.insertRole(roleDto);
        if(isInserted)
            return isInserted;
        else
            return null;
    }

    @Override
    public List<RoleResponse> findAll() {
        return roleRepository.findAll();
    }

    @Override
    public Boolean updateRole(int id, RoleDto roleDto) {
        Boolean isUpdated=roleRepository.updateRole(id, roleDto);
        if(isUpdated) return isUpdated;
        else return null;
    }

    @Override
    public Boolean deleteRole(int id) {
        return roleRepository.deleteRole(id);
    }

    @Override
    public int selectRoleId() {
        return roleRepository.selectRoleId();
    }

    @Override
    public RoleDto findOne(int id) {
        return roleRepository.findOne(id);
    }

    @Override
    public RoleResponse selectRoleById(int id) {
        return roleRepository.selectRoleByID(id);
    }
}
