package com.kshrd.exam_api.service.serviceImplement;


import com.kshrd.exam_api.controller.request.InstructionNonIDRequestModel;
import com.kshrd.exam_api.controller.request.InstructionNonIDsRequestModel;
import com.kshrd.exam_api.controller.request.InstructionResponse;
import com.kshrd.exam_api.controller.utils.ModelMapperUtil;
import com.kshrd.exam_api.repository.model.InstructionDto;
import com.kshrd.exam_api.repository.rep.InstructionRepository;
import com.kshrd.exam_api.service.serviceInterface.InstructionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class InstructionSeviceImp implements InstructionService {

    InstructionRepository instructionRepository;
    @Autowired
    public InstructionSeviceImp(InstructionRepository instructionRepository) {
        this.instructionRepository = instructionRepository;
    }

    @Override
    public InstructionNonIDRequestModel insert(InstructionNonIDRequestModel instructionDto) {

        boolean isInserted = instructionRepository.insert(instructionDto);
        return isInserted ? instructionDto : null;

    }

    @Override
    public List<InstructionDto> select() {
        return instructionRepository.select();
    }

    @Override
    public boolean update(InstructionNonIDsRequestModel instructionNonIDRequest, int id) {
        return instructionRepository.update(instructionNonIDRequest, id);
    }

    @Override
    public boolean delete(int id) {
        return instructionRepository.delete(id);
    }

    @Override
    public InstructionDto selectByID(int id) {
        return instructionRepository.selectByID(id);
    }

    @Override
    public List<InstructionResponse> getInstructionByQuiz(int id) {
        return instructionRepository.getInstructionByQuiz(id);
    }
}
