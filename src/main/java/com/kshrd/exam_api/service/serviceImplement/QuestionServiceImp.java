package com.kshrd.exam_api.service.serviceImplement;


import com.kshrd.exam_api.repository.model.QuestionDto;
import com.kshrd.exam_api.repository.rep.QuestionRepository;
import com.kshrd.exam_api.service.serviceInterface.QuestionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class QuestionServiceImp implements QuestionService {

    private QuestionRepository questionRepository;
    @Autowired
    public QuestionServiceImp(QuestionRepository questionRepository) {
        this.questionRepository = questionRepository;
    }

    @Override
    public List<QuestionDto> findAll() {
        return questionRepository.findAll();
    }

    @Override
    public Boolean insert(QuestionDto questionDto) {
        return questionRepository.insert(questionDto);
    }

    @Override
    public Boolean delete(int id) {
        return questionRepository.delete(id);
    }

    @Override
    public Boolean update(int id, QuestionDto questionDto) {
        return questionRepository.update(id, questionDto);
    }

    @Override
    public QuestionDto findOne(int id) {
        return questionRepository.findOne(id);
    }

    @Override
    public List<QuestionDto> findByQuizId(int id) {
        return questionRepository.findByQuizId(id);
    }
}
