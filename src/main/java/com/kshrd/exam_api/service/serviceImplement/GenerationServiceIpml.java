package com.kshrd.exam_api.service.serviceImplement;

import com.kshrd.exam_api.controller.request.GenerationNonIDRequestModel;
import com.kshrd.exam_api.controller.request.GenerationNonIDStatusRequestModel;
import com.kshrd.exam_api.controller.request.GenerationRequestDto;
import com.kshrd.exam_api.controller.request.GenerationRequestOnlyStatus;
import com.kshrd.exam_api.repository.model.GenerationDto;
import com.kshrd.exam_api.repository.rep.GenerationRepository;
import com.kshrd.exam_api.service.serviceInterface.GenerationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class GenerationServiceIpml implements GenerationService {

    GenerationRepository generationRepository;
    @Autowired
    public GenerationServiceIpml(GenerationRepository generationRepository) {
        this.generationRepository = generationRepository;
    }

    @Override
    public GenerationNonIDStatusRequestModel insert(GenerationNonIDStatusRequestModel generationDto) {
        boolean isInserted = generationRepository.insert(generationDto);
        return isInserted ? generationDto : null;
    }



    @Override
    public List<GenerationDto> select() {
        return generationRepository.select();
    }

    @Override
    public GenerationDto selectByID(int id) {
        return generationRepository.selectByID(id);
    }

    @Override
    public boolean update(GenerationNonIDStatusRequestModel generationRequestDto, int id) {
        System.out.println("id in service:"+id);
        return generationRepository.update(generationRequestDto, id);
    }

    @Override
    public boolean delete(int id) {
        GenerationDto generationDto = generationRepository.selectByID(id);
        return generationRepository.delete(id);
    }

    @Override
    public int selectID() {
        return generationRepository.selectID();
    }

    @Override
    public List<Integer> selectIdByStatus() {
        return generationRepository.selectIdByStatus();
    }

    @Override
    public boolean UpdateStatus(int id, GenerationRequestOnlyStatus status) {
        return generationRepository.UpdateStatus(id, status);
    }
}
