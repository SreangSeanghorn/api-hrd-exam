package com.kshrd.exam_api.service.serviceImplement;


import com.kshrd.exam_api.controller.request.StudentRequestDto;
import com.kshrd.exam_api.controller.request.StudentRequestFromUser;
import com.kshrd.exam_api.controller.response.messages.BaseMessage;
import com.kshrd.exam_api.repository.model.StudentDto;
import com.kshrd.exam_api.repository.rep.StudentRepository;
import com.kshrd.exam_api.service.serviceInterface.StudentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;

@Service
public class StudentServiceImp implements StudentService {
    private StudentRepository studentRepository;
    private GenerationServiceIpml generationServiceIpml;

    @Autowired
    public void setStudentRepository(StudentRepository studentRepository) {
        this.studentRepository = studentRepository;
    }

    @Autowired
    public void setGenerationServiceIpml(GenerationServiceIpml generationServiceIpml) {
        this.generationServiceIpml = generationServiceIpml;
    }

    @Override
    public Boolean insert(StudentRequestDto requestDto) {
        if(requestDto.getGenerationId() == 0){
            List<Integer> genearationId = generationServiceIpml.selectIdByStatus();
            requestDto.setGenerationId(genearationId.get(genearationId.size()-1));
        }
        boolean isInserted = studentRepository.insert(requestDto);
        if(isInserted){
            return isInserted;
        }else {
           return null;
        }
    }

    @Override
    public int delete(int id) {
        return studentRepository.delete(id);
    }

    @Override
    public Boolean update(int id, StudentRequestFromUser requestDto) {

        boolean isUpdated = studentRepository.update(id,requestDto);
        if(isUpdated)
            return isUpdated;
        else
            return null;
    }

    @Override
    public List<StudentDto> select(int generationId) {
        List<Integer> genearationId = generationServiceIpml.selectIdByStatus();
        System.out.println(genearationId);
        return studentRepository.select(generationId);
    }

    @Override
    public StudentDto selectById(int id) {
        return studentRepository.selectById(id);
    }

    @Override
    public List<StudentDto> selectByClass(String cl,int generationId) {
//        generationId=0;
        System.out.println("class in Service:"+cl+generationId);
        System.out.println("data:"+studentRepository.selectByClass(cl,generationId));
        return studentRepository.selectByClass(cl,generationId);
    }

    @Override
    public List<StudentDto> selectByGeneration(String gt) {
        return studentRepository.selectByGeneration(gt);
    }

    @Override
    public StudentDto selectStudentById(int id) {
        return studentRepository.selectById(id);
    }
    
    @Override
    public StudentDto login(String cardId) {
        return studentRepository.login(cardId);
    }

}
