package com.kshrd.exam_api.service.serviceImplement;

import com.kshrd.exam_api.controller.request.TeacherUpdateDto;
import com.kshrd.exam_api.controller.request.TeacherUpdatePasswordDto;
import com.kshrd.exam_api.controller.response.RoleResponse;
import com.kshrd.exam_api.controller.response.TeacherResponse;
import com.kshrd.exam_api.repository.model.RoleDto;
import com.kshrd.exam_api.repository.model.TeacherDto;
import com.kshrd.exam_api.repository.rep.TeacherRepository;
import com.kshrd.exam_api.service.serviceInterface.TeacherService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TeacherServiceImp implements TeacherService {
    private TeacherRepository teacherRepository;
    @Autowired
    public void setTeacherRepository(TeacherRepository teacherRepository) {
        this.teacherRepository = teacherRepository;
    }

    @Override
    public Boolean insertTeacher(TeacherDto teacher) {
        Boolean isInserted=teacherRepository.insertTeacher(teacher);
        if(isInserted) {
            teacher.setId((teacherRepository.selectTeacherId()));
            for(RoleDto roleDto :teacher.getRoleDto()){
                teacherRepository.insertUserRole(roleDto,teacher);
            }
            return isInserted;
        }
        else return null;
    }

    @Override
    public Boolean updatePassword(int id, TeacherUpdatePasswordDto updatePasswordDto) {
        return teacherRepository.updatePassword(id,updatePasswordDto);
    }

    @Override
    public List<TeacherResponse> findAll() {
        List<TeacherResponse> list=teacherRepository.findAll();
        List<RoleResponse> roleDtos;
        for(int i=0;i<list.size();i++){
            roleDtos =teacherRepository.role(list.get(i).getId());
            list.get(i).setRoleDtos(roleDtos);
        }
        return list;
    }

    @Override
    public TeacherResponse findOne(int id) {
        TeacherResponse teacherResponse=  teacherRepository.findOne(id);
        List<RoleResponse> roleDtos;
        roleDtos =teacherRepository.role(teacherResponse.getId());
        teacherResponse.setRoleDtos(roleDtos);
        return teacherResponse;

    }

    @Override
    public Boolean updateTeacher(int id, TeacherUpdateDto teacher) {
        Boolean isUpdated=teacherRepository.updateTeacher(id,teacher);
        if(isUpdated){
            return isUpdated;
        }
       else return null;
    }
    @Override
    public Boolean deleteTeacher(int id) {
        Boolean isDeleted=teacherRepository.deleteTeacher(id);
        if(isDeleted) return true;
        else  return null;

    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException{
        TeacherDto teacher=teacherRepository.loadUser(username);
        if (teacher == null) {
            throw new UsernameNotFoundException(String.format("No user found with username '%s'.", username));
        } else {
            return teacher;
        }


    }

    @Override
    public TeacherResponse selectTeacherById(int id) {
        return teacherRepository.selectTeacherById(id);
    }
}
