package com.kshrd.exam_api.service.serviceImplement;


import com.kshrd.exam_api.controller.request.MultiAnswer;
import com.kshrd.exam_api.controller.request.QuizRequestDto;
import com.kshrd.exam_api.controller.request.QuizRequestNonDateStatusDto;
import com.kshrd.exam_api.controller.response.InstructionFilterResponse;
import com.kshrd.exam_api.controller.response.QuestionFilterResponse;
import com.kshrd.exam_api.controller.response.QuizResponseFilter;
import com.kshrd.exam_api.controller.utils.DateTimeUtils;
import com.kshrd.exam_api.controller.utils.Paging;
import com.kshrd.exam_api.repository.model.MultiAnswersDto;
import com.kshrd.exam_api.repository.model.OptionDto;
import com.kshrd.exam_api.repository.model.QuizDto;
import com.kshrd.exam_api.repository.model.SingleAnswerDto;
import com.kshrd.exam_api.repository.rep.QuizRepository;
import com.kshrd.exam_api.repository.rep.ResultRepository;
import com.kshrd.exam_api.service.serviceInterface.QuizService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;
//import sun.rmi.runtime.Log;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

@Service
public class QuizServiceImp implements QuizService {

    QuizRepository quizRepository;
    @Autowired
    ResultRepository resultRepository;
    @Autowired
    public QuizServiceImp(QuizRepository quizRepository) {
        this.quizRepository = quizRepository;
    }

    @Override
    public QuizDto insert(QuizDto quizDto) {

        boolean isInserted = quizRepository.insert(quizDto);
        return isInserted ? quizDto : null;

    }

    @Override
    public List<QuizResponseFilter> select() {
        List<QuizResponseFilter> list=quizRepository.select();
        List<InstructionFilterResponse> lIn=null;
        List<QuestionFilterResponse> res;
       List<QuestionFilterResponse> res1;
        MultiAnswersDto multiAnswersDto;
        SingleAnswerDto singleAnswerDto;

        for(int i=0;i<list.size();i++){
            lIn=quizRepository.selectInstruction(list.get(i).getQuizId());
            list.get(i).setResponses(lIn);
            System.out.println(lIn.size());
            for(int j=0;j<lIn.size();j++){
                res=quizRepository.selectQA(lIn.get(j).getInstructionId());
                for(int k=0;k<res.size();k++){
                    res.get(k).setAnswer("");

                }
                if(res.isEmpty()){
                    res1=quizRepository.selectQNoA(lIn.get(j).getInstructionId());
                    for(int x=0;x<res1.size();x++){
                        res1.get(x).setAnswer("");
                        res.add(res1.get(x));
                    }
                }
                lIn.get(j).setResponse(res);
            }
        }
        return list;
    }
    @Override
    public List<QuizResponseFilter> selectByStatus() {
        List<QuizResponseFilter> list=quizRepository.selectByStatus();
        List<InstructionFilterResponse> lIn=null;
        List<QuestionFilterResponse> res;
        List<QuestionFilterResponse> res1;
        MultiAnswersDto multiAnswersDto;
        SingleAnswerDto singleAnswerDto;
        for(int i=0;i<list.size();i++){
            lIn=quizRepository.selectInstruction(list.get(i).getQuizId());
            list.get(i).setResponses(lIn);
            System.out.println(lIn.size());

            for(int j=0;j<lIn.size();j++){
                System.out.println(lIn.get(j).getInstructionId());
                res=quizRepository.selectQA(lIn.get(j).getInstructionId());
                for(int k=0;k<res.size();k++){
                    res.get(k).setAnswer("");

                    multiAnswersDto=quizRepository.selectMultiAnswer(res.get(k).getQuestionId());

                    if((multiAnswersDto.getAnswer())!=null){
                        multiAnswersDto.getAnswer().getOption1().setIsCorrect("false");
                        multiAnswersDto.getAnswer().getOption2().setIsCorrect("false");
                        multiAnswersDto.getAnswer().getOption3().setIsCorrect("false");
                        multiAnswersDto.getAnswer().getOption4().setIsCorrect("false");
                        res.get(k).setAnswer(multiAnswersDto.getAnswer());

                    }



                }
                if(res.isEmpty()){
                    res1=quizRepository.selectQNoA(lIn.get(j).getInstructionId());
                    for(int x=0;x<res1.size();x++){
                        res1.get(x).setAnswer("");
                        res.add(res1.get(x));

                    }

                }
                lIn.get(j).setResponse(res);
            }
        }

        return list;
    }


    @Override
    public List<QuizDto> selectQuiz(int generationId) {
        return quizRepository.selectQuiz(generationId);
    }

    @Override
    public QuizResponseFilter selectQuizByID(int id) {

        QuizResponseFilter filter=quizRepository.selectQuizByID(id);

        List<InstructionFilterResponse> lIn=null;
        List<QuestionFilterResponse> res;
        List<QuestionFilterResponse> res1;

        MultiAnswersDto multiAnswersDto;
        SingleAnswerDto singleAnswerDto;

            lIn=quizRepository.selectInstruction(filter.getQuizId());
            filter.setResponses(lIn);
        for(int j=0;j<lIn.size();j++){
            res=quizRepository.selectQA(lIn.get(j).getInstructionId());
            for(int k=0;k<res.size();k++){
                multiAnswersDto=quizRepository.selectMultiAnswer(res.get(k).getQuestionId());
                singleAnswerDto=quizRepository.selectSingleAnswer(res.get(k).getQuestionId());
                if((multiAnswersDto.getAnswer())!=null){
                    res.get(k).setAnswer(multiAnswersDto.getAnswer());
                    singleAnswerDto.setAnswer(null);
                }
                if((singleAnswerDto.getAnswer())!=null){
                    res.get(k).setAnswer(singleAnswerDto.getAnswer());
                }}
            if(res.isEmpty()){
                res1=quizRepository.selectQNoA(lIn.get(j).getInstructionId());
                for(int x=0;x<res1.size();x++){
                    res.add(res1.get(x));
                }
            }

           lIn.get(j).setResponse(res);

        }
        return filter;
    }

    @Override
    public boolean update(int id,QuizRequestNonDateStatusDto quizRequestDto) {
        return quizRepository.update(id,quizRequestDto);
    }

    @Override
    public boolean delete(int id) {
            return quizRepository.delete(id);

    }

    @Override
    public QuizDto selectByID(int id) {
        return quizRepository.selectByID(id);
    }

    @Override
    public boolean updateQuizStatus(int id, boolean status) {
        return quizRepository.updateQuizStatus(id,status);
    }

    @Override
    public boolean updateQuizStatusFalse(int id) {
        return quizRepository.updateQuizStatusFalse(id);
    }

    @Override
    public boolean isDeleted(int id) {
        return quizRepository.isDeleted(id);
    }

    // create pagination

    @Override
    public Integer countAllQuiz() {
        return quizRepository.countAllQuiz();
    }


    @Override
    public List<QuizResponseFilter> selectAllByStatus(Paging page) {
        page.setTotalCount(quizRepository.countAllQuiz());
        List<QuizResponseFilter> list=quizRepository.selectAllByStatus(page);
        List<InstructionFilterResponse> lIn=null;
        List<QuestionFilterResponse> res;
        List<QuestionFilterResponse> res1;
        MultiAnswersDto multiAnswersDto;
        SingleAnswerDto singleAnswerDto;

        for(int i=0;i<list.size();i++){
            lIn=quizRepository.selectInstruction(list.get(i).getQuizId());
            list.get(i).setResponses(lIn);
            System.out.println(lIn.size());
            for(int j=0;j<lIn.size();j++){
                res=quizRepository.selectQA(lIn.get(j).getInstructionId());
                for(int k=0;k<res.size();k++){
                    multiAnswersDto=quizRepository.selectMultiAnswer(res.get(k).getQuestionId());

                    singleAnswerDto=quizRepository.selectSingleAnswer(res.get(k).getQuestionId());
                    if((multiAnswersDto.getAnswer())!=null){
                        res.get(k).setAnswer(multiAnswersDto.getAnswer());
                        singleAnswerDto.setAnswer(null);
                    }
                    if((singleAnswerDto.getAnswer())!=null){
                        res.get(k).setAnswer(singleAnswerDto.getAnswer());
                    }
                }
                if(res.isEmpty()){
                    res1=quizRepository.selectQNoA(lIn.get(j).getInstructionId());
                    for(int x=0;x<res1.size();x++){
                        res.add(res1.get(x));
                    }
                }
                lIn.get(j).setResponse(res);
            }
        }
        return list;
    }

    @Override
    public boolean updateQuizSent(int id, boolean isSent) {
        List<Integer> list = resultRepository.selectstatus(id);
        boolean check = true;

        for (int i = 0; i < list.size(); i++) {
            System.out.println("nuber:" + list.get(i));
            if (list.get(i) != 2) {
                check = false;
            }
        }
        if (isSent == true) {
            if (check == true) {
                System.out.println(id + " " + isSent);
                return quizRepository.updateQuizSent(id, true);
            }
            else {quizRepository.updateQuizSent(id, false);return false;}

        } else {
            quizRepository.updateQuizSent(id, false);
            return false;
        }
    }
}
