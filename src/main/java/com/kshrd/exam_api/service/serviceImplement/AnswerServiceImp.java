package com.kshrd.exam_api.service.serviceImplement;


import com.kshrd.exam_api.controller.request.SingleAnswerRequestDto;
import com.kshrd.exam_api.repository.model.MultiAnswersDto;
import com.kshrd.exam_api.repository.model.SingleAnswerDto;
import com.kshrd.exam_api.repository.rep.AnswerRepository;
import com.kshrd.exam_api.service.serviceInterface.AnswerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class AnswerServiceImp implements AnswerService {

    private AnswerRepository answerRepository;

    @Autowired
    public AnswerServiceImp(AnswerRepository answerRepository) {
        this.answerRepository = answerRepository;
    }

    @Override
    public List<MultiAnswersDto> findAll() {
        return answerRepository.findAll();
    }

    public List<SingleAnswerDto> singleAnswers(){
        return answerRepository.singleAnswers();
    }

    @Override
    public Boolean insertMultipleChoices(MultiAnswersDto multiAnswersDto) {
        return answerRepository.insertMultipleChoices(multiAnswersDto);
    }

    @Override
    public Boolean insertSingleAnswer(SingleAnswerRequestDto answerRequestDto) {
        return answerRepository.insertSingleAnswer(answerRequestDto);
    }

    @Override
    public Boolean delete(int id) {
        return answerRepository.delete(id);
    }

    @Override
    public Boolean update(int id, MultiAnswersDto multiAnswersDto) {
        return answerRepository.update(id, multiAnswersDto);
    }

    @Override
    public Boolean updateSingleAnswer(int id, SingleAnswerRequestDto requestDto) {
        return answerRepository.updateSingleAnswer(id, requestDto);
    }
}
