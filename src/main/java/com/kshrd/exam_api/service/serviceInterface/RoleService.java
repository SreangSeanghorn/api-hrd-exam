package com.kshrd.exam_api.service.serviceInterface;

import com.kshrd.exam_api.controller.response.RoleResponse;
import com.kshrd.exam_api.repository.model.RoleDto;

import java.util.List;

public interface RoleService {

    Boolean insertRole(RoleDto roleDto);

    List<RoleResponse> findAll();

    Boolean updateRole(int id, RoleDto roleDto);

    Boolean deleteRole(int id);

    int selectRoleId();

    RoleDto findOne(int id);

    RoleResponse selectRoleById(int id);
}
