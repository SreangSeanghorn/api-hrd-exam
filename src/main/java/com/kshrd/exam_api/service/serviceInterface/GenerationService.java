package com.kshrd.exam_api.service.serviceInterface;

import com.kshrd.exam_api.controller.request.GenerationNonIDRequestModel;
import com.kshrd.exam_api.controller.request.GenerationNonIDStatusRequestModel;
import com.kshrd.exam_api.controller.request.GenerationRequestDto;
import com.kshrd.exam_api.controller.request.GenerationRequestOnlyStatus;
import com.kshrd.exam_api.repository.model.GenerationDto;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface GenerationService {

    GenerationNonIDStatusRequestModel insert(GenerationNonIDStatusRequestModel generationDto);
    List<GenerationDto> select() throws Exception;
    GenerationDto selectByID(int id);
    boolean update(GenerationNonIDStatusRequestModel generationRequestDto, int id);
    boolean delete(int id);
    int selectID();
    List<Integer> selectIdByStatus();
    boolean UpdateStatus(int id, GenerationRequestOnlyStatus status);
}
