package com.kshrd.exam_api.service.serviceInterface;

import com.kshrd.exam_api.controller.request.ResultCheckingRequestDto;
import com.kshrd.exam_api.controller.request.ResultRequest;
import com.kshrd.exam_api.controller.request.ResultRequestDto;

import com.kshrd.exam_api.controller.response.CheckingStudentResponse;
import com.kshrd.exam_api.controller.response.ResultFilterResponse;
import com.kshrd.exam_api.controller.request.ScoreRequest;

import com.kshrd.exam_api.controller.response.ResultListResponse;
import com.kshrd.exam_api.controller.response.ResultReponse;
import com.kshrd.exam_api.controller.utils.Paging;
import com.kshrd.exam_api.repository.model.ResultDto;

import java.util.List;

public interface ResultService {
    Boolean insert(ResultRequestDto resultRequestDto);

    Boolean insertResult(ResultRequest results);

    List<ResultDto> select();

    CheckingStudentResponse selectDetailResult(int student_id,int quiz_id);
    List<String> result();
    ResultDto selectByResultId(int id);
    String GetResultById(int id);
    Boolean UpdateResult(int student_id,int quiz_id, ResultCheckingRequestDto requestDto);
    int DeleteResult(int id);
    List<ResultDto> SelectByClassname(String Classname);
    List<String> GetResultByClassname(String Classname);

    ResultDto getResultByStudentID(int student_id,int quiz_id);

    List<ResultDto> getResultByClass(int quiz_id,String class_name);
    List<ResultDto> selectResultByQuiz(int quiz_id);

    List<ResultReponse> selectAssignQuiz(Paging page);
    boolean updateScore(int id, ScoreRequest scoreRequest);
    CheckingStudentResponse checkingResponse(int student_id,int quiz_id);
}
