package com.kshrd.exam_api.service.serviceInterface;

import com.kshrd.exam_api.controller.request.TeacherRequestDto;
import com.kshrd.exam_api.controller.request.TeacherUpdateDto;
import com.kshrd.exam_api.controller.request.TeacherUpdatePasswordDto;
import com.kshrd.exam_api.controller.response.TeacherResponse;
import com.kshrd.exam_api.repository.model.TeacherDto;
import org.springframework.security.core.userdetails.UserDetailsService;

import java.util.List;

public interface TeacherService extends UserDetailsService {

    Boolean insertTeacher(TeacherDto teacher);

    List<TeacherResponse> findAll();

    TeacherResponse findOne(int id);

    Boolean updateTeacher(int id, TeacherUpdateDto teacher);

    Boolean deleteTeacher(int id);

    TeacherResponse selectTeacherById(int id);
    Boolean updatePassword(int id, TeacherUpdatePasswordDto updatePasswordDto);
}
