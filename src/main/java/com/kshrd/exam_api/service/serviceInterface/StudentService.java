package com.kshrd.exam_api.service.serviceInterface;

import com.kshrd.exam_api.controller.request.StudentRequestDto;
import com.kshrd.exam_api.controller.request.StudentRequestFromUser;
import com.kshrd.exam_api.repository.model.StudentDto;

import java.util.List;

public interface StudentService {
    Boolean insert(StudentRequestDto requestDto);
    int delete(int id);
    Boolean update(int id, StudentRequestFromUser requestDto);
    List<StudentDto> select(int generationId);
    StudentDto selectById(int id);
    List<StudentDto> selectByClass(String cl,int generationId);
    List<StudentDto> selectByGeneration(String gt);
    StudentDto selectStudentById(int id);
    StudentDto login(String cardId);

}
