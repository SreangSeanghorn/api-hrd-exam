package com.kshrd.exam_api.service.serviceInterface;

import com.kshrd.exam_api.controller.request.SingleAnswerRequestDto;
import com.kshrd.exam_api.repository.model.MultiAnswersDto;
import com.kshrd.exam_api.repository.model.SingleAnswerDto;

import java.util.List;

public interface AnswerService {

    List<MultiAnswersDto> findAll();
    List<SingleAnswerDto> singleAnswers();
    Boolean insertMultipleChoices(MultiAnswersDto multiAnswersDto);
    Boolean insertSingleAnswer(SingleAnswerRequestDto answerRequestDto);
    Boolean delete(int id);
    Boolean update(int id, MultiAnswersDto multiAnswersDto);
    Boolean updateSingleAnswer(int id, SingleAnswerRequestDto requestDto);
}
