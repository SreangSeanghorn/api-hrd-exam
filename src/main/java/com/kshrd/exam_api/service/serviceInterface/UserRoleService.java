package com.kshrd.exam_api.service.serviceInterface;

import com.kshrd.exam_api.repository.model.RoleDto;

public interface UserRoleService {

    void updateRole(int teacher_id, int role_id, RoleDto roleDto);

    Boolean insertUserRole(int teacher_id, RoleDto roleDto);

    Boolean deleteUserRole(int teacher_id,int role_id);
}
