package com.kshrd.exam_api.service.serviceInterface;

import com.kshrd.exam_api.repository.model.QuestionDto;
import com.kshrd.exam_api.repository.model.QuestionTypeDto;

import java.util.List;

public interface QuestionTypeService {

    List<QuestionTypeDto> findAll();

    Boolean insert(QuestionTypeDto questionTypeDto);

    Boolean update(int id, QuestionTypeDto questionTypeDto);

    Boolean delete(int id);

    QuestionTypeDto findOne(int id);
}
