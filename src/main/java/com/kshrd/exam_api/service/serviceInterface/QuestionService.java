package com.kshrd.exam_api.service.serviceInterface;

import com.kshrd.exam_api.repository.model.QuestionDto;

import java.util.List;

public interface QuestionService {

    List<QuestionDto> findAll();
    Boolean insert(QuestionDto questionDto);
    Boolean delete(int id);
    Boolean update(int id, QuestionDto questionDto);
    QuestionDto findOne(int id);
    List<QuestionDto> findByQuizId(int id);
}
