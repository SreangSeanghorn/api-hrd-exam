package com.kshrd.exam_api.service.serviceInterface;


import com.kshrd.exam_api.controller.request.InstructionNonIDRequestModel;
import com.kshrd.exam_api.controller.request.InstructionNonIDsRequestModel;
import com.kshrd.exam_api.controller.request.InstructionResponse;
import com.kshrd.exam_api.repository.model.InstructionDto;

import java.util.List;

public interface InstructionService {

    InstructionNonIDRequestModel insert(InstructionNonIDRequestModel instructionDto);
    List<InstructionDto> select();
    boolean update(InstructionNonIDsRequestModel instructionNonIDRequest, int id);
    boolean delete(int id);
    InstructionDto selectByID(int id);

    List<InstructionResponse> getInstructionByQuiz(int id);

}
