package com.kshrd.exam_api.service.serviceInterface;

import com.kshrd.exam_api.controller.request.QuizRequestDto;
import com.kshrd.exam_api.controller.request.QuizRequestNonDateStatusDto;
import com.kshrd.exam_api.controller.response.QuizResponseFilter;
import com.kshrd.exam_api.controller.utils.Paging;
import com.kshrd.exam_api.repository.model.QuizDto;

import java.util.List;

public interface QuizService {

    QuizDto insert(QuizDto quizDto);

    List<QuizResponseFilter> select();

    List<QuizResponseFilter> selectByStatus();
    List<QuizDto> selectQuiz(int generationId);

    QuizResponseFilter selectQuizByID(int id);




    boolean update(int id,QuizRequestNonDateStatusDto quizRequestDto);
    boolean delete(int id);
    QuizDto selectByID(int id);
    boolean updateQuizStatus(int id,boolean status);

    boolean updateQuizSent(int id,boolean isSent);
    boolean updateQuizStatusFalse(int id);

    boolean isDeleted(int id);

    // create pagination
    Integer countAllQuiz();
    List<QuizResponseFilter> selectAllByStatus(Paging page);

}
