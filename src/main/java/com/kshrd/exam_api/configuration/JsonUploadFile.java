package com.kshrd.exam_api.configuration;

import java.io.*;

public class JsonUploadFile {
    public void writeFile(Object b) {
        boolean append;
        File file = new File("filejson.txt");
        try {
            FileOutputStream fileOutputStream = new FileOutputStream(file,append=true);
            BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(fileOutputStream));
            bw.write(String.valueOf(b));
            bw.newLine();
            bw.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    public void deleted(){
        File file = new File("filejson.txt");
        file.delete();
    }
}
