package com.kshrd.exam_api.configuration;

import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.net.ConnectException;

@Configuration
public class DataSourceConfiguration {
    @Bean
    public javax.sql.DataSource myPostgresDb() {

            DataSourceBuilder dataSource = DataSourceBuilder.create();
            dataSource.driverClassName("org.postgresql.Driver");

            dataSource.url("jdbc:postgresql://47.88.21.64:5555/hrd_quiz_8th");
            dataSource.username("admin");
            dataSource.password("admin!@#");
            return dataSource.build();



    }
}
