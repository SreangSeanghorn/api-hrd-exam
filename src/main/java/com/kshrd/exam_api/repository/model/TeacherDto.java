package com.kshrd.exam_api.repository.model;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.Collection;
import java.util.List;

public class TeacherDto implements UserDetails {
    private int teacherId;
    private String name;
    private String password;
    private Boolean status;
    private List<RoleDto> roleDto;
    private RoleDto roles;
    public TeacherDto() {
    }

    public TeacherDto(int teacherId, String name, String password, Boolean status, RoleDto roles) {
        this.teacherId = teacherId;
        this.name = name;
        this.password = password;
        this.status = status;
        this.roles = roles;
    }

    public int getId() {
        return teacherId;
    }

    public void setId(int id) {
        this.teacherId = id;
    }

    public RoleDto getRoles() {
        return roles;
    }

    public void setRoles(RoleDto roles) {
        this.roles = roles;
    }

    public TeacherDto(int id, String name, String password, Boolean status, List<RoleDto> roleDto) {
        this.teacherId = id;
        this.name = name;
        this.password = password;
        this.status = status;
        this.roleDto = roleDto;

    }
    public List<RoleDto> getRoleDto() {
        return roleDto;
    }

    public void setRoleDto(List<RoleDto> roleDto) {
        this.roleDto = roleDto;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }


    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return roleDto;
    }

    public String getPassword() {
        return password;
    }

    @Override
    public String getUsername() {
        return this.name;
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return true;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return "Teacher{" +
                "teacherId=" + teacherId +
                ", name='" + name + '\'' +
                ", password='" + password + '\'' +
                ", status=" + status +
                ", roleDto=" + roleDto +
                '}';
    }
}
