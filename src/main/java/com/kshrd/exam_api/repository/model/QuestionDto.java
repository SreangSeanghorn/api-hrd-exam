package com.kshrd.exam_api.repository.model;

public class QuestionDto {
    private int questionId;
    private String question;
    private String image;
    private QuestionTypeDto questionType;
    private InstructionDto instruction;
    AnswerDto answer;
    private int point;

    // constructor
    public QuestionDto() {
    }

    public QuestionDto(int questionId, String question, String image, QuestionTypeDto questionType, InstructionDto instruction, AnswerDto answer, int point) {
        this.questionId = questionId;
        this.question = question;
        this.image = image;
        this.questionType = questionType;
        this.instruction = instruction;
        this.answer = answer;
        this.point = point;
    }

    // getter setter method


    public int getQuestionId() {
        return questionId;
    }

    public void setQuestionId(int questionId) {
        this.questionId = questionId;
    }

    public String getQuestion() {
        return question;
    }

    public void setQuestion(String question) {
        this.question = question;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public QuestionTypeDto getQuestionType() {
        return questionType;
    }

    public void setQuestionType(QuestionTypeDto questionType) {
        this.questionType = questionType;
    }

    public InstructionDto getInstruction() {
        return instruction;
    }

    public void setInstruction(InstructionDto instruction) {
        this.instruction = instruction;
    }

    public AnswerDto getAnswer() {
        return answer;
    }

    public void setAnswer(AnswerDto answer) {
        this.answer = answer;
    }

    public int getPoint() {
        return point;
    }

    public void setPoint(int point) {
        this.point = point;
    }

    // Overiride toString method

    @Override
    public String toString() {
        return "QuestionDto{" +
                "questionId=" + questionId +
                ", question='" + question + '\'' +
                ", image='" + image + '\'' +
                ", questionType=" + questionType +
                ", instruction=" + instruction +
                ", answer=" + answer +
                ", point=" + point +
                '}';
    }
}
