package com.kshrd.exam_api.repository.model;

public class UsersRole {
    private int userRoleId;
    private RoleDto roleDto;
    private TeacherDto teacher;

    public UsersRole() {
    }

    public UsersRole(int userRoleId, RoleDto roleDto, TeacherDto teacher) {
        this.userRoleId = userRoleId;
        this.roleDto = roleDto;
        this.teacher = teacher;
    }

    public int getUserRoleId() {
        return userRoleId;
    }

    public void setUserRoleId(int userRoleId) {
        this.userRoleId = userRoleId;
    }

    public RoleDto getRoleDto() {
        return roleDto;
    }

    public void setRoleDto(RoleDto roleDto) {
        this.roleDto = roleDto;
    }

    public TeacherDto getTeacher() {
        return teacher;
    }

    public void setTeacher(TeacherDto teacher) {
        this.teacher = teacher;
    }

    @Override
    public String toString() {
        return "UsersRole{" +
                "userRoleId=" + userRoleId +
                ", roleDto=" + roleDto +
                ", teacher=" + teacher +
                '}';
    }
}