package com.kshrd.exam_api.repository.model;

public class AnswerDto {
    private int answerId;
    private String answer;

    public AnswerDto() {
    }

    public AnswerDto(int answerId, String answer) {
        this.answerId = answerId;
        this.answer = answer;
    }

    public int getAnswerId() {
        return answerId;
    }

    public void setAnswerId(int answerId) {
        this.answerId = answerId;
    }

    public String getAnswer() {
        return answer;
    }

    public void setAnswer(String answer) {
        this.answer = answer;
    }

    @Override
    public String toString() {
        return "AnswerDto{" +
                "answerId=" + answerId +
                ", answer=" + answer +
                '}';
    }
}
