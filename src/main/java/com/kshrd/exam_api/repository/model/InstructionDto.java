package com.kshrd.exam_api.repository.model;

public class InstructionDto {
    private int instructionId;
    private String instruction;
    private String title;
    private QuizDto quiz;

    public InstructionDto() {
    }

    public InstructionDto(String instruction, String title,QuizDto quiz) {
        this.instruction = instruction;
        this.title=title;
        this.quiz = quiz;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getInstructionId() {
        return instructionId;
    }

    public void setInstructionId(int instructionId) {
        this.instructionId = instructionId;
    }

    public String getInstruction() {
        return instruction;
    }

    public void setInstruction(String instruction) {
        this.instruction = instruction;
    }

    public QuizDto getQuiz() {
        return quiz;
    }

    public void setQuiz(QuizDto quiz) {
        this.quiz = quiz;
    }

    @Override
    public String toString() {
        return "InstructionDto{" +
                "instructionId=" + instructionId +
                ", instruction='" + instruction + '\'' +
                ", title='" + title + '\'' +
                ", quiz=" + quiz +
                '}';
    }
}