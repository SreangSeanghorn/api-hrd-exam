package com.kshrd.exam_api.repository.model;

public class StudentDto {
    private int studentId;
    private String name;
    private String gender;
    private String className;
    private String cardID;
    private GenerationDto generation;

    public StudentDto() {
    }

    public StudentDto(int studentId, String name, String gender, String className, String cardID, GenerationDto generation) {
        this.studentId = studentId;
        this.name = name;
        this.gender = gender;
        this.className = className;
        this.cardID = cardID;
        this.generation = generation;
    }

    public int getStudentId() {
        return studentId;
    }

    public void setStudentId(int studentId) {
        this.studentId = studentId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getClassName() {
        return className;
    }

    public void setClassName(String className) {
        this.className = className;
    }

    public String getCardID() {
        return cardID;
    }

    public void setCardID(String cardID) {
        this.cardID = cardID;
    }

    public GenerationDto getGeneration() {
        return generation;
    }

    public void setGeneration(GenerationDto generation) {
        this.generation = generation;
    }

    @Override
    public String toString() {
        return "Student{" +
                "studentId=" + studentId +
                ", name='" + name + '\'' +
                ", gender='" + gender + '\'' +
                ", className='" + className + '\'' +
                ", cardID=" + cardID +
                ", generation=" + generation +
                '}';
    }
}
