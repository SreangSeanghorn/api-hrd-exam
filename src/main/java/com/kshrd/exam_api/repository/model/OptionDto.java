package com.kshrd.exam_api.repository.model;

public class OptionDto {

    private OptionAnswerDto option1;
    private OptionAnswerDto option2;
    private OptionAnswerDto option3;
    private OptionAnswerDto option4;

    public OptionDto() {
    }

    public OptionDto(OptionAnswerDto option1, OptionAnswerDto option2, OptionAnswerDto option3, OptionAnswerDto option4) {
        this.option1 = option1;
        this.option2 = option2;
        this.option3 = option3;
        this.option4 = option4;
    }

    public OptionAnswerDto getOption1() {
        return option1;
    }

    public void setOption1(OptionAnswerDto option1) {
        this.option1 = option1;
    }

    public OptionAnswerDto getOption2() {
        return option2;
    }

    public void setOption2(OptionAnswerDto option2) {
        this.option2 = option2;
    }

    public OptionAnswerDto getOption3() {
        return option3;
    }

    public void setOption3(OptionAnswerDto option3) {
        this.option3 = option3;
    }

    public OptionAnswerDto getOption4() {
        return option4;
    }

    public void setOption4(OptionAnswerDto option4) {
        this.option4 = option4;
    }

    @Override
    public String toString() {
        return "OptionDto{" +
                "option1=" + option1 +
                ", option2=" + option2 +
                ", option3=" + option3 +
                ", option4=" + option4 +
                '}';
    }
}
