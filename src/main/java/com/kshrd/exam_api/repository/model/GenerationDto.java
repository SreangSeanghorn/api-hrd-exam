package com.kshrd.exam_api.repository.model;

public class GenerationDto {
    private int generationId;
    private String generation;
    private Boolean status;

    public GenerationDto() {

    }

    public GenerationDto(int generationId, String generation, Boolean status) {
        this.generationId = generationId;
        this.generation = generation;
        this.status = status;
    }

    public GenerationDto(String generation, Boolean status) {
        this.generation = generation;
        this.status = status;
    }

    public int getGenerationId() {
        return generationId;
    }

    public void setGenerationId(int generationId) {
        this.generationId = generationId;
    }

    public String getGeneration() {
        return generation;
    }

    public void setGeneration(String generation) {
        this.generation = generation;
    }

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return "GenerationDto{" +
                "generationId=" + generationId +
                ", generation='" + generation + '\'' +
                ", status=" + status +
                '}';
    }
}
