package com.kshrd.exam_api.repository.model;

import java.util.Arrays;

public class MultiAnswersDto {
    private int answerId;
    private OptionDto answer;

    public MultiAnswersDto() {
    }

    public MultiAnswersDto(int answerId, OptionDto answer) {
        this.answerId = answerId;
        this.answer = answer;
    }

    public int getAnswerId() {
        return answerId;
    }

    public void setAnswerId(int answerId) {
        this.answerId = answerId;
    }

    public OptionDto getAnswer() {
        return answer;
    }

    public void setAnswer(OptionDto answer) {
        this.answer = answer;
    }

    @Override
    public String toString() {
        return "MultiAnswersDto{" +
                "answerId=" + answerId +
                ", answer=" + answer +
                '}';
    }
}
