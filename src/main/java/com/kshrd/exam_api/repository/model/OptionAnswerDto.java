package com.kshrd.exam_api.repository.model;

public class OptionAnswerDto {

    private String answer;
    private String isCorrect;

    public OptionAnswerDto() {
    }

    public OptionAnswerDto(String answer, String isCorrect) {
        this.answer = answer;
        this.isCorrect = isCorrect;
    }

    public String getAnswer() {
        return answer;
    }

    public void setAnswer(String answer) {
        this.answer = answer;
    }

    public String getIsCorrect() {
        return isCorrect;
    }

    public void setIsCorrect(String isCorrect) {
        this.isCorrect = isCorrect;
    }

    @Override
    public String toString() {
        return "OptionAnswerDto{" +
                "answer=" + answer +
                ", isCorrect='" + isCorrect + '\'' +
                '}';
    }
}
