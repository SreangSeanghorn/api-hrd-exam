package com.kshrd.exam_api.repository.model;

public class QuestionTypeDto {
    private int questionTypeId;
    private String questionType;

    public QuestionTypeDto() {
    }

    public QuestionTypeDto(int questionTypeId, String questionType) {
        this.questionTypeId = questionTypeId;
        this.questionType = questionType;
    }

    public int getQuestionTypeId() {
        return questionTypeId;
    }

    public void setQuestionTypeId(int questionTypeId) {
        this.questionTypeId = questionTypeId;
    }

    public String getQuestionType() {
        return questionType;
    }

    public void setQuestionType(String questionType) {
        this.questionType = questionType;
    }

    @Override
    public String toString() {
        return "QuestionTypeDto{" +
                "questionTypeId=" + questionTypeId +
                ", questionType='" + questionType + '\'' +
                '}';
    }
}
