package com.kshrd.exam_api.repository.model;

public class ScoreDto {
    private int resultId;
    private float score;
    private boolean status;

    public ScoreDto() {
    }

    public ScoreDto(int id, float score, boolean status) {
        this.resultId = id;
        this.score = score;
        this.status = status;
    }

    public int getId() {
        return resultId;
    }

    public void setId(int id) {
        this.resultId = id;
    }

    public float getScore() {
        return score;
    }

    public void setScore(float score) {
        this.score = score;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return "ScoreDto{" +
                "resultId=" + resultId +
                ", score=" + score +
                ", status=" + status +
                '}';
    }
}
