package com.kshrd.exam_api.repository.model;

import com.fasterxml.jackson.annotation.JsonFormat;

import java.util.Date;

public class QuizDto {

    private int quizId;
    private String title;
    private String subject;
    @JsonFormat(pattern="yyyy-MM-dd")
    private Date date;
    private int duration;
    private Boolean status;
    private GenerationDto generation;
    private TeacherDto teacher;

    public QuizDto() {
    }

    public QuizDto(int quizId, String title, String subject, Date date, int duration, GenerationDto generation, TeacherDto teacher, Boolean status) {
        this.quizId = quizId;
        this.title = title;
        this.subject = subject;
        this.date = date;
        this.duration = duration;
        this.generation = generation;
        this.teacher = teacher;
        this.status = status;
    }

    public int getQuizId() {
        return quizId;
    }

    public void setQuizId(int quizId) {
        this.quizId = quizId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public int getDuration() {
        return duration;
    }

    public void setDuration(int duration) {
        this.duration = duration;
    }

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public GenerationDto getGeneration() {
        return generation;
    }

    public void setGeneration(GenerationDto generation) {
        this.generation = generation;
    }

    public TeacherDto getTeacher() {
        return teacher;
    }

    public void setTeacher(TeacherDto teacher) {
        this.teacher = teacher;
    }

    @Override
    public String toString() {
        return "QuizDto{" +
                "quizId=" + quizId +
                ", title='" + title + '\'' +
                ", subject='" + subject + '\'' +
                ", date=" + date +
                ", duration=" + duration +
                ", status=" + status +
                ", generation=" + generation +
                ", teacher=" + teacher +
                '}';
    }
}
