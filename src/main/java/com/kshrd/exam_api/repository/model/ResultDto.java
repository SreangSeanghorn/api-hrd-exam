package com.kshrd.exam_api.repository.model;

import com.kshrd.exam_api.controller.request.ControllerQuestion;
import com.kshrd.exam_api.controller.request.QuizRequestDto;
import com.kshrd.exam_api.controller.request.ResultJsonContainer;

public class ResultDto {
    private int resultId;
    private ResultJsonContainer result;
    private float score;
    private int quiz_id;
    private int status;
    private QuizRequestDto quiz;
    private StudentDto student;

    public ResultDto() {
    }

    public ResultDto(int resultId, ResultJsonContainer result, float score, int quiz_id, int status, QuizRequestDto quiz, StudentDto student) {
        this.resultId = resultId;
        this.result = result;
        this.score = score;
        this.quiz_id = quiz_id;
        this.status = status;
        this.quiz = quiz;
        this.student = student;
    }

    public int getResultId() {
        return resultId;
    }

    public void setResultId(int resultId) {
        this.resultId = resultId;
    }

    public ResultJsonContainer getResult() {
        return result;
    }

    public void setResult(ResultJsonContainer result) {
        this.result = result;
    }

    public float getScore() {
        return score;
    }

    public void setScore(float score) {
        this.score = score;
    }

    public int getQuiz_id() {
        return quiz_id;
    }

    public void setQuiz_id(int quiz_id) {
        this.quiz_id = quiz_id;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public QuizRequestDto getQuiz() {
        return quiz;
    }

    public void setQuiz(QuizRequestDto quiz) {
        this.quiz = quiz;
    }

    public StudentDto getStudent() {
        return student;
    }

    public void setStudent(StudentDto student) {
        this.student = student;
    }

    @Override
    public String toString() {
        return "ResultDto{" +
                "resultId=" + resultId +
                ", result=" + result +
                ", score=" + score +
                ", quiz_id=" + quiz_id +
                ", status=" + status +
                ", quiz=" + quiz +
                ", student=" + student +
                '}';
    }
}
