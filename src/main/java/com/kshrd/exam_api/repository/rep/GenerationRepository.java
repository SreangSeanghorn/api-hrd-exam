package com.kshrd.exam_api.repository.rep;

import com.kshrd.exam_api.controller.request.GenerationNonIDRequestModel;
import com.kshrd.exam_api.controller.request.GenerationNonIDStatusRequestModel;
import com.kshrd.exam_api.controller.request.GenerationRequestDto;
import com.kshrd.exam_api.controller.request.GenerationRequestOnlyStatus;
import com.kshrd.exam_api.repository.model.GenerationDto;
import com.kshrd.exam_api.repository.provider.GenerationProvider;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface GenerationRepository {

    @InsertProvider(type = GenerationProvider.class, method = "insertGeneration")
    boolean insert(GenerationNonIDStatusRequestModel generationDto);

    @SelectProvider(type = GenerationProvider.class, method = "selectGeneration")
    @Results({
            @Result(column = "id", property = "generationId")
    })
    List<GenerationDto> select();

    @SelectProvider(type = GenerationProvider.class, method = "selectGenerationByID")
    @Results({

            @Result(column = "id", property = "generationId")
    })
    GenerationDto selectByID(int generationId);

    @SelectProvider(type = GenerationProvider.class, method = "selectID")
    @Results({

            @Result(column = "id", property = "generationId")
    })
    int selectID();

    @UpdateProvider(type = GenerationProvider.class, method = "updateGeneration")
    boolean update(@Param("generation") GenerationNonIDStatusRequestModel generationRequestDto,@Param("generationId") int id);

    @DeleteProvider(type = GenerationProvider.class, method = "deleteGeneration")
    boolean delete(int generationId);

    // create new by lon dara
    // select student by status ture
    @SelectProvider(type = GenerationProvider.class,method = "SelectGenerationId")
    List<Integer> selectIdByStatus();

    @Update("update he_generations set status=false")
    boolean setGenerationStatus();

    @Select("SELECT id FROM he_generations where status=true")
    int getGenerationId();

    @UpdateProvider(type = GenerationProvider.class,method = "UpdateStatus")
    boolean UpdateStatus(int id, GenerationRequestOnlyStatus status);
    @Select("select generation from he_generations")
    List<String> getGeneration();
}
