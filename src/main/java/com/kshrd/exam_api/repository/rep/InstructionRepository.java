package com.kshrd.exam_api.repository.rep;


import com.kshrd.exam_api.controller.request.InstructionNonIDRequestModel;
import com.kshrd.exam_api.controller.request.InstructionNonIDsRequestModel;
import com.kshrd.exam_api.controller.request.InstructionResponse;
import com.kshrd.exam_api.repository.model.InstructionDto;
import com.kshrd.exam_api.repository.provider.InstructionProvider;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface InstructionRepository {

    @InsertProvider(type = InstructionProvider.class, method = "insertInstruction")
    boolean insert(InstructionNonIDRequestModel instructionDto);

    @SelectProvider(type = InstructionProvider.class, method = "selectInstruction")
    @Results({
            @Result(column = "id", property = "instructionId"),
            @Result(column = "sid", property = "quiz.quizId")
    })
    List<InstructionDto> select();

    @SelectProvider(type = InstructionProvider.class, method = "selectInstructionByID")
    @Results({
            @Result(column = "id", property = "instructionId"),
            @Result(column = "quiz_id", property = "quiz.quizId")
    })
    InstructionDto selectByID(int instructionId);

    @UpdateProvider(type = InstructionProvider.class, method = "updateInstruction")
    boolean update(@Param("instruct") InstructionNonIDsRequestModel instructionNonIDRequest, @Param("instructionId")int instructionId);

    @DeleteProvider(type = InstructionProvider.class, method = "deleteInstruction")
    boolean delete(int instructionId);
    @Select("SELECT max(id) FROM he_instructions")
    int selectInstructionId();

    @SelectProvider(type = InstructionProvider.class,method = "getInstructionByQuiz")
    List<InstructionResponse> getInstructionByQuiz(int id);

    @Select("SELECT instruction FROM he_instructions WHERE quiz_id=#{quiz_id}")
    List<String> selectSection(int quiz_id);
}
