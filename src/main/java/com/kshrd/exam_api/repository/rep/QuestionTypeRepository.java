package com.kshrd.exam_api.repository.rep;

import com.kshrd.exam_api.repository.model.QuestionDto;
import com.kshrd.exam_api.repository.model.QuestionTypeDto;
import com.kshrd.exam_api.repository.provider.QuestionProvider;
import com.kshrd.exam_api.repository.provider.QuestionTypeProvider;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface QuestionTypeRepository {

    @Select("SELECT * FROM he_questions_types ORDER BY id asc")
    @Results({
            @Result(property = "questionTypeId",column = "id"),
            @Result(property = "questionType", column = "question_type")
    })
    List<QuestionTypeDto> findAll();

    @Insert("INSERT INTO he_questions_types(question_type) VALUES(#{questionType})")
    Boolean insert(QuestionTypeDto questionTypeDto);

    @UpdateProvider(type = QuestionTypeProvider.class, method = "updateQuestionType")
    Boolean update(int id, @Param("questionType") QuestionTypeDto questionTypeDto);

    @DeleteProvider(type = QuestionTypeProvider.class, method = "deleteQuestionType")
    Boolean delete(int id);

    @SelectProvider(type = QuestionTypeProvider.class, method = "findOneById")
    @Results({
            @Result(property = "questionTypeId",column = "id"),
            @Result(property = "questionType", column = "question_type")
    })
    QuestionTypeDto findOne(int id);


}
