package com.kshrd.exam_api.repository.rep;

import com.kshrd.exam_api.controller.request.QuizRequestDto;
import com.kshrd.exam_api.controller.request.QuizRequestNonDateStatusDto;
import com.kshrd.exam_api.controller.response.InstructionFilterResponse;
import com.kshrd.exam_api.controller.response.QuestionFilterResponse;
import com.kshrd.exam_api.controller.response.QuizResponseFilter;
import com.kshrd.exam_api.controller.utils.Paging;
import com.kshrd.exam_api.repository.model.MultiAnswersDto;
import com.kshrd.exam_api.repository.model.QuizDto;
import com.kshrd.exam_api.repository.model.SingleAnswerDto;
import com.kshrd.exam_api.repository.provider.QuizProvider;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface QuizRepository {

    @InsertProvider(type = QuizProvider.class, method = "insertQuiz")
    boolean insert(QuizDto quizDto);

    @SelectProvider(type = QuizProvider.class, method = "selectQuiz")
    @Results(id = "map",value = {
            @Result(column = "id", property = "quizId"),
            @Result(column = "hid", property = "generation_id"),
            @Result(column = "sid", property = "teacher_id")
    })
    List<QuizResponseFilter> select();


    @SelectProvider(type = QuizProvider.class, method = "selectByStatus")
    @Results({
            @Result(column = "id", property = "quizId"),
            @Result(column = "hid", property = "generation_id"),
            @Result(column = "sid", property = "teacher_id")
    })
    List<QuizResponseFilter> selectByStatus();

    @SelectProvider(type = QuizProvider.class, method = "selectQuiz")
    @Results({
            @Result(column = "id", property = "quizId"),
            @Result(column = "hid", property = "generation.generationId"),
            @Result(column = "sid", property = "teacher.teacherId")
    })
    List<QuizDto> selectQuiz(int generationId);

    @SelectProvider(type = QuizProvider.class, method = "selectSentQuiz")
    @Results({
            @Result(column = "id", property = "quizId"),
            @Result(column = "hid", property = "generation.generationId"),
            @Result(column = "sid", property = "teacher.teacherId")
    })
    List<QuizDto> selectSentQuiz();


    @SelectProvider(type = QuizProvider.class, method = "selectProviderByID")
    @Results({
            @Result(column = "id", property = "quizId"),
            @Result(column = "generation_id", property = "generation.generationId"),
            @Result(column = "teacher_id", property = "teacher.teacherId")
    })
    QuizDto selectByID(int quizId);

    @SelectProvider(type = QuizProvider.class, method = "selectProviderByID")
    @Results({
            @Result(column = "id", property = "quizId"),
            @Result(column = "generation_id", property = "generation_id"),
            @Result(column = "teacher_id", property = "teacher_id")
    })
    QuizResponseFilter selectQuizByID(int quizId);

    @UpdateProvider(type = QuizProvider.class, method = "updateQuiz")
    boolean update(int id,QuizRequestNonDateStatusDto dto);

    @DeleteProvider(type = QuizProvider.class, method = "deleteQuiz")
    boolean delete(int quizId);

    @UpdateProvider(type = QuizProvider.class,method = "updateQuizStatus")
    boolean updateQuizStatus(int id,boolean status);

    @UpdateProvider(type = QuizProvider.class,method = "updateQuizSent")
    boolean updateQuizSent(int id,boolean isSent);

    @UpdateProvider(type = QuizProvider.class,method = "updateQuizStatusFalse")
    boolean updateQuizStatusFalse(int id);

    @UpdateProvider(type = QuizProvider.class,method = "isDeleted")
    boolean isDeleted(int id);


    @Select("select i.id as iid,i.instruction,i.title from he_instructions i INNER JOIN he_quizzes q on i.quiz_id=q.id " +
            "where q.id=#{quiz_id}")
    @Result(column = "iid",property = "instructionId")
    List<InstructionFilterResponse> selectInstruction(int quiz_id);


    @Select("SELECT qs.id as qid,qs.question as question,qs.image as qi,qs.point as qp,qt.id as qtid,qt.question_type as qst,aw.id as aid\n" +
            "from he_questions qs \n" +
            "INNER JOIN he_instructions i ON qs.instruction_id=i.id\n" +
            "INNER JOIN he_questions_types qt on qs.question_type_id=qt.id " +
            "INNER JOIN he_answers aw on qs.answer_id=aw.id where i.id=#{instruction_id}")
    @Results({
            @Result(column = "qid",property = "questionId"),
            @Result(column = "question",property = "question"),
            @Result(column = "qi",property = "image"),
            @Result(column = "qp",property = "qPoint"),
            @Result(column = "qtid",property = "questionType_id"),
            @Result(column = "qst",property = "questionType"),
            @Result(column = "aid",property = "answerId")
    })
        List<QuestionFilterResponse> selectQA(int instruction_id);
    @Select("SELECT qs.id as qid,qs.question as question,qs.image as qi,qs.point as qp,qt.id as qtid,qt.question_type as qst\n" +
            "from he_questions qs \n" +
            "INNER JOIN he_instructions i ON qs.instruction_id=i.id\n" +
            "INNER JOIN he_questions_types qt on qs.question_type_id=qt.id \n" +
            "where i.id=#{instruction_id}")
    @Results({
            @Result(column = "qid",property = "questionId"),
            @Result(column = "question",property = "question"),
            @Result(column = "qi",property = "image"),
            @Result(column = "qp",property = "qPoint"),
            @Result(column = "qtid",property = "questionType_id"),
            @Result(column = "qst",property = "questionType")
    })
    List<QuestionFilterResponse> selectQNoA(int instruction_id);

    @Select("SELECT aw.id as id,aw.answer->'option1'->>'answer' as answer1, answer->'option1'->>'isCorrect' as isCorrect1,\n" +
            "answer->'option2'->>'answer' as answer2, answer->'option2'->>'isCorrect' as isCorrect2,\n" +
            "answer->'option3'->>'answer' as answer3, answer->'option3'->>'isCorrect' as isCorrect3,\n" +
            "answer->'option4'->>'answer' as answer4, answer->'option4'->>'isCorrect' as isCorrect4\n" +
            "from he_questions qs \n" +
            "INNER JOIN he_instructions i ON qs.instruction_id=i.id\n" +
            "INNER JOIN he_questions_types qt on qs.question_type_id=qt.id " +
            "INNER JOIN he_answers aw on qs.answer_id=aw.id where qs.id=#{question_id}")
    @Results({
            @Result(property = "answerId", column = "id"),
            @Result(property = "answer.option1.answer", column = "answer1"),
            @Result(property = "answer.option2.answer", column = "answer2"),
            @Result(property = "answer.option3.answer", column = "answer3"),
            @Result(property = "answer.option4.answer", column = "answer4"),
            @Result(property = "answer.option1.isCorrect", column = "isCorrect1"),
            @Result(property = "answer.option2.isCorrect", column = "isCorrect2"),
            @Result(property = "answer.option3.isCorrect", column = "isCorrect3"),
            @Result(property = "answer.option4.isCorrect", column = "isCorrect4"),
    })
    MultiAnswersDto selectMultiAnswer(int question_id);


    @Select("SELECT aw.id as id,aw.answer->>'answer' as answer from he_questions qs\n" +
            "INNER JOIN he_instructions i ON qs.instruction_id=i.id\n" +
            "INNER JOIN he_questions_types qt on qs.question_type_id=qt.id " +
            "INNER JOIN he_answers aw on qs.answer_id=aw.id where qs.id=#{question_id}")
    @Results({
            @Result(property = "answerId", column = "id"),
            @Result(property = "answer", column = "answer")

    })
    SingleAnswerDto selectSingleAnswer(int question_id);



    /// create pagination
    @SelectProvider(type = QuizProvider.class,method = "countAllQuizByStatus")
    Integer countAllQuiz();
    @SelectProvider(type = QuizProvider.class, method = "selectAllQuizByPaging")
    @Results({
            @Result(column = "id", property = "quizId"),
            @Result(column = "hid", property = "generation_id"),
            @Result(column = "sid", property = "teacher_id")
    })
    List<QuizResponseFilter> selectAllByStatus( @Param("page") Paging page);

    @Select("select max(id) from he_quizzes")
    int selectQuiz_id();

    @Select("SELECT title from he_quizzes")
    List<String> selectTitle();
    @Select("SELECT subject from he_quizzes")
    List<String> selectSubject();

    //For Comaparing to Update
    @Select("SELECT title from he_quizzes where id!=#{quiz_id}")
    List<String> selectExceptTitle(int quiz_id);
    @Select("SELECT subject from he_quizzes where id!=#{quiz_id}")
    List<String> selectExceptSubject(int quiz_id);




}
