package com.kshrd.exam_api.repository.rep;

import com.kshrd.exam_api.controller.request.*;
import com.kshrd.exam_api.controller.response.*;

import com.kshrd.exam_api.controller.utils.Paging;
import com.kshrd.exam_api.repository.model.ResultDto;
import com.kshrd.exam_api.repository.provider.ResultProvider;
import org.apache.ibatis.annotations.*;
import org.mapstruct.Mapping;
import org.springframework.stereotype.Repository;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

@Repository
public interface ResultRepository {
    @InsertProvider(type = ResultProvider.class,method = "InsertResult")
    Boolean Insert(ResultRequestDto resultRequestDto);



   @InsertProvider(type = ResultProvider.class,method = "insert")
    Boolean insertResult(ResultRequest results);


    @SelectProvider(type = ResultProvider.class,method = "SelectResult")
    @Results(id="mapSelect",value = {
            @Result(property = "resultId",column = "id"),
            @Result(property = "quiz_id",column = "quiz_id"),
            @Result(property = "quiz.title",column = "title"),
            @Result(property = "quiz.subject",column = "subject"),
            @Result(property = "quiz.date",column = "date"),
            @Result(property = "quiz.duration",column = "duration"),
            @Result(property = "quiz.status",column = "q_status"),
            @Result(property = "quiz.generationId",column = "generation_id"),
            @Result(property = "quiz.teacherId",column = "teacher_id"),
            @Result(property = "student.studentId",column = "student_id"),
            @Result(property = "student.name",column = "name"),
            @Result(property = "student.gender",column = "gender"),
            @Result(property = "student.className",column = "class"),
            @Result(property = "student.cardID",column = "card_id"),
            @Result(property = "student.generation.generationId",column = "generation_id"),
            @Result(property = "student.generation.generation",column = "generation"),
            @Result(property = "student.generation.status",column = "g_status")
    })
    List<ResultDto> select();

    @SelectProvider(type = ResultProvider.class,method = "SelectResultById")
    @Results({
            @Result(property = "resultId",column = "id"),
            @Result(property = "quiz.quizId",column = "quiz_id"),
            @Result(property = "quiz.title",column = "title"),
            @Result(property = "quiz.subject",column = "subject"),
            @Result(property = "quiz.date",column = "date"),
            @Result(property = "quiz.duration",column = "duration"),
            @Result(property = "quiz.status",column = "q_status"),
            @Result(property = "quiz.generationId",column = "generation_id"),
            @Result(property = "quiz.teacherId",column = "teacher_id"),
            @Result(property = "student.studentId",column = "student_id"),
            @Result(property = "student.name",column = "name"),
            @Result(property = "student.gender",column = "gender"),
            @Result(property = "student.className",column = "class"),
            @Result(property = "student.cardID",column = "card_id"),
            @Result(property = "student.generation.generationId",column = "generation_id"),
            @Result(property = "student.generation.generation",column = "generation"),
            @Result(property = "student.generation.status",column = "g_status")
    })
    ResultDto selectByResultId(int id);

    @UpdateProvider(type = ResultProvider.class,method = "UpdateResultProvider")
    Boolean UpdateResult(int student_id,int quiz_id, @Param("requestDto") ResultCheckingRequestDto requestDto);


    @DeleteProvider(type = ResultProvider.class,method = "DeleteResultProvider")
    int DeleteResult(int id);

    @SelectProvider(type = ResultProvider.class,method = "GetResultByClassName")
    @Results(id="map",value = {
            @Result(property = "resultId",column = "id"),
            @Result(property = "status",column = "status"),
            @Result(property = "quiz.title",column = "title"),
            @Result(property = "quiz.subject",column = "subject"),
            @Result(property = "quiz.date",column = "date"),
            @Result(property = "quiz.duration",column = "duration"),
            @Result(property = "quiz.status",column = "q_status"),
            @Result(property = "quiz.generationId",column = "generation_id"),
            @Result(property = "quiz.teacherId",column = "teacher_id"),
            @Result(property = "student.studentId",column = "student_id"),
            @Result(property = "student.name",column = "name"),
            @Result(property = "student.gender",column = "gender"),
            @Result(property = "student.className",column = "class"),
            @Result(property = "student.cardID",column = "card_id"),
            @Result(property = "student.generation.generationId",column = "generation_id"),
            @Result(property = "student.generation.generation",column = "generation"),
            @Result(property = "student.generation.status",column = "g_status")
    })
    List<ResultDto> SelectByClassname(String Classname);

    @SelectProvider(type = ResultProvider.class,method = "SelectOnlyResult")
    List<String> result();

 @SelectProvider(type = ResultProvider.class,method = "resultByStudentId")
 List<String> resultByStudentId(@Param("student_id") int student_id,@Param("quiz_id") int quiz_id);

    @SelectProvider(type = ResultProvider.class,method = "sentByStudentId")
    List<String> sentByStudentId(@Param("student_id") int student_id,@Param("quiz_id") int quiz_id);

    @SelectProvider(type = ResultProvider.class,method = "resultByQuizId")
    List<String> resultByQuizId(@Param("quiz_id") int quiz_id);

    @SelectProvider(type = ResultProvider.class,method = "GetResultByClass")
    List<String> GetResultByClassname(String Classname);

    @SelectProvider(type = ResultProvider.class,method = "SelectOnlyResultByID")
    String GetResultById(int id);

    @SelectProvider(type = ResultProvider.class,method = "getResultByStudentID")
    @ResultMap("map")
    ResultDto getResultByStudentID(int student_id,int quiz_id);


    @SelectProvider(type = ResultProvider.class,method ="getResultByClass" )
    @ResultMap("map")
    List<ResultDto> getResultByClass(int quiz_id,String class_name);


    @SelectProvider(type = ResultProvider.class,method ="selectResultByQuiz" )
    @ResultMap("map")
    List<ResultDto> selectResultByQuiz(int quiz_id);


//    @Select("SELECT DISTINCT q.id,q.title,q.date,q.subject,q.teacher_id from he_results r\n" +
//            "INNER JOIN he_quizzes q\n" +
//            "ON r.quiz_id=q.id order by  q.id ASC limit #{page.limit} offset #{page.offset}")
    @SelectProvider(type = ResultProvider.class,method = "selectAssignQuiz")
    @Results({
            @Result(column = "id",property = "quizId"),
            @Result(column = "teacher_id",property = "teacherId"),

    })
    List<ResultReponse> selectAssignQuiz(@Param("page") Paging page);

    @Select("SELECT count(DISTINCT q.id) from he_quizzes q INNER JOIN he_results r \n" +
            "on q.id=r.quiz_id ")
    int selectCountQuizRecord();


    @SelectProvider(type = ResultProvider.class,method = "selectDetailResult")
    @Results({
            @Result(column = "id", property = "quizId"),
            @Result(column = "hid", property = "generation_id"),
            @Result(column = "sid", property = "teacher_id")
    })
    ResultFilterResponse selectDetailResult(int quiz_id);

@Select("SELECT r.id as rid,r.result as result,r.score as rc,s.id as sid,s.card_id as scard," +
        "s.name as sn,s.gender as sg,s.class as sc from he_results r \n" +
        "INNER JOIN he_quizzes q on r.quiz_id=q.id\n" +
        "INNER JOIN he_students s on r.student_id=s.id where s.id=#{student_id} AND q.id=#{quiz_id}  ")
@Results({
        @Result(column = "rid",property = "result_id"),
       // @Result(column = "result",property ="results" ),
        @Result(column = "rc",property = "score"),
        @Result(column = "sid",property ="student.studentId" ),
        @Result(column = "scard",property = "student.cardID"),
        @Result(column = "sn",property = "student.name"),
        @Result(column = "sg",property ="student.gender" ),
        @Result(column = "sc",property ="student.className" )
})
 List<ResultListResponse> selectResult(@Param("student_id") int student_id,@Param("quiz_id")int quiz_id);

    @Select("SELECT r.id as rid,r.result as result,r.score as rc,s.id as sid,s.card_id as scard," +
            "s.name as sn,s.gender as sg,s.class as sc from he_results r \n" +
            "INNER JOIN he_quizzes q on r.quiz_id=q.id\n" +
            "INNER JOIN he_students s on r.student_id=s.id where s.id=#{student_id} AND q.id=#{quiz_id} AND q.is_sent=true")
    @Results({
            @Result(column = "rid",property = "result_id"),
            // @Result(column = "result",property ="results" ),
            @Result(column = "rc",property = "score"),
            @Result(column = "sid",property ="student.studentId" ),
            @Result(column = "scard",property = "student.cardID"),
            @Result(column = "sn",property = "student.name"),
            @Result(column = "sg",property ="student.gender" ),
            @Result(column = "sc",property ="student.className" )
    })
    List<ResultListResponse> sentResult(@Param("student_id") int student_id,@Param("quiz_id")int quiz_id);

    @UpdateProvider(type = ResultProvider.class,method = "updateOnlyScore")
    boolean updateScore(int id, ScoreRequest scoreRequest);

    @Select("SELECT result from he_results r INNER JOIN he_quizzes q\n" +
            "on r.quiz_id=q.id INNER JOIN he_students s on r.student_id=s.id where q.id=#{quiz_id} AND s.class=#{class_name}  ")
    List<String> selectResultByClass(@Param("quiz_id" )int quiz_id,@Param("class_name") String class_name);

    @Select("select i.id as iid from he_instructions i INNER JOIN he_quizzes q on i.quiz_id=q.id " +
            "where q.id=#{quiz_id}")
    @Result(column = "iid",property = "instructionId")
    List<InstructionCheckingResponse> selectInstruction(int quiz_id);

    @Select("select i.id from he_instructions i INNER JOIN he_quizzes q on i.quiz_id=q.id " +
            "where q.id=#{quiz_id}")
    List<Integer> selectInstructionId(int quiz_id);

    @Select("SELECT qs.id as qid,qs.question as question,qs.image as qi,qs.point as qp,qt.id as qtid,qt.question_type as qst\n" +
            "from he_questions qs \n" +
            "INNER JOIN he_instructions i ON qs.instruction_id=i.id\n" +
            "INNER JOIN he_questions_types qt on qs.question_type_id=qt.id " +
            "INNER JOIN he_answers aw on qs.answer_id=aw.id where i.id=#{instruction_id}")
    @Results({
            @Result(column = "qid",property = "questionId"),
            @Result(column = "question",property = "question"),
            @Result(column = "qi",property = "image"),
            @Result(column = "qp",property = "point"),
            @Result(column = "qtid",property = "questionType_id"),
            @Result(column = "qst",property = "questionType")
    })
    List<QuestionFilterChecking> selectQA(int instruction_id);
    @Select("SELECT qs.id as qid,qs.question as question,qs.image as qi,qs.point as qp,qt.id as qtid,qt.question_type as qst\n" +
            "from he_questions qs \n" +
            "INNER JOIN he_instructions i ON qs.instruction_id=i.id\n" +
            "INNER JOIN he_questions_types qt on qs.question_type_id=qt.id \n" +
            "where i.id=#{instruction_id}")
    @Results({
            @Result(column = "qid",property = "questionId"),
            @Result(column = "question",property = "question"),
            @Result(column = "qi",property = "image"),
            @Result(column = "qp",property = "point"),
            @Result(column = "qtid",property = "questionType_id"),
            @Result(column = "qst",property = "questionType")
    })
    QuestionFilterChecking selectQNoA(int instruction_id);

    @Update("update he_results set result=#{resultRequest.result,jdbcType=OTHER," +
            " typeHandler = com.kshrd.exam_api.configuration.JSONTypeHandler}" +
            ",score=#{resultRequest.score},status=#{resultRequest.status} where student_id=#{student_id} AND quiz_id=#{quiz_id}")
    boolean updateResult(@Param("student_id") int student_id,@Param("quiz_id") int quiz_id,
                         @Param("resultRequest") ResultListResponse resultRequest);


    @Select("select score from he_results where student_id=#{student_id} AND quiz_id=#{quiz_id}")
    int getScore(@Param("student_id") int student_id,@Param("quiz_id") int quiz_id);

    @Select("select status from he_results where student_id=#{student_id} AND quiz_id=#{quiz_id}")
    int getStatus(@Param("student_id") int student_id,@Param("quiz_id") int quiz_id);

    @Select("SELECT DISTINCT s.class from he_results r \n" +
            "INNER JOIN he_students s on r.student_id=s.id\n" +
            "INNER JOIN he_quizzes q on r.quiz_id=q.id\n" +
            "where q.id=#{quiz_id}")
    List<String> selectClassName(int quiz_id);

    @Select("select s.card_id from he_results r inner join he_students s on r.student_id=s.id and quiz_id=#{quiz_id}")
    List<String> selectCardId(int quiz_id);
    @Select("select quiz_id from he_results")
        List<Integer> selectQuizId();

    @Select("select status from he_results where quiz_id=#{id}")
    List<Integer> selectstatus(int id);

    @Select("select student_id from he_results where quiz_id=#{id}")
    List<Integer> selectStudentId(int id);
}
