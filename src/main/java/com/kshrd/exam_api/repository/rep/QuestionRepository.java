package com.kshrd.exam_api.repository.rep;

import com.kshrd.exam_api.controller.response.QuestionsResponse;
import com.kshrd.exam_api.repository.model.QuestionDto;
import com.kshrd.exam_api.repository.provider.QuestionProvider;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface QuestionRepository {

    @SelectProvider(type = QuestionProvider.class, method = "findAllQuestion")
    @Results(id="getMap",value = {
            @Result(property = "questionId", column = "id"),
            @Result(property = "instruction.instructionId", column = "inid"),
            @Result(property = "instruction.instruction", column = "is"),
            @Result(property = "instruction.title", column = "it"),
            @Result(property = "instruction.quiz.quizId", column = "iq"),
            @Result(property = "instruction.quiz.title", column = "ht"),
            @Result(property = "instruction.quiz.subject", column = "hs"),
            @Result(property = "instruction.quiz.date", column = "hdate"),
            @Result(property = "instruction.quiz.duration", column = "hd"),
            @Result(property = "instruction.quiz.status", column = "hst"),
            @Result(property = "questionType.questionTypeId", column = "qtid"),
            @Result(property = "questionType.questionType", column = "qs"),
            @Result(property = "answer.answerId", column = "aid"),
            @Result(property = "answer.answer", column = "aw")
    })
    List<QuestionDto> findAll();

    @InsertProvider(type = QuestionProvider.class, method = "insertQuestion")
    Boolean insert(QuestionDto questionDto);

    @InsertProvider(type = QuestionProvider.class, method = "insertQuestionWithoutAnswer")
    Boolean insertNoAnswer(QuestionDto questionDto);

    @SelectProvider(type = QuestionProvider.class, method = "findById")
    @ResultMap("getMap")
    QuestionDto findOne(int id);

    @DeleteProvider(type = QuestionProvider.class,method = "deleteQuestion")
    Boolean delete(int id);

    @UpdateProvider(type = QuestionProvider.class, method = "updateQuestion")
    Boolean update(int id, @Param("questions")QuestionDto questionDto);

    @UpdateProvider(type = QuestionProvider.class, method = "updateQuestionNoAnswer")
    Boolean updateNoAnswer(int id, @Param("questions")QuestionDto questionDto);

    @SelectProvider(type = QuestionProvider.class, method = "findByQuizId")
    @ResultMap("getMap")
    List<QuestionDto> findByQuizId(int id);

    @Select("SELECT max(id) from he_questions")
    int selectMaxQuestionid();
}
