package com.kshrd.exam_api.repository.rep;

import com.kshrd.exam_api.controller.response.RoleResponse;
import com.kshrd.exam_api.repository.model.RoleDto;
import com.kshrd.exam_api.repository.provider.RoleProvider;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface RoleRepository {

    @InsertProvider(type = RoleProvider.class,method = "insertRole")
    Boolean insertRole(RoleDto roleDto);

    @SelectProvider(type= RoleProvider.class,method = "findAll")
    @Result(column = "id",property = "roleId")
    List<RoleResponse> findAll();

    @UpdateProvider(type = RoleProvider.class,method = "updateRole")
    Boolean updateRole(int id, RoleDto roleDto);

    @Delete("delete from he_roles where id=#{id}")
    Boolean deleteRole(int id);

    @Select("SELECT id from he_roles ORDER BY id DESC LIMIT 1")
    int selectRoleId();

    @Select("select * from he_roles where id=#{id}")
    @Result(column = "id",property = "roleId")
    RoleDto findOne(int id);

    @Select("select * from he_roles where id=#{id}")
    @Result(column = "id",property = "roleId")
    RoleResponse selectRoleByID(int id);
}
