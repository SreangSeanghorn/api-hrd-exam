package com.kshrd.exam_api.repository.rep;

import com.kshrd.exam_api.repository.model.RoleDto;
import com.kshrd.exam_api.repository.provider.UserRoleProvider;
import org.apache.ibatis.annotations.DeleteProvider;
import org.apache.ibatis.annotations.InsertProvider;
import org.apache.ibatis.annotations.UpdateProvider;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRoleRepository {

    @UpdateProvider(type = UserRoleProvider.class,method = "updateUserRole")
    void updateRole(int teacher_id, int role_id, RoleDto roleDto);

    @InsertProvider(type = UserRoleProvider.class,method = "insertUserRole")
    Boolean insertUserRole(int teacher_id, RoleDto roleDto);

    @DeleteProvider(value = UserRoleProvider.class,method = "deleteUserRole")
    Boolean deleteUserRole(int teacher_id,int role_id);

}
