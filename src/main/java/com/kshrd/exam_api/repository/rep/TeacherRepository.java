package com.kshrd.exam_api.repository.rep;

import com.kshrd.exam_api.controller.request.TeacherUpdateDto;
import com.kshrd.exam_api.controller.request.TeacherUpdatePasswordDto;
import com.kshrd.exam_api.controller.response.RoleResponse;
import com.kshrd.exam_api.controller.response.TeacherResponse;
import com.kshrd.exam_api.repository.model.RoleDto;
import com.kshrd.exam_api.repository.model.TeacherDto;
import com.kshrd.exam_api.repository.provider.TeacherProvider;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface TeacherRepository {
    @Insert("Insert into he_teachers(name,password,status) " +
            "values(#{teacher.name},#{teacher.password},#{teacher.status})")
    Boolean insertTeacher(@Param("teacher") TeacherDto teacher);

    @Select("select id,name from he_teachers where status=true  order by id desc")
    @Results(
            @Result(column = "id",property = "teacherId")
    )
    List<TeacherResponse> findAll();

    @Select("select id,name from he_teachers \n" +
            "where status=true AND id=#{id}")

    @Results(
            @Result(column = "id",property = "teacherId")
    )
    TeacherResponse findOne(int id);

    @Select("select r.id,r.role from he_roles r inner join he_users_roles u on r.id=u.role_id" +
            " inner join he_teachers t on t.id=u.teacher_id\n" +
            "where t.id=#{id}")
    @Results(
            @Result(column = "id",property = "roleId")
    )
    List<RoleResponse> role(int id);

    @UpdateProvider(type = TeacherProvider.class,method = "updateTeacher")
    Boolean updateTeacher(int id, TeacherUpdateDto teacher);

    @Delete("delete from he_teachers where id=#{id}")
    Boolean deleteTeacher(int id);

    @InsertProvider(type = TeacherProvider.class,method = "insertUserRole")
    Boolean insertUserRole(RoleDto roleDto, TeacherDto teacher);

    @Select("select id from he_teachers ORDER BY id DESC LIMIT 1")
    int selectTeacherId();

    @SelectProvider(type = TeacherProvider.class,method = "selectUser")
    TeacherDto loadUser(String username);

   @Select("select r.id,r.role from he_roles r INNER JOIN he_users_roles u on r.id=u.role_id \n" +
            "INNER JOIN he_teachers t on t.id=u.teacher_id\n" +
            "WHERE t.id=#{id}")
    List<RoleDto> selectRole(int id);

    @UpdateProvider(type = TeacherProvider.class,method = "updateUserRole")
    void updateUserRole(TeacherDto teacher, RoleDto roleDto);

    @UpdateProvider(type = TeacherProvider.class, method = "insertTeacher")
    boolean update(@Param("teacher") TeacherDto teacherDto);

    @Select("select * from he_teachers where id=#{id}")
    TeacherResponse selectTeacherById(int id);


    @Select("select id from he_teachers where name=#{name}")
    int selectTeacherID(String name);

    @Update("update he_teachers set status=false where id=#{id}")
    boolean updated(int id);

    @Update("update he_teachers set password=#{updatePasswordDto.newPassword}" +
            "where id=#{id}")
    boolean updatePassword(@Param("id") int id, @Param("updatePasswordDto") TeacherUpdatePasswordDto updatePasswordDto);

    @Select("select password from he_teachers where id=#{id}")
    String selectPassword(int id);

    @Select("select name from he_teachers")
    List<String> getUsername();

    @Select("select status from he_teachers where name=#{name}")
    Boolean getStatus(@Param("name") String name);

}
