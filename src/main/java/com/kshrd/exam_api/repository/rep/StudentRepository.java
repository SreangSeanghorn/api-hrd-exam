package com.kshrd.exam_api.repository.rep;

import com.kshrd.exam_api.controller.request.StudentRequestDto;
import com.kshrd.exam_api.controller.request.StudentRequestFromUser;
import com.kshrd.exam_api.repository.model.StudentDto;
import com.kshrd.exam_api.repository.provider.StudentProvider;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Repository;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.List;

@Repository
public interface StudentRepository {
    @InsertProvider(type = StudentProvider.class,method = "InsertStudentProvider")
    Boolean insert(StudentRequestDto requestDto);

    @DeleteProvider(type = StudentProvider.class,method = "DeleteStudentProvider")
    int delete(int id);


    @UpdateProvider(type = StudentProvider.class,method = "UpdateStudentProvider")
    Boolean update(int id, StudentRequestFromUser requestDto);


    @SelectProvider(type = StudentProvider.class,method = "SelectStudentProvider")
    @Results({
            @Result(property = "studentId",column = "id"),
            @Result(property = "cardID",column = "card_id"),
            @Result(property = "className",column = "class"),
            @Result(property = "generation.generationId",column = "generation_id"),
            @Result(property = "generation.generation",column = "generation"),
            @Result(property = "generation.status",column = "status")
    })
    List<StudentDto> select(int generationId);


    @SelectProvider(type = StudentProvider.class,method = "SelectById")
    @Results({
            @Result(property = "studentId",column = "id"),
            @Result(property = "cardID",column = "card_id"),
            @Result(property = "className",column = "class"),
            @Result(property = "generation.generationId",column = "generation_id"),
            @Result(property = "generation.generation",column = "generation"),
            @Result(property = "generation.status",column = "status")
    })
    StudentDto selectById(int id);

    @SelectProvider(type = StudentProvider.class,method = "SelectByClass")
    @Results({
            @Result(property = "studentId",column = "id"),
            @Result(property = "cardID",column = "card_id"),
            @Result(property = "className",column = "class"),
            @Result(property = "generation.generationId",column = "generation_id"),
            @Result(property = "generation.generation",column = "generation"),
            @Result(property = "generation.status",column = "status")
    })
    List<StudentDto> selectByClass(@Param("cl") String cl,int generationId);

    @SelectProvider(type = StudentProvider.class,method = "SelectByGeneration")
    @Results({
            @Result(property = "studentId",column = "id"),
            @Result(property = "cardID",column = "card_id"),
            @Result(property = "className",column = "class"),
            @Result(property = "generation.generationId",column = "generation_id"),
            @Result(property = "generation.generation",column = "generation"),
            @Result(property = "generation.status",column = "status")
    })
    List<StudentDto> selectByGeneration(String gt);

    @Select("SELECT * from he_students where id =#{id}")
    StudentDto selectStudentByID(int id);


    @SelectProvider(type = StudentProvider.class,method = "loadingStudent")
    @Results({
            @Result(property = "studentId",column = "id"),
            @Result(property = "cardID",column = "card_id"),
            @Result(property = "className",column = "class"),
            @Result(property = "generation.generationId",column = "generation_id"),
            @Result(property = "generation.generation",column = "generation"),
            @Result(property = "generation.status",column = "status")
    })
    StudentDto login(String cardId);

//    @Select("SELECT COUNT(*) FROM he_students s JOIN he_generations g\n" +
//            "on s.generation_id=g.id WHERE g.status=true")
//    int countStudentRecords();

    @SelectProvider(type = StudentProvider.class,method = "CountStuRecords")
    int countStudentRecords();


//    @Select("SELECT DISTINCT class from he_students")
    @SelectProvider(type = StudentProvider.class,method = "selectClassName")
    List<String> selectClassName(int generationId);
//    @Select("SELECT DISTINCT class from he_students")
//    List<String> selectClassName();

    @Select("select card_id from he_students")
    List<String> selectCardId();

//    @Select("select s.*,g.generation,g.status\n" +
//            "from he_students s INNER JOIN he_generations g \n" +
//            "on s.generation_id=g.id\n" +
//            "where s.class=#{cl} AND g.")
//    @Results({
//            @Result(property = "studentId",column = "id"),
//            @Result(property = "cardID",column = "card_id"),
//            @Result(property = "className",column = "class"),
//            @Result(property = "generation.generationId",column = "generation_id"),
//            @Result(property = "generation.generation",column = "generation"),
//            @Result(property = "generation.status",column = "status")
//    })
//    List<StudentDto> selectByClass(@Param("cl") String cl,int generationId);
}
