package com.kshrd.exam_api.repository.rep;

import com.kshrd.exam_api.controller.request.SingleAnswerRequestDto;
import com.kshrd.exam_api.repository.model.MultiAnswersDto;
import com.kshrd.exam_api.repository.model.OptionDto;
import com.kshrd.exam_api.repository.model.SingleAnswerDto;
import com.kshrd.exam_api.repository.provider.AnswerProvider;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface AnswerRepository {

    @SelectProvider(type = AnswerProvider.class, method = "multipleAnswers")
    @Results({
            @Result(property = "answerId", column = "id"),
            @Result(property = "answer.option1.answer", column = "answer1"),
            @Result(property = "answer.option2.answer", column = "answer2"),
            @Result(property = "answer.option3.answer", column = "answer3"),
            @Result(property = "answer.option4.answer", column = "answer4"),
            @Result(property = "answer.option1.isCorrect", column = "isCorrect1"),
            @Result(property = "answer.option2.isCorrect", column = "isCorrect2"),
            @Result(property = "answer.option3.isCorrect", column = "isCorrect3"),
            @Result(property = "answer.option4.isCorrect", column = "isCorrect4"),
    })
    List<MultiAnswersDto> findAll();

    @InsertProvider(type = AnswerProvider.class, method = "insertMulChoices")
    Boolean insertMultipleChoices(MultiAnswersDto multiAnswersDto);

    @InsertProvider(type = AnswerProvider.class, method = "insertSingleAnswer")
    Boolean insertSingleAnswer(SingleAnswerRequestDto answerRequestDto);

    @DeleteProvider(type = AnswerProvider.class, method = "deleteAnswer")
    Boolean delete(int id);

    @UpdateProvider(type = AnswerProvider.class,method = "updateAnswer")
    Boolean update(int id,@Param("answers") MultiAnswersDto multiAnswersDto);

    @UpdateProvider(type = AnswerProvider.class, method = "updateSingleAnswer")
    Boolean updateSingleAnswer(int id,@Param("answers") SingleAnswerRequestDto requestDto);

    @SelectProvider(type = AnswerProvider.class, method = "singleAnswers")
    @Results({
            @Result(property = "answerId", column = "id"),
            @Result(property = "answer", column = "answer")
    })
    List<SingleAnswerDto> singleAnswers();

    @Select("SELECT max(id) FROM he_answers")
    int selectAnswerId();

}
