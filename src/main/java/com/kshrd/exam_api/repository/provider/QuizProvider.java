package com.kshrd.exam_api.repository.provider;

import com.kshrd.exam_api.controller.request.QuizRequestNonDateStatusDto;
import com.kshrd.exam_api.controller.utils.Paging;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.jdbc.SQL;

public class QuizProvider {

    public String insertQuiz() {
        return new SQL(){
            {
                INSERT_INTO("he_quizzes");
                VALUES("title, subject, date, duration, status, generation_id, teacher_id", "#{title}, #{subject}, #{date}, #{duration}, #{status}, #{generation.generationId}, #{teacher.teacherId}");
            }
        }.toString();
    }

    public String selectQuiz(int generationId) {
        return new SQL() {
            {
                SELECT("U.id, U.title, U.subject, U.date, U.duration, U.status,H.id as hid, S.id as sid");
                FROM("he_quizzes U INNER JOIN he_teachers S on U.teacher_id = S.id INNER JOIN he_generations H on U.generation_id = H.id");
                if(generationId == 0){
                    WHERE("U.isdeleted=false AND H.status=true");
                }else{
                    WHERE("U.isdeleted=false AND U.generation_id = '"+generationId+"'");
                }
                //ORDER_BY("U.id DESC");
            }
        }.toString();
    }

    public String selectSentQuiz() {
        return new SQL() {
            {
                SELECT("U.id, U.title, U.subject, U.date, U.duration, U.status,H.id as hid, S.id as sid");
                FROM("he_quizzes U INNER JOIN he_teachers S on U.teacher_id = S.id INNER JOIN he_generations H on U.generation_id = H.id");
                WHERE("U.isdeleted=false AND H.status=true AND U.is_sent=true");
            }
        }.toString();
    }

    public String selectByStatus() {
        return new SQL() {
            {
                SELECT("U.id, U.title, U.subject, U.date, U.duration, U.status,H.id as hid, S.id as sid");
                FROM("he_quizzes U INNER JOIN he_teachers S on U.teacher_id = S.id INNER JOIN he_generations H on U.generation_id = H.id");
                WHERE("U.isdeleted=false AND U.status=true");
                //ORDER_BY("U.id DESC");
            }
        }.toString();
    }


    public String selectProviderByID() {
        return new SQL() {
            {
                SELECT("*");
                FROM("he_quizzes");
                WHERE("id = #{quizId} AND isdeleted=false");
            }
        }.toString();
    }

    public String updateQuiz(int id, QuizRequestNonDateStatusDto dto) {
        return new SQL() {
            {
                UPDATE("he_quizzes");
                SET("title = '"+dto.getTitle()+"', subject = '"+dto.getSubject()+"', duration = '"+dto.getDuration()+"', status = '"+dto.isStatus()+"'");
                WHERE("id ='"+id+"' AND isdeleted=false");
            }
        }.toString();
    }
 public String isDeleted(int id){
        return new SQL(){{
            UPDATE("he_quizzes");
            SET("isdeleted=true");
            WHERE("id='"+id+"'");
        }}.toString();
 }
    public String deleteQuiz(int quizId) {
        return new SQL() {
            {
                DELETE_FROM("he_quizzes");
                WHERE("id = #{quizId}");
            };
        }.toString();
    }

    public String updateQuizStatus(int id,boolean status){
        return new SQL(){{
            UPDATE("he_quizzes");
            SET("status='"+status+"'");
            WHERE("id='"+id+"' AND isdeleted=false");
        }}.toString();
    }
    public String updateQuizStatusFalse(int id){
        return new SQL(){{
            UPDATE("he_quizzes");
            SET("status=false");
            WHERE("id!='"+id+"'");
        }}.toString();
    }
    public String updateQuizSent(int id,boolean isSent){
        return new SQL(){{
            UPDATE("he_quizzes");
            SET("is_sent='"+isSent+"'");
            WHERE("id='"+id+"' AND isdeleted=false");
        }}.toString();
    }

    /// create page

    public String selectAllQuizByPaging(@Param("page") Paging page) {
        return new SQL() {
            {
                SELECT("U.id, U.title, U.subject, U.date, U.duration, U.status,H.id as hid, S.id as sid");
                FROM("he_quizzes U INNER JOIN he_teachers S on U.teacher_id = S.id INNER JOIN he_generations H on U.generation_id = H.id");
                if(page.getGenerationId() == 0){
                    WHERE("U.isdeleted=false AND H.status=true");
                }else{

                    WHERE("U.isdeleted=false AND U.generation_id= '"+page.getGenerationId()+"'");

                }
                ORDER_BY("U.date DESC limit #{page.limit} offset #{page.offset}");
            }
        }.toString();
    }

    public String countAllQuizByStatus(){
        return new SQL(){{
            SELECT("COUNT(id)");
            FROM("he_quizzes");
            WHERE("isdeleted=false");
        }}.toString();
    }

    // end create page


}
