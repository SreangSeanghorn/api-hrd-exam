package com.kshrd.exam_api.repository.provider;

import com.kshrd.exam_api.repository.model.RoleDto;
import org.apache.ibatis.jdbc.SQL;

public class UserRoleProvider {
    public String updateUserRole(int teacher_id, int role_id, RoleDto roleDto){
        return new SQL(){{
            UPDATE("he_users_roles set role_id='"+ roleDto.getRoleId()+"'");
            WHERE("teacher_id='"+teacher_id+"' AND role_id='"+role_id+"'");
        }}.toString();
    }
    public String insertUserRole(int teacher_id, RoleDto roleDto){
        return new SQL(){{
            INSERT_INTO("he_users_roles");
            VALUES("role_id,teacher_id","'"+ roleDto.getRoleId()+"','"+teacher_id+"'");

        }}.toString();
    }
    public String deleteUserRole(int teacher_id,int role_id){
        return new SQL(){{
            DELETE_FROM("he_users_roles");
            WHERE("teacher_id='"+teacher_id+"' AND role_id='"+role_id+"'");
        }}.toString();
    }
}
