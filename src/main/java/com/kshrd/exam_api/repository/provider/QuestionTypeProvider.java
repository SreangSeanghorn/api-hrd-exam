package com.kshrd.exam_api.repository.provider;

import org.apache.ibatis.jdbc.SQL;

public class QuestionTypeProvider {

    public String updateQuestionType(int id){
        return new SQL(){{
            UPDATE("he_questions_types");
            SET("question_type = #{questionType.questionType}");
            WHERE("id = #{id}");
        }}.toString();
    }

    public String deleteQuestionType( int id){
        return new SQL(){{
            DELETE_FROM("he_questions_types");
            WHERE("id = #{id}");
        }}.toString();
    }

    public String findOneById(int id){
        return new SQL(){{
            SELECT("*");
            FROM("he_questions_types");
            WHERE("id = #{id}");
        }}.toString();
    }
}
