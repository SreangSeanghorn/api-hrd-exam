package com.kshrd.exam_api.repository.provider;

import com.kshrd.exam_api.repository.model.RoleDto;
import org.apache.ibatis.jdbc.SQL;

public class RoleProvider {
    public String insertRole(){
        return new SQL(){{
            INSERT_INTO("he_roles");
            VALUES("role","#{role}");
        }}.toString();
    }
    public String findAll(){
        return new SQL(){{
            SELECT("*");
            FROM("he_roles");
        }}.toString();
    }
    public String updateRole(int id, RoleDto roleDto){
        return new SQL(){{
            UPDATE("he_roles set role='"+ roleDto.getRole()+"'");
            WHERE("id='"+id+"'");

        }}.toString();
    }
}
