package com.kshrd.exam_api.repository.provider;

import com.kshrd.exam_api.controller.request.StudentRequestDto;
import com.kshrd.exam_api.controller.request.StudentRequestFromUser;
import org.apache.ibatis.jdbc.SQL;

public class StudentProvider {
    public String InsertStudentProvider(){
        return new SQL(){{
            INSERT_INTO("he_students");
            VALUES("name","#{name}");
            VALUES("gender","#{gender}");
            VALUES("class","#{className}");
            VALUES("card_id","#{cardID}");
            VALUES("generation_id","#{generationId}");
        }}.toString();
    }
    public String SelectStudentProvider(int generationId){
        return new SQL(){{
            SELECT("s.*,g.generation,g.status");
            FROM("he_students AS s");
            INNER_JOIN("he_generations AS g ON s.generation_id = g.id");
            if(generationId == 0){
                WHERE("g.status=true");
            }else{
                WHERE("g.id='"+generationId+"'");
            }
            ORDER_BY("s.id ASC");
        }}.toString();
    }

    public String DeleteStudentProvider(int id){
        return new SQL(){{
            DELETE_FROM("he_students");
            WHERE("id = '"+id+"'");
        }}.toString();
    }

    public String UpdateStudentProvider(int id, StudentRequestFromUser requestDto){
        return new SQL(){{
            UPDATE("he_students set name = '"+requestDto.getName()+"',gender='"+requestDto.getGender()
                    +"',class='"+requestDto.getClassName()+"',card_id='"+requestDto.getCardID()+"'");
            WHERE("id = '"+id+"'");
        }}.toString();
    }

    public String SelectById(int id){
        return new SQL(){{
            SELECT("s.*,g.generation,g.status");
            FROM("he_students AS s");
            INNER_JOIN("he_generations AS g ON s.generation_id = g.id");
            WHERE("s.id = '"+id+"'");
        }}.toString();

    }

    public String SelectByClass(String cl,int generationId){
        System.out.println("provider");
        System.out.println("class in Provider:"+cl+generationId);
        return new SQL(){{
            SELECT("s.*,g.generation,g.status");
            FROM("he_students s");
            INNER_JOIN("he_generations g ON s.generation_id = g.id");
            if(generationId ==0){
                WHERE("s.class ='"+cl+"' AND g.status=true");
            }else{
                WHERE("s.class ='"+cl+"'  AND g.id= '"+generationId+"'");
            }
            ORDER_BY("s.name");
        }}.toString();
    }

    public String SelectByGeneration(String gt){
        return new SQL(){{
            SELECT("s.*,g.generation,g.status");
            FROM("he_students AS s");
            INNER_JOIN("he_generations AS g ON s.generation_id = g.id");
            WHERE("g.generation = '"+gt+"'");
            ORDER_BY("s.id ASC");
        }}.toString();
    }
    public String loadingStudent(String cardId){
        return new SQL(){{
            SELECT("s.*,g.generation,g.status");
            FROM("he_students AS s");
            INNER_JOIN("he_generations AS g ON s.generation_id = g.id");
            WHERE("card_id = '"+cardId+"'");
        }}.toString();
    }

    public String CountStuRecords(){
        return new SQL(){{
            SELECT("COUNT(*)");
            FROM("he_students s");
            INNER_JOIN("JOIN he_generations g on s.generation_id=g.id ");
//            if(generationId == 0){
                WHERE("g.status=true");
//            }else{
//                WHERE("g.id= '"+generationId+"'");
//            }

        }}.toString();
    }


    public String selectClassName(int generationId){
        return new SQL(){{
            SELECT("DISTINCT s.class");
            FROM("he_students s");
            INNER_JOIN("he_generations g on s.generation_id = g.id");
            if(generationId == 0){
                WHERE("g.status = true");
            }else{
                WHERE("generation_id = '"+generationId+"'");
            }
        }}.toString();
    }

}
