package com.kshrd.exam_api.repository.provider;


import org.apache.ibatis.jdbc.SQL;

public class QuestionProvider {

    public String findAllQuestion() {
        return new SQL() {{
            SELECT("q.*, i.id as inid,i.instruction as is,i.title as it,i.quiz_id as iq,hq.title as ht," +
                    "hq.subject as hs,hq.date as hdate,hq.duration as hd,hq.status as hst," +
                    "qt.id as qtid," +
                    "qt.question_type as qs, a.id as aid,a.answer as aw");
            FROM("he_questions q");
            INNER_JOIN("he_instructions i ON q.instruction_id = i.id");
            INNER_JOIN("he_quizzes hq ON i.quiz_id= hq.id");
            INNER_JOIN("he_questions_types qt ON q.question_type_id = qt.id");
            INNER_JOIN("he_answers a ON q.answer_id = a.id");
            ORDER_BY("q.id asc");
        }}.toString();
    }

    public String findById(int id) {
        return new SQL() {{
            SELECT("q.*, i.id as inid,i.instruction as is,i.title as it,i.quiz_id as iq,hq.title as ht," +
                    "hq.subject as hs,hq.date as hdate,hq.duration as hd,hq.status as hst," +
                    "qt.id as qtid," +
                    "qt.question_type as qs");
            FROM("he_questions q");
            INNER_JOIN("he_instructions i ON q.instruction_id = i.id");
            INNER_JOIN("he_quizzes hq ON i.quiz_id= hq.id");
            INNER_JOIN("he_questions_types qt ON q.question_type_id = qt.id");
            WHERE("q.id = #{id}");
        }}.toString();
    }

    public String findByQuizId(int id) {
        return new SQL() {{
            SELECT("q.*, i.id as inid,i.instruction as is,i.title as it,i.quiz_id as iq,hq.title as ht," +
                    "hq.subject as hs,hq.date as hdate,hq.duration as hd,hq.status as hst," +
                    "qt.id as qtid," +
                    "qt.question_type as qs, a.id as aid,a.answer as aw");
            FROM("he_questions q");
            INNER_JOIN("he_instructions i ON q.instruction_id = i.id");
            INNER_JOIN("he_quizzes hq ON i.quiz_id= hq.id");
            INNER_JOIN("he_questions_types qt ON q.question_type_id = qt.id");
            INNER_JOIN("he_answers a ON q.answer_id = a.id");
            WHERE("hq.id = '" + id + "'");
        }}.toString();
    }

    public String insertQuestion() {
        return new SQL() {{
            INSERT_INTO("he_questions");
            VALUES("question, image, question_type_id, instruction_id, answer_id,point ",
                    "#{question}, #{image}, #{questionType.questionTypeId} ,#{instruction.instructionId}, #{answer.answerId},#{point}");
        }}.toString();
    }

    public String insertQuestionWithoutAnswer() {
        return new SQL() {{
            INSERT_INTO("he_questions");
            VALUES("question, image, question_type_id, instruction_id, point ",
                    "#{question}, #{image}, #{questionType.questionTypeId} ,#{instruction.instructionId},#{point}");
        }}.toString();
    }

    public String deleteQuestion(int id) {
        return new SQL() {{
            DELETE_FROM("he_questions");
            WHERE("id = #{id}");
        }}.toString();
    }

    public String updateQuestion(int id) {
        return new SQL() {{
            UPDATE("he_questions");
            SET("question = #{questions.question}, image = #{questions.image}, question_type_id = #{questions.questionType.questionTypeId}, " +
                    "instruction_id = #{questions.instruction.instructionId}, answer_id = #{questions.answer.answerId},point=#{questions.point}");
            WHERE("id = '" + id + "'");
        }}.toString();
    }

    public String updateQuestionNoAnswer(int id) {
        return new SQL() {{
            UPDATE("he_questions");
            SET("question = #{questions.question}, image = #{questions.image}, question_type_id = #{questions.questionType.questionTypeId}, " +
                    "instruction_id = #{questions.instruction.instructionId},point=#{questions.point}");
            WHERE("id = '" + id + "'");
        }}.toString();
    }
}
