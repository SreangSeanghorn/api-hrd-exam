package com.kshrd.exam_api.repository.provider;

import org.apache.ibatis.jdbc.SQL;

public class InstructionProvider {

    public String insertInstruction() {
        return new SQL(){
            {
                INSERT_INTO("he_instructions");
                VALUES("instruction,title, quiz_id", "#{instruction}, #{title},#{quizId}");
            }
        }.toString();
    }

    public String selectInstruction() {
        return new SQL() {
            {
                SELECT("U.id, U.instruction,U.title, S.id as sid, S.title");
                FROM("he_instructions U INNER JOIN he_quizzes S on U.quiz_id = S.id");
                ORDER_BY("U.id ASC");
            }
        }.toString();
    }

    public String selectInstructionByID() {
        return new SQL() {
            {
                SELECT("*");
                FROM("he_instructions");
                WHERE("id = #{instructionId}");
            }
        }.toString();
    }

    public String updateInstruction() {
        return new SQL() {
            {
                UPDATE("he_instructions");
                SET("instruction = #{instruct.instruction},title=#{instruct.title}");
                WHERE("id = #{instructionId}");
            }
        }.toString();
    }

    public String deleteInstruction(int instructionId) {
        return new SQL() {
            {
                DELETE_FROM("he_instructions");
                WHERE("id = #{instructionId}");
            };
        }.toString();
    }

    public  String getInstructionByQuiz(int id){
        return new SQL(){{
            SELECT("id,instruction,title");
            FROM("he_instructions");
            WHERE("quiz_id = '"+id+"'");
        }}.toString();
    }

}
