package com.kshrd.exam_api.repository.provider;

import com.kshrd.exam_api.controller.request.GenerationRequestOnlyStatus;
//import javafx.beans.binding.When;
import org.apache.ibatis.jdbc.SQL;

public class GenerationProvider {

    public String insertGeneration() {
        return new SQL(){
            {
                INSERT_INTO("he_generations");
                VALUES("generation", "#{generation}");
            }
        }.toString();
    }

    public String selectGeneration() {
        return new SQL() {
            {
                SELECT("*");
                FROM("he_generations");
                ORDER_BY("id ASC");
            }
        }.toString();
    }

    public String selectID() {
        return new SQL() {
            {
                SELECT("max(id) as id");
                FROM("he_generations");
            }
        }.toString();
    }

    public String selectGenerationByID() {
        return new SQL() {
            {
                SELECT("*");
                FROM("he_generations");
                WHERE("id = #{generationId}");
            }
        }.toString();
    }

    public String updateGeneration() {
        return new SQL() {
            {
                UPDATE("he_generations");
                SET("generation = #{generation.generation}");
                WHERE("id = #{generationId}");
            }
        }.toString();
    }

    public String deleteGeneration(int id) {
        return new SQL() {
            {
                DELETE_FROM("he_generations");
                WHERE("id = #{generationId}");
            };
        }.toString();
    }

    // selected generatioinId with status true.
    public String SelectGenerationId(){
        return new SQL(){{
            SELECT("id");
            FROM("he_generations");
            WHERE("status = true");
        }}.toString();
    }

    // update generation only status
    public String UpdateStatus(int id, GenerationRequestOnlyStatus status){
        return  new SQL(){{
            UPDATE("he_generations set status = '"+status.isStatus()+"'");
            WHERE("id = '"+id+"'");
        }}.toString();
    }
}
