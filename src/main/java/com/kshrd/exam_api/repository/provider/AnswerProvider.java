package com.kshrd.exam_api.repository.provider;

import org.apache.ibatis.jdbc.SQL;

public class AnswerProvider {

    public String multipleAnswers(){
        return new SQL(){{
            SELECT("id, answer->'option1'->>'answer' as answer1, answer->'option1'->>'isCorrect' as isCorrect1," +
                    "answer->'option2'->>'answer' as answer2, answer->'option2'->>'isCorrect' as isCorrect2," +
                    "answer->'option3'->>'answer' as answer3, answer->'option3'->>'isCorrect' as isCorrect3," +
                    "answer->'option4'->>'answer' as answer4, answer->'option4'->>'isCorrect' as isCorrect4");
            FROM("he_answers");
            ORDER_BY("id asc");
        }}.toString();

    }

    public String singleAnswers(){
        return new SQL(){{
            SELECT("id, answer->>'answer' as answer");
            FROM("he_answers");
            ORDER_BY("id asc");
        }}.toString();
    }

    public String insertMulChoices() {
        return new SQL() {{
            INSERT_INTO("he_answers");
            VALUES("answer","#{answer,jdbcType = OTHER, typeHandler = com.kshrd.exam_api.configuration.JSONTypeHandler}");
        }}.toString();
    }

    public String insertSingleAnswer(){
        return new SQL(){{
            INSERT_INTO("he_answers");
            VALUES("answer","#{answer, jdbcType = OTHER, typeHandler = com.kshrd.exam_api.configuration.JSONTypeHandler}");
        }}.toString();
    }

    public String deleteAnswer(int id){
        return new SQL(){{
            DELETE_FROM("he_answers");
            WHERE("id = #{id}");
        }}.toString();
    }

    public String updateAnswer(int id){
        return new SQL(){{
            UPDATE("he_answers");
            SET("answer= #{answers.answer, jdbcType = OTHER, typeHandler = com.kshrd.exam_api.configuration.JSONTypeHandler}");
            WHERE("id=#{id}");
        }}.toString();
    }

    public String updateSingleAnswer(int id){
        return new SQL(){{
            UPDATE("he_answers");
            SET("answer= #{answers.answer, jdbcType = OTHER, typeHandler = com.kshrd.exam_api.configuration.JSONTypeHandler}");
            WHERE("id=#{id}");
        }}.toString();
    }

}
