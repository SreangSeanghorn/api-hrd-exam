package com.kshrd.exam_api.repository.provider;

import com.kshrd.exam_api.controller.request.ResultCheckingRequestDto;
import com.kshrd.exam_api.controller.request.ResultRequest;

import com.kshrd.exam_api.controller.request.ScoreRequest;

import com.kshrd.exam_api.controller.utils.Paging;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.jdbc.SQL;

import java.util.List;

public class ResultProvider {
    public String InsertResult(){
        return new SQL(){{
            INSERT_INTO("he_results");
            VALUES("result","#{result,jdbcType=OTHER, typeHandler = com.kshrd.exam_api.configuration.JSONTypeHandler}");
            VALUES("score","#{score}");
            VALUES("quiz_id","#{quizId}");
            VALUES("student_id","#{studentId}");
            VALUES("status","#{status}");
        }}.toString();  
    }

//    public String insert(){
//
//        return new SQL(){{
//            INSERT_INTO("he_results");
//            VALUES("result","#{jsonContainer,jdbcType=OTHER, typeHandler = com.kshrd.exam_api.configuration.JSONTypeHandler}");
////            VALUES("result","#{score,jdbcType=OTHER, typeHandler = com.kshrd.exam_api.configuration.JSONTypeHandler}");
//            VALUES("score","#{score}");
//
//            VALUES("quiz_id","#{quiz_id}");
//            VALUES("student_id","#{student_id}");
//            VALUES("status","true");
//        }}.toString();
//
//    }
    
    public String insert(){
        return new SQL(){{
            INSERT_INTO("he_results");
            VALUES("result","#{jsonContainer,jdbcType=OTHER, typeHandler = com.kshrd.exam_api.configuration.JSONTypeHandler}");
//            VALUES("result","#{score,jdbcType=OTHER, typeHandler = com.kshrd.exam_api.configuration.JSONTypeHandler}");
            VALUES("score","#{score}");
            VALUES("quiz_id","#{quiz_id}");
            VALUES("student_id","#{student_id}");
            VALUES("status", String.valueOf(0));
        }}.toString();

    }
    public String SelectResult(){
        return new SQL(){{
            SELECT("r.*,q.id as quiz_id,q.title,q.subject,q.duration,q.date,q.status as q_status,q.generation_id,q.teacher_id,s.name,s.gender,s.class,s.card_id,g.generation,g.status as g_status");
            FROM("he_results as r INNER JOIN he_quizzes as q ON r.quiz_id = q.id INNER JOIN he_students as s ON r.student_id = s.id INNER JOIN he_generations as g ON q.generation_id = g.id");
            ORDER_BY("r.id ASC");
        }}.toString();
    }
    public String SelectResultById(int id){
        return new SQL(){{
            SELECT("r.*,q.title,q.subject,q.duration,q.date,q.status as q_status,q.generation_id,q.status as q_status,s.name,s.gender,s.class,s.card_id,g.generation,g.status as g_status");
            FROM("he_results as r INNER JOIN he_quizzes as q ON r.quiz_id = q.id INNER JOIN he_students as s ON r.student_id = s.id INNER JOIN he_generations as g ON q.generation_id = g.id");
            WHERE("r.id = '"+id+"'");
        }}.toString();
    }

    public String selectResultByQuiz(int quiz_id){
        return new SQL(){{
            SELECT("r.*,q.title,q.subject,q.duration,q.date,q.status as q_status,q.generation_id,q.status as q_status,s.name,s.gender,s.class,s.card_id,g.generation,g.status as g_status");
            FROM("he_results as r INNER JOIN he_quizzes as q ON r.quiz_id = q.id INNER JOIN he_students as s ON r.student_id = s.id INNER JOIN he_generations as g ON q.generation_id = g.id");
            WHERE("q.id = '"+quiz_id+"'");
        }}.toString();
    }
    public String SelectOnlyResult()
    {
        return new SQL(){{
            SELECT("result");
            FROM("he_results");
        }}.toString();
    }
    public String resultByStudentId()
    {
        return new SQL(){{
            SELECT("result");
            FROM("he_results");
            WHERE("student_id=#{student_id} AND quiz_id=#{quiz_id}");
        }}.toString();
    }
    public String sentByStudentId()
    {
        return new SQL(){{
            SELECT("result from he_results r");
            INNER_JOIN("he_quizzes q ON r.quiz_id=q.id");
            WHERE("student_id=#{student_id} AND quiz_id=#{quiz_id} AND q.is_sent=true");
        }}.toString();
    }
    public String resultByQuizId()
    {
        return new SQL(){{
            SELECT("result");
            FROM("he_results");
            WHERE("quiz_id=#{quiz_id}");
        }}.toString();
    }
    public String SelectOnlyResultByID(int id){
        return new SQL(){{
            SELECT("result");
            FROM("he_results");
            WHERE("id = '"+id+"'");
        }}.toString();
    }
    public String DeleteResultProvider(int id){
        return new SQL(){{
            DELETE_FROM("he_results");
            WHERE("id=#{id}");
        }}.toString();
    }
   public String UpdateResultProvider(int student_id,int quiz_id){
      // System.out.println(requestDto.getStatus()+requestDto.getResult().toString());
        return new SQL(){{
            UPDATE("he_results set result = #{requestDto.result,jdbcType=OTHER," +
                    " typeHandler = com.kshrd.exam_api.configuration.JSONTypeHandler}" +
                    ",score = #{requestDto.score}, quiz_id = '"+quiz_id+"',student_id = #{requestDto.student_id},status=#{requestDto.status}");
            WHERE("student_id='"+student_id+"' AND quiz_id='"+quiz_id+"'");
        }}.toString();
    }

    public String GetResultByClassName(String Classname)
    {
        return new SQL(){{
            SELECT("r.*,q.title,q.subject,q.duration,q.date,q.generation_id,q.status as q_status,q.teacher_id,s.name,s.gender,s.class,s.card_id,g.generation,g.status as g_status");
            FROM("he_results as r INNER JOIN he_quizzes as q ON r.quiz_id = q.id INNER JOIN he_students as s ON r.student_id = s.id INNER JOIN he_generations as g ON q.generation_id = g.id");
            WHERE("s.class = #{Classname}");
            ORDER_BY("r.id ASC");
        }}.toString();
    }

    public String GetResultByClass(String Classname)
    {
        return new SQL(){{
            SELECT("r.result");
            FROM("he_results as r INNER JOIN he_students as s ON r.student_id = s.id");
            WHERE("s.class = #{Classname}");
        }}.toString();
    }

    public String getResultByStudentID(int student_id,int quiz_id)
    {
        System.out.println("Provider: student_id:"+student_id+"quiz_id"+quiz_id);
        return new SQL(){{
            SELECT("r.*,q.title,q.subject,q.duration,q.date,q.status as q_status,q.generation_id,q.status as q_status,s.name,s.gender,s.class,s.card_id,g.generation,g.status as g_status,q.teacher_id");
            FROM("he_results as r INNER JOIN he_quizzes as q ON r.quiz_id = q.id INNER JOIN he_students as s ON r.student_id = s.id INNER JOIN he_generations as g ON q.generation_id = g.id");
            WHERE("s.id = '"+student_id+"' AND q.id='"+quiz_id+"'");
        }}.toString();
    }

    public String getResultByClass(int quiz_id,String class_name)
    {
        return new SQL(){{
            SELECT("r.*,q.title,q.subject,q.duration,q.date,q.status as q_status,q.generation_id,q.status as q_status,s.name,s.gender,s.class,s.card_id,g.generation,g.status as g_status,q.teacher_id");
            FROM("he_results as r INNER JOIN he_quizzes as q ON r.quiz_id = q.id INNER JOIN he_students as s ON r.student_id = s.id INNER JOIN he_generations as g ON q.generation_id = g.id");
            WHERE("q.id='"+quiz_id+"' AND s.class='"+class_name+"'");
        }}.toString();
    }


    public String selectDetailResult() {
        return new SQL() {
            {
                SELECT("U.id, U.title, U.subject, U.date, U.duration, U.status,H.id as hid, S.id as sid");
                FROM("he_quizzes U INNER JOIN he_teachers S on U.teacher_id = S.id INNER JOIN he_generations H on U.generation_id = H.id");
                WHERE("U.id=#{quiz_id} AND U.isdeleted=false");
                //ORDER_BY("U.id DESC");
            }
        }.toString();
    }

    public String updateOnlyScore(int id, ScoreRequest scoreRequest){
        return new SQL(){{
            UPDATE("he_results set score = #{scoreRequest.score},status=#{scoreRequest.status}");
            WHERE("id='"+id+"'");
        }}.toString();
    }

    public String selectAssignQuiz(@Param("page") Paging page){
        return new SQL(){{
            SELECT("DISTINCT q.id,q.title,q.date,q.subject,q.teacher_id,q.is_sent from he_results r");
            INNER_JOIN("he_quizzes q ON r.quiz_id = q.id");
            INNER_JOIN("he_generations g on g.id=q.generation_id ");
            if(page.getGenerationId()==0){
                WHERE("q.isdeleted = false AND g.status=true" );
            }else{
                WHERE("q.isdeleted = false AND q.generation_id = '"+page.getGenerationId()+"'");
            }
            ORDER_BY("q.id ASC limit #{page.limit} offset #{page.offset}");
        }}.toString();
    }
}
