package com.kshrd.exam_api.repository.provider;

import com.kshrd.exam_api.controller.request.TeacherUpdateDto;
import com.kshrd.exam_api.repository.model.RoleDto;
import com.kshrd.exam_api.repository.model.TeacherDto;
import org.apache.ibatis.jdbc.SQL;

public class TeacherProvider {
    public String updateTeacher(int id, TeacherUpdateDto teacher){
        return new SQL(){{
            UPDATE("he_teachers set name='"+teacher.getName()+"'" +
                    ",status='"+teacher.getStatus()+"'");
            WHERE("id='"+id+"'");

        }}.toString();
    }
    public String insertUserRole(RoleDto roleDto, TeacherDto teacher){
        return new SQL(){
            {
                INSERT_INTO("he_users_roles");
                VALUES("role_id,teacher_id","'"+ roleDto.getRoleId()+"','"+teacher.getId()+"'");

            }
        }.toString();
    }
    public String updateUserRole(TeacherDto teacher, RoleDto roleDto){
        return new SQL(){{
            UPDATE("he_users_roles set role_id='"+ roleDto.getRoleId()+"'");
            WHERE("teacher_id='"+teacher.getId()+"'");
        }}.toString();
    }
    public String selectUser(){
        return new SQL(){{
            SELECT("*");
            FROM("he_teachers");
            WHERE("name=#{username}");

        }}.toString();
    }
    public String insertTeacher (){
        return new SQL(){
        }.toString();
    }
}
