package com.kshrd.exam_api.controller.request;

import io.swagger.annotations.ApiModelProperty;

public class InstructionRequestDto {
   @ApiModelProperty(required = true, position = 0)
    private int id;
    @ApiModelProperty(required = true, position = 0)
    private String instruction;
    @ApiModelProperty(required = true, position = 1)
    private  String title;
    @ApiModelProperty(required = true, position = 2)
    private int quizId;


    public InstructionRequestDto() {
    }

    public InstructionRequestDto(String instruction, int quizId,String title) {
       this.id = id;
        this.instruction = instruction;
        this.quizId = quizId;
        this.title=title;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getInstruction() {
        return instruction;
    }

    public void setInstruction(String instruction) {
        this.instruction = instruction;
    }

    public int getQuizId() {
        return quizId;
    }

    public void setQuizId(int quizId) {
        this.quizId = quizId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    @Override
    public String toString() {
        return "InstructionRequestDto{" +
                "id=" + id +
                ", instruction='" + instruction + '\'' +
                ", quizId=" + quizId +
                ", title='" + title + '\'' +
                '}';
    }
}