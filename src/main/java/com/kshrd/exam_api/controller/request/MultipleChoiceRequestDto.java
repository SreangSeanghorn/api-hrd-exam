package com.kshrd.exam_api.controller.request;

import com.kshrd.exam_api.repository.model.OptionDto;

public class MultipleChoiceRequestDto {
    private OptionDto answer;

    public MultipleChoiceRequestDto() {
    }

    public MultipleChoiceRequestDto(OptionDto answer) {
        this.answer = answer;
    }

    public OptionDto getAnswer() {
        return answer;
    }

    public void setAnswer(OptionDto answer) {
        this.answer = answer;
    }

    @Override
    public String toString() {
        return "AnswerRequestDto{" +
                "answer=" + answer +
                '}';
    }
}
