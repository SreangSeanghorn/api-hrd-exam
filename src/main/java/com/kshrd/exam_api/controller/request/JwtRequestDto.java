package com.kshrd.exam_api.controller.request;

import io.swagger.annotations.ApiModelProperty;

public class JwtRequestDto {
    @ApiModelProperty(required = true, position = 0)
    private String username;
    @ApiModelProperty(required = true, position = 1)
    private String password;;

    public JwtRequestDto()
    {
    }

    public JwtRequestDto(String username, String password) {
        this.username = username;
        this.password = password;
    }

    public String getUsername() {
        return this.username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() { return this.password; }

    public void setPassword(String password) {
        this.password = password;
    }

    @Override
    public String toString() {
        return "JwtRequestDto{" +
                "username='" + username + '\'' +
                ", password='" + password + '\'' +
                '}';
    }
}
