package com.kshrd.exam_api.controller.request;

public class AnswerRequestDto {
    private String answer;

    public AnswerRequestDto() {
    }

    public AnswerRequestDto(String answer) {
        this.answer = answer;
    }

    public String getAnswer() {
        return answer;
    }

    public void setAnswer(String answer) {
        this.answer = answer;
    }

    @Override
    public String toString() {
        return "AnswerRequestDto{" +
                "answer=" + answer +
                '}';
    }
}
