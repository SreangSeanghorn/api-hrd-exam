package com.kshrd.exam_api.controller.request;

import com.kshrd.exam_api.repository.model.OptionDto;

import java.util.List;
import java.util.Optional;

public class MultiAnswer {
    Object answer;

    public MultiAnswer() {
    }

    public MultiAnswer(Object answer) {
        this.answer = answer;
    }

    public Object getAnswer() {
        return answer;
    }

    public void setAnswer(Object answer) {
        this.answer = answer;
    }

    @Override
    public String toString() {
        return "MultiAnswer{" +
                "answer=" + answer +
                '}';
    }
}
