package com.kshrd.exam_api.controller.request;

import io.swagger.annotations.ApiModelProperty;

import java.util.List;

public class QuestionDetailRequest {
    @ApiModelProperty(required = true, position = 0)
    private int instructionId;
    @ApiModelProperty(required = true, position = 1)
    List<QuestionList> questionLists;

    public QuestionDetailRequest() {
    }

    public QuestionDetailRequest(int instructionId, List<QuestionList> questionLists) {
        this.instructionId = instructionId;
        this.questionLists = questionLists;
    }

    public List<QuestionList> getQuestionLists() {
        return questionLists;
    }

    public void setQuestionLists(List<QuestionList> questionLists) {
        this.questionLists = questionLists;
    }

    public int getInstructionId() {
        return instructionId;
    }

    public void setInstructionId(int instructionId) {
        this.instructionId = instructionId;
    }

    @Override
    public String toString() {
        return "QuestionDetailRequest{" +
                "instructionId=" + instructionId +
                ", questionLists=" + questionLists +
                '}';
    }
}
