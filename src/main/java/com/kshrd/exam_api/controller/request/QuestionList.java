package com.kshrd.exam_api.controller.request;

import io.swagger.annotations.ApiModelProperty;

import java.util.List;

public class QuestionList {
    @ApiModelProperty(required = true, position = 0)
    private String ask;
    @ApiModelProperty(required = true, position = 1)
    private String image;
    @ApiModelProperty(required = true, position = 2)
    private List<Ansoption> ans;

    public QuestionList() {
    }

    public QuestionList(String ask,String image, List<Ansoption> ans) {
        this.ask = ask;
        this.ans = ans;
        this.image = image;
    }

    public String getAsk() {
        return ask;
    }

    public void setAsk(String ask) {
        this.ask = ask;
    }

    public List<Ansoption> getAns() {
        return ans;
    }

    public void setAns(List<Ansoption> ans) {
        this.ans = ans;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    @Override
    public String toString() {
        return "QuestionList{" +
                "ask='" + ask + '\'' +
                ", image='" + image + '\'' +
                ", ans=" + ans +
                '}';
    }
}
