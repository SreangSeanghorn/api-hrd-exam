package com.kshrd.exam_api.controller.request;

public class GenerationRequestOnlyStatus {

    private boolean status;

    public GenerationRequestOnlyStatus() {
    }

    public GenerationRequestOnlyStatus(boolean status) {
        this.status = status;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return "GenerationRequestOnlyStatus{" +
                "status=" + status +
                '}';
    }
}
