package com.kshrd.exam_api.controller.request;

import com.fasterxml.jackson.annotation.JsonFormat;

import java.util.Date;

public class QuizRequestModel {

    private int id;
    private String title;
    private String subject;
    @JsonFormat(pattern="yyyy-MM-dd")
    private Date date;
    private int duration;
    private Boolean status;
    private int generationId;
    private int teacherId;

    public QuizRequestModel() {
    }

    public QuizRequestModel(int id, String title, String subject, Date date, int duration, Boolean status, int generationId, int teacherId) {
        this.id = id;
        this.title = title;
        this.subject = subject;
        this.date = date;
        this.duration = duration;
        this.status = status;
        this.generationId = generationId;
        this.teacherId = teacherId;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public int getDuration() {
        return duration;
    }

    public void setDuration(int duration) {
        this.duration = duration;
    }

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public int getGenerationId() {
        return generationId;
    }

    public void setGenerationId(int generationId) {
        this.generationId = generationId;
    }

    public int getTeacherId() {
        return teacherId;
    }

    public void setTeacherId(int teacherId) {
        this.teacherId = teacherId;
    }

    @Override
    public String toString() {
        return "QuizRequestModel{" +
                "id=" + id +
                ", title='" + title + '\'' +
                ", subject='" + subject + '\'' +
                ", date=" + date +
                ", duration=" + duration +
                ", status=" + status +
                ", generationId=" + generationId +
                ", teacherId=" + teacherId +
                '}';
    }
}
