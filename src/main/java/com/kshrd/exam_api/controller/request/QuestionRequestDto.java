package com.kshrd.exam_api.controller.request;

import io.swagger.annotations.ApiModelProperty;

public class QuestionRequestDto {
    @ApiModelProperty(required = true, position = 0)
    private int questionId;
    @ApiModelProperty(required = true, position = 1)
    private String question;
    @ApiModelProperty(required = true, position = 2)
    private String image;
    @ApiModelProperty(required = true, position = 3)
    private int questionTypeId;
    @ApiModelProperty(required = true, position = 4)
    private int instructionId;
    @ApiModelProperty(required = true, position = 5)
    private int answerId;
    private int point;

    public QuestionRequestDto() {
    }

    public QuestionRequestDto(int questionId, String question, String image, int questionTypeId, int instructionId, int answerId, int point) {
        this.questionId = questionId;
        this.question = question;
        this.image = image;
        this.questionTypeId = questionTypeId;
        this.instructionId = instructionId;
        this.answerId = answerId;
        this.point = point;
    }

    public int getPoint() {
        return point;
    }

    public void setPoint(int point) {
        this.point = point;
    }

    public int getQuestionId() {
        return questionId;
    }

    public void setQuestionId(int questionId) {
        this.questionId = questionId;
    }

    public String getQuestion() {
        return question;
    }

    public void setQuestion(String question) {
        this.question = question;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public int getQuestionTypeId() {
        return questionTypeId;
    }

    public void setQuestionTypeId(int questionTypeId) {
        this.questionTypeId = questionTypeId;
    }

    public int getInstructionId() {
        return instructionId;
    }

    public void setInstructionId(int instructionId) {
        this.instructionId = instructionId;
    }

    public int getAnswerId() {
        return answerId;
    }

    public void setAnswerId(int answerId) {
        this.answerId = answerId;
    }

    @Override
    public String toString() {
        return "QuestionRequestDto{" +
                "questionId=" + questionId +
                ", question='" + question + '\'' +
                ", image='" + image + '\'' +
                ", questionTypeId=" + questionTypeId +
                ", instructionId=" + instructionId +
                ", answerId=" + answerId +
                ", point=" + point +
                '}';
    }
}
