package com.kshrd.exam_api.controller.request;

import com.kshrd.exam_api.controller.response.RoleResponse;
import com.kshrd.exam_api.repository.model.RoleDto;
import io.swagger.annotations.ApiModelProperty;

import java.util.List;

public class TeacherRequestDto {
    @ApiModelProperty(required = true, position = 0)
    private String name;
    @ApiModelProperty(required = true, position = 2)
    private String password;
    @ApiModelProperty(required = true, position = 3)
    private List<RoleResponse> roleDto;
    public TeacherRequestDto() {
    }

    public TeacherRequestDto(String name, String gender, String password, Boolean status) {
        this.name = name;
        this.password = password;
    }

    public TeacherRequestDto(String name, String password, Boolean status, List<RoleResponse> roleDto) {
        this.name = name;
        this.password = password;
        this.roleDto = roleDto;
    }

    public List<RoleResponse> getRoleDto() {
        return roleDto;
    }

    public void setRoleDto(List<RoleResponse> roleDto) {
        this.roleDto = roleDto;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }


    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Override
    public String toString() {
        return "TeacherRequestModel{" +
                "name='" + name + '\'' +
                ", password='" + password + '\'' +
                ", roleDto=" + roleDto +
                '}';
    }
}
