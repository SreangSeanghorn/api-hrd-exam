package com.kshrd.exam_api.controller.request;

import io.swagger.annotations.ApiModelProperty;

public class InstructionNonIDRequestModel {
    @ApiModelProperty(required = true, position = 0)
    private String instruction;
    @ApiModelProperty(required = true, position = 1)
    private String title;
    @ApiModelProperty(required = true, position = 2)
    private int quizId;

    public InstructionNonIDRequestModel() {
    }

    public InstructionNonIDRequestModel(String instruction, String title,int quizId) {
        this.instruction = instruction;
        this.title=title;
        this.quizId = quizId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getInstruction() {
        return instruction;
    }

    public void setInstruction(String instruction) {
        this.instruction = instruction;
    }

    public int getQuizId() {
        return quizId;
    }

    public void setQuizId(int quizId) {
        this.quizId = quizId;
    }

    @Override
    public String toString() {
        return "InstructionNonIDRequestModel{" +
                "instruction='" + instruction + '\'' +
                ", title='" + title + '\'' +
                ", quizId=" + quizId +
                '}';
    }
}
