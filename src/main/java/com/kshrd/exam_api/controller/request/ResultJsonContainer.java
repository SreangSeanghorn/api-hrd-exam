package com.kshrd.exam_api.controller.request;

import com.kshrd.exam_api.controller.response.InstructionFilterResponse;

import java.util.List;

public class ResultJsonContainer {
    private List<InstructionFilterResponse> filterResponses;

    public ResultJsonContainer() {
    }

    public ResultJsonContainer(List<InstructionFilterResponse> filterResponses) {
        this.filterResponses = filterResponses;
    }

    public List<InstructionFilterResponse> getFilterResponses() {
        return filterResponses;
    }

    public void setFilterResponses(List<InstructionFilterResponse> filterResponses) {
        this.filterResponses = filterResponses;
    }

    @Override
    public String toString() {
        return "ResultJsonContainer{" +
                "filterResponses=" + filterResponses +
                '}';
    }
}
