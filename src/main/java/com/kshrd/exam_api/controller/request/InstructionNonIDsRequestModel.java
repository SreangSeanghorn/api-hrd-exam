package com.kshrd.exam_api.controller.request;

public class InstructionNonIDsRequestModel {

    private String instruction;
    private String title;

    public InstructionNonIDsRequestModel() {
    }

    public InstructionNonIDsRequestModel(String instruction, String title) {
        this.instruction = instruction;
        this.title = title;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getInstruction() {
        return instruction;
    }

    public void setInstruction(String instruction) {
        this.instruction = instruction;
    }

    @Override
    public String toString() {
        return "InstructionNonIDsRequestModel{" +
                "instruction='" + instruction + '\'' +
                ", title='" + title + '\'' +
                '}';
    }
}
