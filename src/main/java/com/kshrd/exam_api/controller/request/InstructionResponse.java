package com.kshrd.exam_api.controller.request;

public class InstructionResponse {
    private int id;
    private String instruction;
    private String title;

    public InstructionResponse() {
    }

    public InstructionResponse(int id, String instruction, String title) {
        this.id = id;
        this.instruction = instruction;
        this.title = title;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getInstruction() {
        return instruction;
    }

    public void setInstruction(String instruction) {
        this.instruction = instruction;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    @Override
    public String toString() {
        return "InstructionResponse{" +
                "id=" + id +
                ", instruction='" + instruction + '\'' +
                ", title='" + title + '\'' +
                '}';
    }
}
