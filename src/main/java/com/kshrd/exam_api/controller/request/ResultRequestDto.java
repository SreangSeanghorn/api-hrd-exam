package com.kshrd.exam_api.controller.request;

import io.swagger.annotations.ApiModelProperty;

public class ResultRequestDto {
    @ApiModelProperty(required = true, position = 0)
    private ControllerQuestion result;
    @ApiModelProperty(required = true, position = 1)
    private float score;
    @ApiModelProperty(required = true, position = 2)
    private int quizId;
    @ApiModelProperty(required = true, position = 3)
    private int studentId;
    @ApiModelProperty(required = true, position = 4)
    private boolean status;

    public ResultRequestDto() {
    }


    public ResultRequestDto(ControllerQuestion result, float score, int quizId, int studentId, boolean status) {
        this.result = result;
        this.score = score;
        this.quizId = quizId;
        this.studentId = studentId;
        this.status = status;
    }

    public ControllerQuestion getResult() {
        return result;
    }

    public void setResult(ControllerQuestion result) {
        this.result = result;
    }

    public float getScore() {
        return score;
    }

    public void setScore(float score) {
        this.score = score;
    }

    public int getQuizId() {
        return quizId;
    }

    public void setQuizId(int quizId) {
        this.quizId = quizId;
    }

    public int getStudentId() {
        return studentId;
    }

    public void setStudentId(int studentId) {
        this.studentId = studentId;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return "ResultRequestDto{" +
                "result='" + result + '\'' +
                ", score=" + score +
                ", quizId=" + quizId +
                ", studentId=" + studentId +
                ", studentId=" + status +
                '}';
    }
}
