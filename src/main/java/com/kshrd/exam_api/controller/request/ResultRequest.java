package com.kshrd.exam_api.controller.request;

public class ResultRequest {
    private int quiz_id;
    private ResultJsonContainer jsonContainer;
    private int student_id;
    private int score;

    public ResultRequest() {
    }

    public ResultRequest(int quiz_id, ResultJsonContainer jsonContainer, int student_id, int score) {
        this.quiz_id = quiz_id;
        this.jsonContainer = jsonContainer;
        this.student_id = student_id;
        this.score = score;
    }

    public int getQuiz_id() {
        return quiz_id;
    }

    public void setQuiz_id(int quiz_id) {
        this.quiz_id = quiz_id;
    }

    public ResultJsonContainer getJsonContainer() {
        return jsonContainer;
    }

    public void setJsonContainer(ResultJsonContainer jsonContainer) {
        this.jsonContainer = jsonContainer;
    }

    public int getStudent_id() {
        return student_id;
    }

    public void setStudent_id(int student_id) {
        this.student_id = student_id;
    }

    public int getScore() {
        return score;
    }

    public void setScore(int score) {
        this.score = score;
    }

    @Override
    public String toString() {
        return "ResultRequest{" +
                "quiz_id=" + quiz_id +
                ", jsonContainer=" + jsonContainer +
                ", student_id=" + student_id +
                ", score=" + score +
                '}';
    }
}
