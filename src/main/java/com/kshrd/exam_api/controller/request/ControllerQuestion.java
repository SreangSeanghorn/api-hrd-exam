package com.kshrd.exam_api.controller.request;

import java.util.List;

public class ControllerQuestion {
    private List<QuestionDetailRequest> question;

    public ControllerQuestion() {
    }

    public ControllerQuestion(List<QuestionDetailRequest> question) {
        this.question = question;
    }

    public List<QuestionDetailRequest> getQuestion() {
        return question;
    }

    public void setQuestion(List<QuestionDetailRequest> question) {
        this.question = question;
    }

    @Override
    public String toString() {
        return "ControllerQuestion{" +
                "question=" + question +
                '}';
    }
}
