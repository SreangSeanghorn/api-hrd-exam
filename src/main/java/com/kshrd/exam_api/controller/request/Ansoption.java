package com.kshrd.exam_api.controller.request;

public class Ansoption {
    private String option;
    public Ansoption() {
    }

    public Ansoption(String option) {
        this.option = option;
    }

    public String getOption() {
        return option;
    }

    public void setOption(String option) {
        this.option = option;
    }

    @Override
    public String toString() {
        return "Ansoption{" +
                "option='" + option + '\'' +
                '}';
    }
}
