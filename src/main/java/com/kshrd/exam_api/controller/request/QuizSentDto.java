package com.kshrd.exam_api.controller.request;

public class QuizSentDto {
    Boolean isSent;

    public QuizSentDto() {
    }

    public QuizSentDto(Boolean isSent) {
        this.isSent = isSent;
    }

    public Boolean getSent() {
        return isSent;
    }

    public void setSent(Boolean sent) {
        isSent = sent;
    }

    @Override
    public String toString() {
        return "QuizSentDto{" +
                "isSent=" + isSent +
                '}';
    }
}
