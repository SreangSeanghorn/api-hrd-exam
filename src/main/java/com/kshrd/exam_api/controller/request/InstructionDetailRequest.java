package com.kshrd.exam_api.controller.request;

import io.swagger.annotations.ApiModelProperty;

import java.util.ArrayList;
import java.util.List;

public class InstructionDetailRequest {
    @ApiModelProperty(required = true, position = 0)
    List<QuestionDetailRequest> truefalse = new ArrayList<>();
    @ApiModelProperty(required = true, position = 1)
    List<QuestionDetailRequest> multiplechoice = new ArrayList<>();
    @ApiModelProperty(required = true, position = 2)
    List<QuestionDetailRequest> fillInGap = new ArrayList<>();
    @ApiModelProperty(required = true, position = 3)
    List<QuestionDetailRequest> questions = new ArrayList<>();
    @ApiModelProperty(required = true, position = 4)
    List<QuestionDetailRequest> code = new ArrayList<>();

    public InstructionDetailRequest() {
    }

    public InstructionDetailRequest(List<QuestionDetailRequest> truefalse, List<QuestionDetailRequest> multiplechoice, List<QuestionDetailRequest> fillInGap, List<QuestionDetailRequest> questions, List<QuestionDetailRequest> code) {
        this.truefalse = truefalse;
        this.multiplechoice = multiplechoice;
        this.fillInGap = fillInGap;
        this.questions = questions;
        this.code = code;
    }

    public List<QuestionDetailRequest> getTruefalse() {
        return truefalse;
    }

    public void setTruefalse(List<QuestionDetailRequest> truefalse) {
        this.truefalse = truefalse;
    }

    public List<QuestionDetailRequest> getMultiplechoice() {
        return multiplechoice;
    }

    public void setMultiplechoice(List<QuestionDetailRequest> multiplechoice) {
        this.multiplechoice = multiplechoice;
    }

    public List<QuestionDetailRequest> getFillInGap() {
        return fillInGap;
    }

    public void setFillInGap(List<QuestionDetailRequest> fillInGap) {
        this.fillInGap = fillInGap;
    }

    public List<QuestionDetailRequest> getQuestions() {
        return questions;
    }

    public void setQuestions(List<QuestionDetailRequest> questions) {
        this.questions = questions;
    }

    public List<QuestionDetailRequest> getCode() {
        return code;
    }

    public void setCode(List<QuestionDetailRequest> code) {
        this.code = code;
    }

    @Override
    public String toString() {
        return "InstructionDetailRequest{" +
                "truefalse=" + truefalse +
                ", multiplechoice=" + multiplechoice +
                ", fillInGap=" + fillInGap +
                ", questions=" + questions +
                ", code=" + code +
                '}';
    }
}
