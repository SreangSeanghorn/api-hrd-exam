package com.kshrd.exam_api.controller.request;

import io.swagger.annotations.ApiModelProperty;

public class TeacherUpdateDto {
    @ApiModelProperty(required = true, position = 0)
    private String name;
    @ApiModelProperty(required = true, position = 2)
    private String password;
    @ApiModelProperty(required = true, position = 3)
    private Boolean status;

    public TeacherUpdateDto() {
    }

    public TeacherUpdateDto(String name, String password, Boolean status) {
        this.name = name;
        this.password = password;
        this.status = status;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return "TeacherUpdateDto{" +
                "name='" + name + '\'' +
                ", password='" + password + '\'' +
                ", status=" + status +
                '}';
    }
}
