package com.kshrd.exam_api.controller.request;

public class JSONUpdatePoint {
    private int questionId;
    private int point;

    public JSONUpdatePoint() {
    }

    public JSONUpdatePoint(int questionId, int point) {
        this.questionId = questionId;
        this.point = point;
    }

    public int getQuestionId() {
        return questionId;
    }

    public void setQuestionId(int questionId) {
        this.questionId = questionId;
    }

    public int getPoint() {
        return point;
    }

    public void setPoint(int point) {
        this.point = point;
    }

    @Override
    public String toString() {
        return "JSONUpdatePoint{" +
                "questionId=" + questionId +
                ", point=" + point +
                '}';
    }
}
