package com.kshrd.exam_api.controller.request;


import io.swagger.annotations.ApiModelProperty;

public class StudentRequestDto {

    @ApiModelProperty(required = true, position = 0)
    private String name;
    @ApiModelProperty(required = true, position = 1)
    private String gender;
    @ApiModelProperty(required = true, position = 2)
    private String className;
    @ApiModelProperty(required = true, position = 3)
    private String cardID;
    @ApiModelProperty(required = true, position = 4)
    private int generationId;

    public StudentRequestDto() {
    }

    public StudentRequestDto(String name, String gender, String className, String cardID, int generationId) {
        this.name = name;
        this.gender = gender;
        this.className = className;
        this.cardID = cardID;
        this.generationId = generationId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getClassName() {
        return className;
    }

    public void setClassName(String className) {
        this.className = className;
    }

    public String getCardID() {
        return cardID;
    }

    public void setCardID(String cardID) {
        this.cardID = cardID;
    }

    public int getGenerationId() {
        return generationId;
    }

    public void setGenerationId(int generationId) {
        this.generationId = generationId;
    }

    @Override
    public String toString() {
        return "StudentRequestDto{" +
                "name='" + name + '\'' +
                ", gender='" + gender + '\'' +
                ", className='" + className + '\'' +
                ", cardID=" + cardID +
                ", generationId=" + generationId +
                '}';
    }
}
