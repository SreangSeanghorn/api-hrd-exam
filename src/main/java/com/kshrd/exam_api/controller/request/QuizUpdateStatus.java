package com.kshrd.exam_api.controller.request;

public class QuizUpdateStatus {
    boolean status;

    public QuizUpdateStatus(Boolean status) {
        this.status = status;
    }

    public QuizUpdateStatus() {
    }

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }
}
