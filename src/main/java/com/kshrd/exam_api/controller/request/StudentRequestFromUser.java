package com.kshrd.exam_api.controller.request;


import io.swagger.annotations.ApiModelProperty;

public class StudentRequestFromUser {
    @ApiModelProperty(required = true, position = 0)
    private String name;
    @ApiModelProperty(required = true, position = 1)
    private String gender;
    @ApiModelProperty(required = true, position = 2)
    private String className;
    @ApiModelProperty(required = true, position = 3)
    private String cardID;

    public StudentRequestFromUser() {
    }

    public StudentRequestFromUser(String name, String gender, String className, String cardID) {
        this.name = name;
        this.gender = gender;
        this.className = className;
        this.cardID = cardID;
    }
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getClassName() {
        return className;
    }

    public void setClassName(String className) {
        this.className = className;
    }

    public String getCardID() {
        return cardID;
    }

    public void setCardID(String cardID) {
        this.cardID = cardID;
    }
}
