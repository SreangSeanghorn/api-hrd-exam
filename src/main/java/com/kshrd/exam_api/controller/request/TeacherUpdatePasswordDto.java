package com.kshrd.exam_api.controller.request;

import io.swagger.annotations.ApiModelProperty;
import io.swagger.annotations.ApiOperation;

public class TeacherUpdatePasswordDto {
    @ApiModelProperty(required = true, position = 0)
    private String oldPassword;
    @ApiModelProperty(required = true, position = 1)
    private String newPassword;

    public TeacherUpdatePasswordDto() {
    }

    public TeacherUpdatePasswordDto(String oldPassword, String newPassword) {
        this.oldPassword = oldPassword;
        this.newPassword = newPassword;
    }

    public String getOldPassword() {
        return oldPassword;
    }

    public void setOldPassword(String oldPassword) {
        this.oldPassword = oldPassword;
    }

    public String getNewPassword() {
        return newPassword;
    }

    public void setNewPassword(String newPassword) {
        this.newPassword = newPassword;
    }

    @Override
    public String toString() {
        return "TeacherUpdatePasswordDto{" +
                "oldPassword='" + oldPassword + '\'' +
                ", newPassword='" + newPassword + '\'' +
                '}';
    }
}
