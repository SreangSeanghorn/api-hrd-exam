package com.kshrd.exam_api.controller.request;

public class GenerationNonIDStatusRequestModel {

    private String generation;

    public GenerationNonIDStatusRequestModel() {
    }

    public GenerationNonIDStatusRequestModel(String generation) {
        this.generation = generation;
    }

    public String getGeneration() {
        return generation;
    }

    public void setGeneration(String generation) {
        this.generation = generation;
    }

    @Override
    public String toString() {
        return "GenerationNonIDStatusRequestModel{" +
                "generation='" + generation + '\'' +
                '}';
    }
}
