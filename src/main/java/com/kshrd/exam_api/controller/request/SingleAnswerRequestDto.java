package com.kshrd.exam_api.controller.request;

public class SingleAnswerRequestDto {

    private AnswerRequestDto answer;

    public SingleAnswerRequestDto() {
    }

    public SingleAnswerRequestDto(AnswerRequestDto answer) {
        this.answer = answer;
    }

    public AnswerRequestDto getAnswer() {
        return answer;
    }

    public void setAnswer(AnswerRequestDto answer) {
        this.answer = answer;
    }

    @Override
    public String toString() {
        return "SingleAnswerRequestDto{" +
                "answer=" + answer +
                '}';
    }
}
