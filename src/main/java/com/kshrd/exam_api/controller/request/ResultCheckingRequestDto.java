package com.kshrd.exam_api.controller.request;

public class ResultCheckingRequestDto {
    private ResultJsonContainer result;
    private int score;
    private int status;
    private int student_id;

    public ResultCheckingRequestDto() {
    }

    public ResultCheckingRequestDto(ResultJsonContainer result, int score, int status, int student_id) {
        this.result = result;
        this.score = score;
        this.status = status;
        this.student_id = student_id;
    }


    public ResultJsonContainer getResult() {
        return result;
    }

    public void setResult(ResultJsonContainer result) {
        this.result = result;
    }

    public int getScore() {
        return score;
    }

    public void setScore(int score) {
        this.score = score;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public int getStudent_id() {
        return student_id;
    }

    public void setStudent_id(int student_id) {
        this.student_id = student_id;
    }

    @Override
    public String toString() {
        return "ResultCheckingRequestDto{" +
                "result=" + result +
                ", score=" + score +
                ", status=" + status +
                ", student_id=" + student_id +
                '}';
    }
}
