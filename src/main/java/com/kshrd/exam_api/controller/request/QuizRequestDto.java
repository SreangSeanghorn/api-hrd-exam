package com.kshrd.exam_api.controller.request;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModelProperty;

import java.util.Date;

public class QuizRequestDto {

    @ApiModelProperty(required = true, position = 0)
    private String title;
    @ApiModelProperty(required = true, position = 1)
    private String subject;
    @JsonFormat(pattern="yyyy-MM-dd")
    @ApiModelProperty(required = true, position = 2)
    private Date date;
    @ApiModelProperty(required = true, position = 3)
    private int duration;
    @ApiModelProperty(required = true, position = 4)
    private Boolean status;
    @ApiModelProperty(required = true, position = 5)
    private int generationId;
    @ApiModelProperty(required = true, position = 6)
    private int teacherId;

    public QuizRequestDto() {
    }

    public QuizRequestDto(String title, String subject, Date date, int duration, Boolean status, int generationId, int teacherId) {
        this.title = title;
        this.subject = subject;
        this.date = date;
        this.duration = duration;
        this.status = status;
        this.generationId = generationId;
        this.teacherId = teacherId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public int getDuration() {
        return duration;
    }

    public void setDuration(int duration) {
        this.duration = duration;
    }

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public int getGenerationId() {
        return generationId;
    }

    public void setGenerationId(int generationId) {
        this.generationId = generationId;
    }

    public int getTeacherId() {
        return teacherId;
    }

    public void setTeacherId(int teacherId) {
        this.teacherId = teacherId;
    }

    @Override
    public String toString() {
        return "QuizRequestDto{" +
                "title='" + title + '\'' +
                ", subject='" + subject + '\'' +
                ", date=" + date +
                ", duration=" + duration +
                ", status=" + status +
                ", generationId=" + generationId +
                ", teacherId=" + teacherId +
                '}';
    }
}
