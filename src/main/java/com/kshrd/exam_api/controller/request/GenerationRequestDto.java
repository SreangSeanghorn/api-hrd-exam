package com.kshrd.exam_api.controller.request;

public class GenerationRequestDto {
    private int id;
    private String generation;
    private Boolean status;

    public GenerationRequestDto() {
    }

    public GenerationRequestDto(int id, String generation, Boolean status) {
        this.id = id;
        this.generation = generation;
        this.status = status;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getGeneration() {
        return generation;
    }

    public void setGeneration(String generation) {
        this.generation = generation;
    }

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return "GenerationRequestDto{" +
                "id=" + id +
                ", generation='" + generation + '\'' +
                ", status=" + status +
                '}';
    }
}
