package com.kshrd.exam_api.controller.request;

import com.kshrd.exam_api.repository.model.RoleDto;

public class UsersRole {
    private int userRoleId;
    private RoleDto roleDto;
    private TeacherRequestDto teacher;

    public UsersRole() {
    }

    public UsersRole(int userRoleId, RoleDto roleDto, TeacherRequestDto teacher) {
        this.userRoleId = userRoleId;
        this.roleDto = roleDto;
        this.teacher = teacher;
    }

    public int getUserRoleId() {
        return userRoleId;
    }

    public void setUserRoleId(int userRoleId) {
        this.userRoleId = userRoleId;
    }

    public RoleDto getRoleDto() {
        return roleDto;
    }

    public void setRoleDto(RoleDto roleDto) {
        this.roleDto = roleDto;
    }

    public TeacherRequestDto getTeacher() {
        return teacher;
    }

    public void setTeacher(TeacherRequestDto teacher) {
        this.teacher = teacher;
    }

    @Override
    public String toString() {
        return "UsersRole{" +
                "userRoleId=" + userRoleId +
                ", roleDto=" + roleDto +
                ", teacher=" + teacher +
                '}';
    }
}