package com.kshrd.exam_api.controller.request;

public class GenerationNonIDRequestModel {

    private String generation;
    private boolean status;

    public GenerationNonIDRequestModel() {
    }

    public GenerationNonIDRequestModel(String generation, boolean status) {
        this.generation = generation;
        this.status = status;
    }

    public String getGeneration() {
        return generation;
    }

    public void setGeneration(String generation) {
        this.generation = generation;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return "GenerationNonIDRequestModel{" +
                "generation='" + generation + '\'' +
                ", status=" + status +
                '}';
    }
}
