package com.kshrd.exam_api.controller.request;

public class UpdateOnlyStatus {
    private Boolean status;

    public UpdateOnlyStatus() {
    }

    public UpdateOnlyStatus(Boolean status) {
        this.status = status;
    }

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return "UpdateOnlyStatus{" +
                "status=" + status +
                '}';
    }
}
