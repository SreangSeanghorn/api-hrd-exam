package com.kshrd.exam_api.controller.request;

public class QuestionFilterChecking {

    private int questionId;
    private String question;
    int questionType_id;
    private String image;
    private String questionType;
    Object  answer;
    private String isCorrect;
    private int point;

    public QuestionFilterChecking() {
    }

    public QuestionFilterChecking(int questionId, String question, int questionType_id, String image, String questionType, Object answer, String isCorrect, int point) {
        this.questionId = questionId;
        this.question = question;
        this.questionType_id = questionType_id;
        this.image = image;
        this.questionType = questionType;
        this.answer = answer;
        this.isCorrect = isCorrect;
        this.point = point;
    }

    public int getQuestionId() {
        return questionId;
    }

    public void setQuestionId(int questionId) {
        this.questionId = questionId;
    }

    public String getQuestion() {
        return question;
    }

    public void setQuestion(String question) {
        this.question = question;
    }

    public int getQuestionType_id() {
        return questionType_id;
    }

    public void setQuestionType_id(int questionType_id) {
        this.questionType_id = questionType_id;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getQuestionType() {
        return questionType;
    }

    public void setQuestionType(String questionType) {
        this.questionType = questionType;
    }

    public Object getAnswer() {
        return answer;
    }

    public void setAnswer(Object answer) {
        this.answer = answer;
    }

    public String getIsCorrect() {
        return isCorrect;
    }

    public void setIsCorrect(String isCorrect) {
        this.isCorrect = isCorrect;
    }

    public int getPoint() {
        return point;
    }

    public void setPoint(int point) {
        this.point = point;
    }

    @Override
    public String toString() {
        return "QuestionFilterChecking{" +
                "questionId=" + questionId +
                ", question='" + question + '\'' +
                ", questionType_id=" + questionType_id +
                ", image='" + image + '\'' +
                ", questionType='" + questionType + '\'' +
                ", answer=" + answer +
                ", isCorrect='" + isCorrect + '\'' +
                ", point=" + point +
                '}';
    }
}
