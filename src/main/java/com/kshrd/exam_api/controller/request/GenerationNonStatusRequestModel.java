package com.kshrd.exam_api.controller.request;

public class GenerationNonStatusRequestModel {

    private int id;
    private String generation;

    public GenerationNonStatusRequestModel() {
    }

    public GenerationNonStatusRequestModel(int id, String generation) {
        this.id = id;
        this.generation = generation;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getGeneration() {
        return generation;
    }

    public void setGeneration(String generation) {
        this.generation = generation;
    }

    @Override
    public String toString() {
        return "GenerationNonStatusRequestModel{" +
                "id=" + id +
                ", generation='" + generation + '\'' +
                '}';
    }
}
