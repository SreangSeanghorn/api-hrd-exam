package com.kshrd.exam_api.controller.request;

public class QuestionTypeRequestDto {
    private String questionType;

    public QuestionTypeRequestDto() {
    }

    public QuestionTypeRequestDto(String questionType) {
        this.questionType = questionType;
    }

    public String getQuestionType() {
        return questionType;
    }

    public void setQuestionType(String questionType) {
        this.questionType = questionType;
    }

    @Override
    public String toString() {
        return "QuestionTypeRequestDto{" +
                "questionType='" + questionType + '\'' +
                '}';
    }
}
