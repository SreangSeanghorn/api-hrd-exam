package com.kshrd.exam_api.controller.request;

import io.swagger.annotations.ApiModelProperty;

public class ScoreRequest {
    @ApiModelProperty(required = true, position = 0)
    private float score;
    @ApiModelProperty(required = true, position = 1)
    private boolean status = true;

    public ScoreRequest() {
    }

    public ScoreRequest(float score) {
        this.score = score;
    }

    public float getScore() {
        return score;
    }

    public void setScore(float score) {
        this.score = score;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return "ScoreRequest{" +
                "score=" + score +
                ", status=" + status +
                '}';
    }
}
