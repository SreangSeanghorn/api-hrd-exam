package com.kshrd.exam_api.controller.request;

public class RoleRequestDto {
    String role;

    public RoleRequestDto() {
    }

    public RoleRequestDto(String role) {
        this.role = role;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    @Override
    public String toString() {
        return "RoleRequestModel{" +
                "role='" + role + '\'' +
                '}';
    }
}
