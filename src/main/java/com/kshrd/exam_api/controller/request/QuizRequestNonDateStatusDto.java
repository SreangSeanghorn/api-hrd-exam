package com.kshrd.exam_api.controller.request;

import com.fasterxml.jackson.annotation.JsonFormat;

import java.util.Date;

public class QuizRequestNonDateStatusDto {

    private String title;
    private String subject;
    private int duration;
    private boolean status;

    public QuizRequestNonDateStatusDto() {
    }

    public QuizRequestNonDateStatusDto(String title, String subject, int duration, boolean status) {
        this.title = title;
        this.subject = subject;
        this.duration = duration;
        this.status = status;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public int getDuration() {
        return duration;
    }

    public void setDuration(int duration) {
        this.duration = duration;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }


    @Override
    public String toString() {
        return "QuizRequestNonDateStatusDto{" +
                "title='" + title + '\'' +
                ", subject='" + subject + '\'' +
                ", duration=" + duration +
                ", status=" + status +
                '}';
    }
}
