package com.kshrd.exam_api.controller.response.messages;

public enum Resources {

    RESULTS("Results"),
    STUDENTS("Students"),
    STUDENT("Student"),
    TEACHERS("Teachers"),
    TEACHER("Teachers"),
    QUIZZES("Quizzes"),
    QUIZ("Quiz"),
    ROLE("Role"),
    QUESTIONS("Questions"),
    QUESTION_TYPE("Question_Type"),
    ANSWERS("Answers"),
    INSTRUCTION("Instructions"),
    INSTRUCTION_ONE("Instruction"),
    GENERATION("Generations"),
    GENERATION_ONE("Generation"),
    QUIZ_ID("QuizID"),
    Q_DURATION("Duration"),
    GENERATION_ID("GenerationID"),
    TEACHER_ID("TeacherID"),
    Q_TITLE("Title"),
    Q_SUBJECT("Subject");

    private String name;

    Resources(String name){
        this.name = name;
    }
    public String getName(){
        return name;
    }
}
