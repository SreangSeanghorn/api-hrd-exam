package com.kshrd.exam_api.controller.response;

import com.kshrd.exam_api.controller.request.QuestionFilterChecking;
import io.swagger.annotations.ApiModelProperty;

import java.util.List;

public class InstructionFilterResponse {
    @ApiModelProperty(required = true, position = 0)
    private int instructionId;
    @ApiModelProperty(required = true, position = 1)
    private String instruction;
    @ApiModelProperty(required = true, position = 2)
    private String title;
    @ApiModelProperty(required = true, position = 3)
    private List<QuestionFilterResponse> response;

    public InstructionFilterResponse() {
    }

    public InstructionFilterResponse(int instructionId, String instruction, String title, List<QuestionFilterResponse> response) {
        this.instructionId = instructionId;
        this.instruction = instruction;
        this.title = title;
        this.response = response;
    }

    public int getInstructionId() {
        return instructionId;
    }

    public void setInstructionId(int instructionId) {
        this.instructionId = instructionId;
    }

    public String getInstruction() {
        return instruction;
    }

    public void setInstruction(String instruction) {
        this.instruction = instruction;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public List<QuestionFilterResponse> getResponse() {
        return response;
    }

    public void setResponse(List<QuestionFilterResponse> response) {
        this.response = response;
    }

    @Override
    public String toString() {
        return "InstructionFilterResponse{" +
                "instructionId=" + instructionId +
                ", instruction='" + instruction + '\'' +
                ", title='" + title + '\'' +
                ", response=" + response +
                '}';
    }
}
