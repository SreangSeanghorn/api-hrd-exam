package com.kshrd.exam_api.controller.response;

public class JwtResponse {
    private String message;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    private final String jwttoken;

    public JwtResponse(String message, String jwttoken) {
        this.message = message;
        this.jwttoken = jwttoken;
    }

    public JwtResponse(String jwttoken) {
        this.jwttoken = jwttoken;
    }
    public String getToken() {
        return this.jwttoken;
    }

    @Override
    public String toString() {
        return "JwtResponse{" +
                "message='" + message + '\'' +
                ", jwttoken='" + jwttoken + '\'' +
                '}';
    }
}
