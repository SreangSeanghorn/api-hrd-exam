package com.kshrd.exam_api.controller.response;

import io.swagger.annotations.ApiModelProperty;

public class QuestionResponseDto {
    @ApiModelProperty(required = true, position = 0)
    private String question;
    @ApiModelProperty(required = true, position = 1)
    private String image;
    @ApiModelProperty(required = true, position = 2)
    private int questionTypeId;
    @ApiModelProperty(required = true, position = 3)
    private int instructionId;
    @ApiModelProperty(required = true, position = 4)
    private int answerId;
    @ApiModelProperty(required = true, position = 5)
    private int point;

    public QuestionResponseDto() {
    }

    public QuestionResponseDto(String question, String image, int questionTypeId, int instructionId, int answerId, int point) {
        this.question = question;
        this.image = image;
        this.questionTypeId = questionTypeId;
        this.instructionId = instructionId;
        this.answerId = answerId;
        this.point = point;
    }

    public int getPoint() {
        return point;
    }

    public void setPoint(int point) {
        this.point = point;
    }

    public String getQuestion() {
        return question;
    }

    public void setQuestion(String question) {
        this.question = question;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public int getQuestionTypeId() {
        return questionTypeId;
    }

    public void setQuestionTypeId(int questionTypeId) {
        this.questionTypeId = questionTypeId;
    }

    public int getInstructionId() {
        return instructionId;
    }

    public void setInstructionId(int instructionId) {
        this.instructionId = instructionId;
    }

    public int getAnswerId() {
        return answerId;
    }

    public void setAnswerId(int answerId) {
        this.answerId = answerId;
    }

    @Override
    public String toString() {
        return "QuestionResponseDto{" +
                "question='" + question + '\'' +
                ", image='" + image + '\'' +
                ", questionTypeId=" + questionTypeId +
                ", instructionId=" + instructionId +
                ", answerId=" + answerId +
                ", point=" + point +
                '}';
    }
}
