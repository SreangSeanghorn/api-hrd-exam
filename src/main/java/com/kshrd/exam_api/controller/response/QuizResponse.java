package com.kshrd.exam_api.controller.response;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.kshrd.exam_api.repository.model.GenerationDto;
import com.kshrd.exam_api.repository.model.TeacherDto;

import java.util.Date;

public class QuizResponse {
    private int quizId;
    private String title;
    private String subject;
    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date date;
    private int duration;
    private Boolean status;
    private int  generationId;
    private int teacherId;

    public QuizResponse() {
    }

    public QuizResponse(int quizId, String title, String subject, Date date, int duration, Boolean status, int generationId, int teacherId) {
        this.quizId = quizId;
        this.title = title;
        this.subject = subject;
        this.date = date;
        this.duration = duration;
        this.status = status;
        this.generationId = generationId;
        this.teacherId = teacherId;
    }

    public int getQuizId() {
        return quizId;
    }

    public void setQuizId(int quizId) {
        this.quizId = quizId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public int getDuration() {
        return duration;
    }

    public void setDuration(int duration) {
        this.duration = duration;
    }

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public int getGenerationId() {
        return generationId;
    }

    public void setGenerationId(int generationId) {
        this.generationId = generationId;
    }

    public int getTeacherId() {
        return teacherId;
    }

    public void setTeacherId(int teacherId) {
        this.teacherId = teacherId;
    }

    @Override
    public String toString() {
        return "QuizResponse{" +
                "quizId=" + quizId +
                ", title='" + title + '\'' +
                ", subject='" + subject + '\'' +
                ", date=" + date +
                ", duration=" + duration +
                ", status=" + status +
                ", generationId=" + generationId +
                ", teacherId=" + teacherId +
                '}';
    }
}
