package com.kshrd.exam_api.controller.response;

import com.kshrd.exam_api.repository.model.RoleDto;

import java.util.List;

public class TeacherResponse {
    private int teacherId;
    private String name;
    List<RoleResponse> roleDtos;
    public TeacherResponse() {
    }

    public TeacherResponse(int teacherId, String name,List<RoleResponse> roleDtos) {
        this.teacherId = teacherId;
        this.name = name;
        this.roleDtos = roleDtos;
    }

    public TeacherResponse(int id, String name, String gender, Boolean status) {
        this.teacherId = teacherId;
        this.name = name;
    }

    public List<RoleResponse> getRoleDtos() {
        return roleDtos;
    }

    public void setRoleDtos(List<RoleResponse> roleDtos) {
        this.roleDtos = roleDtos;
    }

    public int getId() {
        return teacherId;
    }

    public void setId(int id) {
        this.teacherId = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }



    @Override
    public String toString() {
        return "TeacherResponse{" +
                "id=" + teacherId +
                ", name='" + name + '\'' +
                '}';
    }
}
