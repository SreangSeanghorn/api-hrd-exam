package com.kshrd.exam_api.controller.response;

import com.kshrd.exam_api.controller.request.ResultJsonContainer;
import com.kshrd.exam_api.repository.model.StudentDto;

public class ResultListResponse {
    private int quiz_id;
    private int result_id;
    private ResultJsonContainer result;
    private int score;
    private int status;
    private StudentDto student;

    public ResultListResponse(int quiz_id, int result_id, ResultJsonContainer result, int score, int status, StudentDto student) {
        this.quiz_id = quiz_id;
        this.result_id = result_id;
        this.result = result;
        this.score = score;
        this.status = status;
        this.student = student;
    }

    public ResultListResponse() {
    }

    public int getResult_id() {
        return result_id;
    }

    public void setResult_id(int result_id) {
        this.result_id = result_id;
    }

    public ResultJsonContainer getResult() {
        return result;
    }

    public void setResult(ResultJsonContainer result) {
        this.result = result;
    }

    public int getScore() {
        return score;
    }

    public void setScore(int score) {
        this.score = score;
    }

    public StudentDto getStudent() {
        return student;
    }

    public void setStudent(StudentDto student) {
        this.student = student;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public int getQuiz_id() {
        return quiz_id;
    }

    public void setQuiz_id(int quiz_id) {
        this.quiz_id = quiz_id;
    }

    @Override
    public String toString() {
        return "ResultListResponse{" +
                "quiz_id=" + quiz_id +
                ", result_id=" + result_id +
                ", result=" + result +
                ", score=" + score +
                ", status=" + status +
                ", student=" + student +
                '}';
    }
}
