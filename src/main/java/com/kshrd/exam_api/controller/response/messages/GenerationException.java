package com.kshrd.exam_api.controller.response.messages;

public class GenerationException {

    public enum Error{

        GENERATION_ERROR("Generation cannot be null."),
        STATUS_ERROR("Status cannot be null.");

        private  String message;

        Error(String message) {
            this.message = message;
        }

        public String getMessage() {
            return message;
        }
    }

}