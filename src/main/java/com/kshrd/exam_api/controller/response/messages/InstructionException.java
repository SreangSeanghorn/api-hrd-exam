package com.kshrd.exam_api.controller.response.messages;

public class InstructionException {

    public enum Error{

        INSTRUCTION_ERROR("Instruction cannot be null."),
        QUIZID_ERROR("QuizID cannot be null.");

        private  String message;

        Error(String message) {
            this.message = message;
        }

        public String getMessage() {
            return message;
        }
    }

}