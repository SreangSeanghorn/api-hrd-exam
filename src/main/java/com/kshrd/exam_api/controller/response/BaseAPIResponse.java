package com.kshrd.exam_api.controller.response;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonInclude;
import org.springframework.http.HttpStatus;

import java.sql.Timestamp;


public class BaseAPIResponse<T> {
    private String message;
    @JsonInclude(JsonInclude.Include.NON_NULL) //data can null.
    private T data;

    private HttpStatus status;
    @JsonInclude(JsonInclude.Include.NON_NULL) //timestamp can null.
    @JsonFormat(pattern="yyyy-MM-dd")
    private Timestamp timestamp;
    @JsonInclude(JsonInclude.Include.NON_NULL) //timestamp can null.
    private int code;

    public BaseAPIResponse() {
    }

    public BaseAPIResponse(String message, T data, HttpStatus status, Timestamp timestamp, int code) {
        this.message = message;
        this.data = data;
        this.status = status;
        this.timestamp = timestamp;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }

    public HttpStatus getStatus() {
        return status;
    }

    public void setStatus(HttpStatus status) {
        this.status = status;
    }

    public Timestamp getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Timestamp timestamp) {
        this.timestamp = timestamp;
    }


    @Override
    public String toString() {
        return "BaseAPIResponse{" +
                "message='" + message + '\'' +
                ", data=" + data +
                ", status=" + status +
                ", timestamp=" + timestamp +
                ", code=" + code +
                '}';
    }
}
