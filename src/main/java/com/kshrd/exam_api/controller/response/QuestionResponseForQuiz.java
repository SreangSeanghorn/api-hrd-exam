package com.kshrd.exam_api.controller.response;

import io.swagger.annotations.ApiModelProperty;

public class QuestionResponseForQuiz {
    @ApiModelProperty(required = true, position = 0)
    private int questionId;
    @ApiModelProperty(required = true, position = 1)
    private String question;
    @ApiModelProperty(required = true, position = 2)
    int questionType_id;
    @ApiModelProperty(required = true, position = 3)
    private String image;
    @ApiModelProperty(required = true, position = 4)
    private String questionType;
    @ApiModelProperty(required = true, position = 5)
    Object  answer;
    @ApiModelProperty(required = true, position = 6)
    private int qPoint;

    public QuestionResponseForQuiz() {
    }

    public QuestionResponseForQuiz(int questionId, String question, int questionType_id, String image, String questionType, Object answer, int qPoint) {
        this.questionId = questionId;
        this.question = question;
        this.questionType_id = questionType_id;
        this.image = image;
        this.questionType = questionType;
        this.answer = answer;
        this.qPoint = qPoint;
    }

    public int getQuestionId() {
        return questionId;
    }

    public void setQuestionId(int questionId) {
        this.questionId = questionId;
    }

    public String getQuestion() {
        return question;
    }

    public void setQuestion(String question) {
        this.question = question;
    }

    public int getQuestionType_id() {
        return questionType_id;
    }

    public void setQuestionType_id(int questionType_id) {
        this.questionType_id = questionType_id;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getQuestionType() {
        return questionType;
    }

    public void setQuestionType(String questionType) {
        this.questionType = questionType;
    }

    public Object getAnswer() {
        return answer;
    }

    public void setAnswer(Object answer) {
        this.answer = answer;
    }

    public int getqPoint() {
        return qPoint;
    }

    public void setqPoint(int qPoint) {
        this.qPoint = qPoint;
    }

    @Override
    public String toString() {
        return "QuestionResponseForQuiz{" +
                "questionId=" + questionId +
                ", question='" + question + '\'' +
                ", questionType_id=" + questionType_id +
                ", image='" + image + '\'' +
                ", questionType='" + questionType + '\'' +
                ", answer=" + answer +
                ", qPoint=" + qPoint +
                '}';
    }
}
