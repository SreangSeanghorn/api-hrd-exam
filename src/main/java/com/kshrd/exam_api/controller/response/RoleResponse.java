package com.kshrd.exam_api.controller.response;

public class RoleResponse {
    int roleId;
    String role;

    public RoleResponse() {
    }

    public RoleResponse(int roleId, String role) {
        this.roleId = roleId;
        this.role = role;
    }

    public int getRoleId() {
        return roleId;
    }

    public void setRoleId(int roleId) {
        this.roleId = roleId;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    @Override
    public String toString() {
        return "RoleResponse{" +
                "roleId=" + roleId +
                ", role='" + role + '\'' +
                '}';
    }
}
