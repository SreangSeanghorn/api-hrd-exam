package com.kshrd.exam_api.controller.response;

public class AnswerObject {
    String answer;
    String ans;

    public AnswerObject() {
    }

    public AnswerObject(String answer, String ans) {
        this.answer = answer;
        this.ans = ans;
    }

    public String getAnswer() {
        return answer;
    }

    public void setAnswer(String answer) {
        this.answer = answer;
    }

    public String getAns() {
        return ans;
    }

    public void setAns(String ans) {
        this.ans = ans;
    }

    @Override
    public String toString() {
        return "AnswerObject{" +
                "answer='" + answer + '\'' +
                ", ans='" + ans + '\'' +
                '}';
    }
}
