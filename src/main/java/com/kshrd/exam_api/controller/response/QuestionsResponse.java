package com.kshrd.exam_api.controller.response;

import com.kshrd.exam_api.repository.model.InstructionDto;
import com.kshrd.exam_api.repository.model.MultiAnswersDto;
import com.kshrd.exam_api.repository.model.OptionDto;
import com.kshrd.exam_api.repository.model.QuestionTypeDto;

public class QuestionsResponse {

    private int questionId;
    private String question;
    private String image;
    private InstructionDto instruction;
    private QuestionTypeDto questionType;
    private OptionDto answer;
    private int point;

    public QuestionsResponse() {
    }

    public QuestionsResponse(int questionId, String question, String image, InstructionDto instruction, QuestionTypeDto questionType, OptionDto answer, int point) {
        this.questionId = questionId;
        this.question = question;
        this.image = image;
        this.instruction = instruction;
        this.questionType = questionType;
        this.answer = answer;
        this.point = point;
    }

    public int getQuestionId() {
        return questionId;
    }

    public void setQuestionId(int questionId) {
        this.questionId = questionId;
    }

    public String getQuestion() {
        return question;
    }

    public void setQuestion(String question) {
        this.question = question;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public InstructionDto getInstruction() {
        return instruction;
    }

    public void setInstruction(InstructionDto instruction) {
        this.instruction = instruction;
    }

    public QuestionTypeDto getQuestionType() {
        return questionType;
    }

    public void setQuestionType(QuestionTypeDto questionType) {
        this.questionType = questionType;
    }

    public OptionDto getAnswer() {
        return answer;
    }

    public void setAnswer(OptionDto answer) {
        this.answer = answer;
    }

    public int getPoint() {
        return point;
    }

    public void setPoint(int point) {
        this.point = point;
    }

    @Override
    public String toString() {
        return "QuestionsResponse{" +
                "questionId=" + questionId +
                ", question='" + question + '\'' +
                ", image='" + image + '\'' +
                ", instruction=" + instruction +
                ", questionType=" + questionType +
                ", answer=" + answer +
                ", point=" + point +
                '}';
    }
}
