package com.kshrd.exam_api.controller.response;

import com.kshrd.exam_api.controller.request.QuestionFilterChecking;

import java.util.List;

public class InstructionCheckingResponse {
    private int instructionId;

    public InstructionCheckingResponse() {
    }

    public InstructionCheckingResponse(int instructionId, String instruction, String title, List<QuestionFilterChecking> response) {
        this.instructionId = instructionId;
    }

    public int getInstructionId() {
        return instructionId;
    }

    public void setInstructionId(int instructionId) {
        this.instructionId = instructionId;
    }



    @Override
    public String toString() {
        return "InstructionCheckingResponse{" +
                "instructionId=" + instructionId +
                '}';
    }
}
