package com.kshrd.exam_api.controller.response;

public class TeacherUpdateResponse {
    private int teacherId;
    private String name;
    private  Boolean status;
    public TeacherUpdateResponse() {
    }

    public TeacherUpdateResponse(int teacherId, String name, String gender, Boolean status) {
        this.teacherId = teacherId;
        this.name = name;
        this.status = status;
    }

    public int getTeacherId() {
        return teacherId;
    }

    public void setTeacherId(int teacherId) {
        this.teacherId = teacherId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }


    public TeacherUpdateResponse(Boolean status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return "TeacherUpdateResponse{" +
                "teacherId=" + teacherId +
                ", name='" + name + '\'' +
                ", status=" + status +
                '}';
    }
}
