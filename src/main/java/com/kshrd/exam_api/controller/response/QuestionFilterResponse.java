package com.kshrd.exam_api.controller.response;

import com.kshrd.exam_api.controller.request.Ansoption;
import io.swagger.annotations.ApiModelProperty;

import java.util.Comparator;
import java.util.List;

public class QuestionFilterResponse implements Comparable<QuestionFilterResponse> {

    @ApiModelProperty(required = true, position = 0)
    private int questionId;
    @ApiModelProperty(required = true, position = 1)
    private String question;
    @ApiModelProperty(required = true, position = 2)
    int questionType_id;
    @ApiModelProperty(required = true, position = 3)
    private String image;
    @ApiModelProperty(required = true, position = 4)
    private String questionType;
    @ApiModelProperty(required = true, position = 5)
    int  answerId;
    @ApiModelProperty(required = true, position = 6)
    Object  answer;
    @ApiModelProperty(required = true, position = 7)
    private int point;
    private int qPoint;

    public QuestionFilterResponse() {
    }

    public QuestionFilterResponse(int questionId, String question, int questionType_id, String image, String questionType, int answerId, Object answer, int point, int qPoint) {
        this.questionId = questionId;
        this.question = question;
        this.questionType_id = questionType_id;
        this.image = image;
        this.questionType = questionType;
        this.answerId = answerId;
        this.answer = answer;
        this.point = point;
        this.qPoint = qPoint;
    }

    public int getQuestionType_id() {
        return questionType_id;
    }

    public void setQuestionType_id(int questionType_id) {
        this.questionType_id = questionType_id;
    }

    public int getQuestionId() {
        return questionId;
    }

    public void setQuestionId(int questionId) {
        this.questionId = questionId;
    }

    public String getQuestion() {
        return question;
    }

    public void setQuestion(String question) {
        this.question = question;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getQuestionType() {
        return questionType;
    }

    public void setQuestionType(String questionType) {
        this.questionType = questionType;
    }

    public Object getAnswer() {
        return answer;
    }

    public void setAnswer(Object answer) {
        this.answer = answer;
    }

    public int getPoint() {
        return point;
    }

    public void setPoint(int point) {
        this.point = point;
    }

    public int getqPoint() {
        return qPoint;
    }

    public void setqPoint(int qPoint) {
        this.qPoint = qPoint;
    }

    public int getAnswerId() {
        return answerId;
    }

    public void setAnswerId(int answerId) {
        this.answerId = answerId;
    }

    @Override
    public String toString() {
        return "QuestionFilterResponse{" +
                "questionId=" + questionId +
                ", question='" + question + '\'' +
                ", questionType_id=" + questionType_id +
                ", image='" + image + '\'' +
                ", questionType='" + questionType + '\'' +
                ", answerId=" + answerId +
                ", answer=" + answer +
                ", point=" + point +
                ", qPoint=" + qPoint +
                '}';
    }

    @Override
    public int compareTo(QuestionFilterResponse questionFilterResponse) {
        System.out.println("Camparable to:"+this.getQuestionId()+questionFilterResponse.getQuestionId());
        System.out.println("return first:"+String.valueOf(this.getQuestionId()<questionFilterResponse.getQuestionId()?1:0));
        return this.getQuestionId()<questionFilterResponse.getQuestionId()?1:0;
    }
    public static class OrderById implements Comparator<QuestionFilterResponse>{


        @Override
        public int compare(QuestionFilterResponse t1, QuestionFilterResponse t2) {
            System.out.println("Camparator: "+t1.getQuestionId()+t2.getQuestionId());
            System.out.println("return:"+String.valueOf(t1.getQuestionId()<t2.getQuestionId()?1:0));
            return t1.getQuestionId()<t2.getQuestionId()? -1:0;

        }
    }
}
