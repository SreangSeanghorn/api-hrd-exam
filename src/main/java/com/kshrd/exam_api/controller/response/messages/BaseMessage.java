package com.kshrd.exam_api.controller.response.messages;

public class BaseMessage {
    public enum Success{
        INSERT_SUCCESS("A Record has been inserted successfully"),
        UPDATE_SUCCESS("A Record has been updated successfully"),
        SELECT_ALL_RECORD_SUCCESS("All Records have been found"),
        SELECT_ONE_RECORD_SUCCESS("A Record has been found"),
        DELETE_SUCCESS("A Record has been deleted successfully");

        private String message;

        Success(String message) {
            this.message = message;
        }

        public String getMessage() {
            return message;
        }

    }

    public enum Error{
        //ERROR("Path variable is wrong in this request. Please change it to your request."),
        SELECT_ERROR("The Record cannot be found"),
        INSERT_ERROR("Inserting has been failed"),
        UPDATE_ERROR("Updating has been failed"),
        DELETE_ERROR("Deleting has been failed");

        private  String message;

        Error(String message) {
            this.message = message;
        }

        public String getMessage() {
            return message;
        }
    }
}
