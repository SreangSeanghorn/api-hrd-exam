package com.kshrd.exam_api.controller.response;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.kshrd.exam_api.repository.model.ResultDto;

import java.util.Date;
import java.util.List;

public class ResultReponse {
    private int quizId;
    private String title;
    @JsonFormat(pattern="yyyy-MM-dd")
    private Date date;
    private String subject;
    private int teacherId;
    private  boolean is_sent;

    public ResultReponse() {
    }


    public ResultReponse(int quizId, String title, Date date, String subject, int teacherId, boolean is_sent) {
        this.quizId = quizId;
        this.title = title;
        this.date = date;
        this.subject = subject;
        this.teacherId = teacherId;
        this.is_sent = is_sent;
    }

    public int getQuizId() {
        return quizId;
    }

    public void setQuizId(int quizId) {
        this.quizId = quizId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public int getTeacherId() {
        return teacherId;
    }

    public void setTeacherId(int teacherId) {
        this.teacherId = teacherId;
    }

    public boolean isIs_sent() {
        return is_sent;
    }

    public void setIs_sent(boolean is_sent) {
        this.is_sent = is_sent;
    }

    @Override
    public String toString() {
        return "ResultReponse{" +
                "quizId=" + quizId +
                ", title='" + title + '\'' +
                ", date=" + date +
                ", subject='" + subject + '\'' +
                ", teacherId=" + teacherId +
                ", is_sent=" + is_sent +
                '}';
    }
}
