package com.kshrd.exam_api.controller.response;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.kshrd.exam_api.controller.request.QuizRequestDto;
import com.kshrd.exam_api.controller.request.ResultJsonContainer;
import com.kshrd.exam_api.repository.model.StudentDto;

import java.util.Date;
import java.util.List;

public class ResultFilterResponse {
    private int quizId;
    private String title;
    private String subject;
    @JsonFormat(pattern="yyyy-MM-dd")
    private Date date;
    private int duration;
    private boolean status;
    private int generation_id;
    private int teacher_id;
    private List<InstructionFilterResponse> instructionFilterResponses;
    private List<ResultListResponse> resultList;

    public ResultFilterResponse() {
    }

    public ResultFilterResponse(int quizId, String title, String subject, Date date, int duration, boolean status, int generation_id, int teacher_id, List<InstructionFilterResponse> instructionFilterResponses, List<ResultListResponse> resultList) {
        this.quizId = quizId;
        this.title = title;
        this.subject = subject;
        this.date = date;
        this.duration = duration;
        this.status = status;
        this.generation_id = generation_id;
        this.teacher_id = teacher_id;
        this.instructionFilterResponses = instructionFilterResponses;
        this.resultList = resultList;
    }

    public int getQuizId() {
        return quizId;
    }

    public void setQuizId(int quizId) {
        this.quizId = quizId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public int getDuration() {
        return duration;
    }

    public void setDuration(int duration) {
        this.duration = duration;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public int getGeneration_id() {
        return generation_id;
    }

    public void setGeneration_id(int generation_id) {
        this.generation_id = generation_id;
    }

    public int getTeacher_id() {
        return teacher_id;
    }

    public void setTeacher_id(int teacher_id) {
        this.teacher_id = teacher_id;
    }

    public List<InstructionFilterResponse> getInstructionFilterResponses() {
        return instructionFilterResponses;
    }

    public void setInstructionFilterResponses(List<InstructionFilterResponse> instructionFilterResponses) {
        this.instructionFilterResponses = instructionFilterResponses;
    }

    public List<ResultListResponse> getResultList() {
        return resultList;
    }

    public void setResultList(List<ResultListResponse> resultList) {
        this.resultList = resultList;
    }

    @Override
    public String toString() {
        return "ResultFilterResponse{" +
                "quizId=" + quizId +
                ", title='" + title + '\'' +
                ", subject='" + subject + '\'' +
                ", date=" + date +
                ", duration=" + duration +
                ", status=" + status +
                ", generation_id=" + generation_id +
                ", teacher_id=" + teacher_id +
                ", instructionFilterResponses=" + instructionFilterResponses +
                ", resultList=" + resultList +
                '}';
    }
}
