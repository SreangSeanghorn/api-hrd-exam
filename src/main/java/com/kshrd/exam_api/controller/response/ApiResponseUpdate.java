package com.kshrd.exam_api.controller.response;

import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.http.HttpStatus;

import java.sql.Timestamp;

public class ApiResponseUpdate<T> {

    private String message;
    private HttpStatus status;
    @JsonFormat(pattern="yyyy-MM-dd")
    private Timestamp time;

    public ApiResponseUpdate() {
    }

    public ApiResponseUpdate(String message, HttpStatus status, Timestamp time) {
        this.message = message;
        this.status = status;
        this.time = time;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public HttpStatus getStatus() {
        return status;
    }

    public void setStatus(HttpStatus status) {
        this.status = status;
    }

    public Timestamp getTime() {
        return time;
    }

    public void setTime(Timestamp time) {
        this.time = time;
    }

    @Override
    public String toString() {
        return "ApiResponseUpdate{" +
                ", message='" + message + '\'' +
                ", status=" + status +
                ", time=" + time +
                '}';
    }
}

