package com.kshrd.exam_api.controller.response;

import java.util.List;

public class CheckingStudentResponse {
    //private List<InstructionCheckingResponse> instructionFilterResponses;
    private List<ResultListResponse> resultList;

    public CheckingStudentResponse() {
    }

    public CheckingStudentResponse(List<InstructionCheckingResponse> instructionFilterResponses, List<ResultListResponse> resultList) {
        //this.instructionFilterResponses = instructionFilterResponses;
        this.resultList = resultList;
    }

//    public List<InstructionCheckingResponse> getInstructionFilterResponses() {
//        return instructionFilterResponses;
//    }
//
////    public void setInstructionFilterResponses(List<InstructionCheckingResponse> instructionFilterResponses) {
////        this.instructionFilterResponses = instructionFilterResponses;
////    }

    public List<ResultListResponse> getResultList() {
        return resultList;
    }

    public void setResultList(List<ResultListResponse> resultList) {
        this.resultList = resultList;
    }

    @Override
    public String toString() {
        return "CheckingStudentResponse{" +
                ", resultList=" + resultList +
                '}';
    }
}
