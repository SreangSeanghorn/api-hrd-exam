package com.kshrd.exam_api.controller.response;

import com.fasterxml.jackson.annotation.JsonFormat;

import java.util.Date;
import java.util.List;

public class QuizResponseFilter {
    private int quizId;
    private String title;
    private String subject;
    @JsonFormat(pattern="yyyy-MM-dd")
    private Date date;
    private int duration;
    private boolean status;
    private int generation_id;
    private int teacher_id;
    private List<InstructionFilterResponse> responses;


    public QuizResponseFilter() {
    }

    public QuizResponseFilter(int quizId, String title, String subject, Date date, int duration, boolean status, int generation_id, int teacher_id, List<InstructionFilterResponse> responses) {
        this.quizId = quizId;
        this.title = title;
        this.subject = subject;
        this.date = date;
        this.duration = duration;
        this.status = status;
        this.generation_id = generation_id;
        this.teacher_id = teacher_id;
        this.responses = responses;
    }

    public int getQuizId() {
        return quizId;
    }

    public void setQuizId(int quizId) {
        this.quizId = quizId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public int getDuration() {
        return duration;
    }

    public void setDuration(int duration) {
        this.duration = duration;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public int getGeneration_id() {
        return generation_id;
    }

    public void setGeneration_id(int generation_id) {
        this.generation_id = generation_id;
    }

    public int getTeacher_id() {
        return teacher_id;
    }

    public void setTeacher_id(int teacher_id) {
        this.teacher_id = teacher_id;
    }

    public List<InstructionFilterResponse> getResponses() {
        return responses;
    }

    public void setResponses(List<InstructionFilterResponse> responses) {
        this.responses = responses;
    }


    @Override
    public String toString() {
        return "QuizResponseFilter{" +
                "quizId=" + quizId +
                ", title='" + title + '\'' +
                ", subject='" + subject + '\'' +
                ", date='" + date + '\'' +
                ", duration=" + duration +
                ", status=" + status +
                ", generation_id=" + generation_id +
                ", teacher_id=" + teacher_id +
                ", responses=" + responses +
                '}';
    }
}
