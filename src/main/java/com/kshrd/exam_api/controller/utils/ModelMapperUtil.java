package com.kshrd.exam_api.controller.utils;

import com.kshrd.exam_api.repository.model.InstructionDto;
import com.kshrd.exam_api.repository.model.QuizDto;
import org.modelmapper.ModelMapper;
import org.modelmapper.PropertyMap;
import org.modelmapper.convention.MatchingStrategies;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;

@Component
public class ModelMapperUtil {

    public static ModelMapper mapper(){
        return new ModelMapper();
    }

}
