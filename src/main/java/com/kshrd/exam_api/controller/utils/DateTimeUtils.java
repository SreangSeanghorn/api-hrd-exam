package com.kshrd.exam_api.controller.utils;

import org.springframework.stereotype.Component;

import java.sql.Timestamp;

@Component
public class DateTimeUtils {
    public static Timestamp getCurrentTime(){
        return new Timestamp(System.currentTimeMillis());
    }
}
