package com.kshrd.exam_api.controller.rest;

import com.kshrd.exam_api.controller.request.MultipleChoiceRequestDto;
import com.kshrd.exam_api.controller.request.SingleAnswerRequestDto;
import com.kshrd.exam_api.controller.response.BaseAPIResponse;
import com.kshrd.exam_api.controller.response.messages.MessageProperties;
import com.kshrd.exam_api.controller.response.messages.Resources;
import com.kshrd.exam_api.controller.utils.DateTimeUtils;
import com.kshrd.exam_api.controller.utils.ModelMapperUtil;
import com.kshrd.exam_api.repository.model.MultiAnswersDto;
import com.kshrd.exam_api.repository.model.SingleAnswerDto;
import com.kshrd.exam_api.repository.rep.AnswerRepository;
import com.kshrd.exam_api.service.serviceInterface.AnswerService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

/**********************************************************************
 * Original Author: Dien Sereyrith
 * Created Date: 21-07-2020
 * Development Group: HRD_EXAM
 * Description: This class is described how to work with answer
 **********************************************************************/

@RestController
@CrossOrigin(origins = "*")
@RequestMapping("api/v1/answers")
public class AnswerRestController {

    private AnswerService answerService;
    private ModelMapperUtil mapperUtil;
    private MessageProperties message;
    private DateTimeUtils timeUtils;
    @Autowired
    private AnswerRepository answerRepository;

    @Autowired
    public AnswerRestController(AnswerService answerService) {
        this.answerService = answerService;
    }

    @Autowired
    public void setAnswerService(AnswerService answerService) {
        this.answerService = answerService;
    }

    @Autowired
    public void setMapperUtil(ModelMapperUtil mapperUtil) {
        this.mapperUtil = mapperUtil;
    }

    @Autowired
    public void setMessage(MessageProperties message) {
        this.message = message;
    }

    @Autowired
    public void setTimeUtils(DateTimeUtils timeUtils) {
        this.timeUtils = timeUtils;
    }

    /**********************************************************************
     * Original Author: Dien Sereyrith
     * Created Date: 21-07-2020
     * Description: create methods to work with answers table
     * Modification History : everyday
     *     Date:
     *  Developer: Dien Sereyrith
     *  TODO: - get all answers from answer table of database
     *        - post answer for multiple choice to answer table of database
     *        - post answer for fill in the gap to answer table of database
     *        - delete answer from answer table of database
     *        - update answer for multiple choice in answer table of database
     *        - update answer for fill in the gap in answer table of database
     **********************************************************************/

    /**********************************************************************
     * This method is get all answers from answer table of database
     **********************************************************************/
    @GetMapping()
    public ResponseEntity<BaseAPIResponse<Object>> findAll() {

        List<MultiAnswersDto> multiAnswersDtoList = answerService.findAll();
        List<SingleAnswerDto> singleAnswerDtoList = answerService.singleAnswers();

        List<Object> listAll = new ArrayList();
        for (int i= 0; i<multiAnswersDtoList.size(); i++){
            if (multiAnswersDtoList.get(i).getAnswer()!=null){
                listAll.add(multiAnswersDtoList.get(i));
            }else{
                listAll.add(singleAnswerDtoList.get(i));
            }
        }
        BaseAPIResponse<Object> response = new BaseAPIResponse<>();

        if (singleAnswerDtoList !=null) {
            response.setMessage(message.selected(Resources.ANSWERS.getName()));
            response.setData(listAll);
            response.setStatus(HttpStatus.OK);
            response.setTimestamp(timeUtils.getCurrentTime());
            return ResponseEntity.ok(response);
        } else {
            response.setMessage(message.selectedError(Resources.ANSWERS.getName()));
            response.setStatus(HttpStatus.NO_CONTENT);
            response.setTimestamp(timeUtils.getCurrentTime());
            return new ResponseEntity<>(response, HttpStatus.NO_CONTENT);
        }
    }

    /**********************************************************************
     * This method is post multiple choice answers into answer table of database
     **********************************************************************/
    @PostMapping("/multiple")
    public ResponseEntity<BaseAPIResponse<MultiAnswersDto>> insert(@RequestBody MultipleChoiceRequestDto multipleChoiceRequestDto){
        BaseAPIResponse<MultiAnswersDto> response = new BaseAPIResponse<>();
        ModelMapper mapper = new ModelMapper();
        MultiAnswersDto multiAnswersDto = mapper.map(multipleChoiceRequestDto, MultiAnswersDto.class);

        try{

            if(!(multiAnswersDto.getAnswer().getOption1().getAnswer().trim().isEmpty() || multiAnswersDto.getAnswer().getOption1().getIsCorrect().trim().isEmpty() ||
                    multiAnswersDto.getAnswer().getOption2().getAnswer().trim().isEmpty() || multiAnswersDto.getAnswer().getOption2().getIsCorrect().trim().isEmpty() ||
                    multiAnswersDto.getAnswer().getOption3().getAnswer().trim().isEmpty() || multiAnswersDto.getAnswer().getOption3().getIsCorrect().trim().isEmpty() ||
                    multiAnswersDto.getAnswer().getOption4().getAnswer().trim().isEmpty() || multiAnswersDto.getAnswer().getOption4().getIsCorrect().trim().isEmpty())) {

                answerService.insertMultipleChoices(multiAnswersDto);
                response.setMessage(message.inserted(Resources.ANSWERS.getName()));
                MultiAnswersDto multipleChoiceRequestDto1 = mapper.map(multiAnswersDto, MultiAnswersDto.class);
                int id=answerRepository.selectAnswerId();
                multipleChoiceRequestDto1.setAnswerId(id);
                response.setData(multipleChoiceRequestDto1);
                response.setStatus(HttpStatus.CREATED);
                response.setTimestamp(timeUtils.getCurrentTime());
                return ResponseEntity.ok(response);

            }else {
                response.setMessage(message.insertError(Resources.ANSWERS.getName()));
                response.setStatus(HttpStatus.INTERNAL_SERVER_ERROR);
                response.setTimestamp(timeUtils.getCurrentTime());
                return new ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);
            }

        }catch (Exception e){

            response.setMessage(e.getMessage());
            response.setStatus(HttpStatus.BAD_REQUEST);
            response.setTimestamp(timeUtils.getCurrentTime());
            return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
        }

    }

    /**********************************************************************
     * This method is post fill in the gap answers into answer table of database
     **********************************************************************/
    @PostMapping()
    public ResponseEntity<BaseAPIResponse<SingleAnswerDto>> insertSingleAnswer(@RequestBody SingleAnswerRequestDto singleAnswer){
        BaseAPIResponse<SingleAnswerDto> response = new BaseAPIResponse<>();
        ModelMapper mapper = new ModelMapper();
        try{

            if(!(singleAnswer.getAnswer().getAnswer()==null)){
                answerService.insertSingleAnswer(singleAnswer);
                SingleAnswerDto answerDto=mapper.map(singleAnswer,SingleAnswerDto.class);
                int sid=answerRepository.selectAnswerId();
                response.setMessage(message.inserted(Resources.ANSWERS.getName()));
                answerDto.setAnswerId(sid);
                response.setData(answerDto);
                response.setStatus(HttpStatus.CREATED);
                response.setTimestamp(timeUtils.getCurrentTime());
                return ResponseEntity.ok(response);
            }else {
                response.setMessage(message.insertError(Resources.ANSWERS.getName()));
                response.setStatus(HttpStatus.INTERNAL_SERVER_ERROR);
                response.setTimestamp(timeUtils.getCurrentTime());
                return new ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);
            }

        }catch (Exception e){
            response.setMessage(e.getMessage());
            response.setStatus(HttpStatus.BAD_REQUEST);
            response.setTimestamp(timeUtils.getCurrentTime());
            return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
        }

    }

    /**********************************************************************
     * This method is delete answer by id in answer table of database
     **********************************************************************/
    @DeleteMapping("/{id}")
    public ResponseEntity<BaseAPIResponse<Object>> delete(@PathVariable("id") int id){
        List<MultiAnswersDto> multiAnswersDtoList = answerService.findAll();
        List<SingleAnswerDto> singleAnswerDtoList = answerService.singleAnswers();


        List<Object> listAll = new ArrayList();
        for (int i= 0; i<multiAnswersDtoList.size(); i++){
            if (multiAnswersDtoList.get(i).getAnswer()!=null){
                listAll.add(multiAnswersDtoList.get(i));
            }else{
                listAll.add(singleAnswerDtoList.get(i));
            }
        }
        BaseAPIResponse<Object> response = new BaseAPIResponse<>();
        try{
            if(answerService.delete(id)){
                response.setMessage(message.deleted(Resources.ANSWERS.getName()));
                response.setData(listAll.get(id-1));
                response.setStatus(HttpStatus.OK);
                response.setTimestamp(timeUtils.getCurrentTime());
                return ResponseEntity.ok(response);
            }else {
                response.setMessage(message.deletedError(Resources.ANSWERS.getName()));
                response.setStatus(HttpStatus.NO_CONTENT);
                response.setTimestamp(timeUtils.getCurrentTime());
                return new ResponseEntity<>(response,HttpStatus.NO_CONTENT);
            }
        }catch (Exception e){
            response.setMessage(e.getMessage());
            response.setStatus(HttpStatus.BAD_REQUEST);
            response.setTimestamp(timeUtils.getCurrentTime());
            return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
        }
    }

    /**********************************************************************
     * This method is update a answer for multiple choice to answer table of database
     **********************************************************************/
    @PutMapping("/multiple/{id}")
    public ResponseEntity<BaseAPIResponse<MultipleChoiceRequestDto>> update(@PathVariable("id") int id, @RequestBody MultipleChoiceRequestDto multipleChoiceRequestDto){
        BaseAPIResponse<MultipleChoiceRequestDto> response = new BaseAPIResponse<>();
        ModelMapper mapper = new ModelMapper();
        MultiAnswersDto multiAnswersDto = mapper.map(multipleChoiceRequestDto, MultiAnswersDto.class);
        try {
            if (answerService.update(id, multiAnswersDto)){
                response.setMessage(message.updated(Resources.ANSWERS.getName()));
                response.setData(multipleChoiceRequestDto);
                response.setStatus(HttpStatus.OK);
                response.setTimestamp(timeUtils.getCurrentTime());
                return ResponseEntity.ok(response);
            }else {
                response.setMessage(message.updatedError(Resources.ANSWERS.getName()));
                response.setStatus(HttpStatus.INTERNAL_SERVER_ERROR);
                response.setTimestamp(timeUtils.getCurrentTime());
                return new ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);
            }
        }catch (Exception e){
            response.setMessage(e.getMessage());
            response.setStatus(HttpStatus.BAD_REQUEST);
            response.setTimestamp(timeUtils.getCurrentTime());
            return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
        }
    }

    /**********************************************************************
     * This method is update a answers for fill in the gap to answer table of database
     **********************************************************************/
    @PutMapping("/{id}")
    public ResponseEntity<BaseAPIResponse<SingleAnswerRequestDto>> update(@PathVariable("id") int id, @RequestBody SingleAnswerRequestDto requestDto){
        BaseAPIResponse<SingleAnswerRequestDto> response = new BaseAPIResponse<>();
        ModelMapper mapper = new ModelMapper();
        if (requestDto!=null){
            answerService.updateSingleAnswer(id,requestDto);
            response.setMessage("You have updated successfully");
            response.setData(requestDto);
            response.setStatus(HttpStatus.OK);
            response.setTimestamp(new Timestamp(System.currentTimeMillis()));
            return ResponseEntity.ok(response);
        }else {
            response.setMessage("You have update unsuccessful");
            response.setStatus(HttpStatus.NO_CONTENT);
            response.setTimestamp(new Timestamp(System.currentTimeMillis()));
            return new ResponseEntity<>(response, HttpStatus.NO_CONTENT);
        }
    }
}
