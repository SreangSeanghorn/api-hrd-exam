package com.kshrd.exam_api.controller.rest;

import com.kshrd.exam_api.controller.request.QuestionTypeRequestDto;
import com.kshrd.exam_api.controller.response.BaseAPIResponse;
import com.kshrd.exam_api.controller.response.messages.BaseMessage;
import com.kshrd.exam_api.controller.response.messages.MessageProperties;
import com.kshrd.exam_api.controller.response.messages.Resources;
import com.kshrd.exam_api.controller.utils.DateTimeUtils;
import com.kshrd.exam_api.repository.model.QuestionTypeDto;
import com.kshrd.exam_api.service.serviceImplement.QuestionTypeServiceImp;
import com.kshrd.exam_api.service.serviceInterface.QuestionTypeService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.sql.Timestamp;
import java.util.List;

/**********************************************************************
 * Original Author: Dien Sereyrith
 * Created Date: 21-07-2020
 * Development Group: HRD_EXAM
 * Description: This class is described a bout how to work with question types
 **********************************************************************/
@RestController
@RequestMapping("/api/v1/questiontypes")
public class QuestionTypeRestController {

    private QuestionTypeServiceImp questionTypeServiceImp;
    private MessageProperties message;
    private DateTimeUtils timeUtils;

    @Autowired
    public QuestionTypeRestController(QuestionTypeServiceImp questionTypeServiceImp) {
        this.questionTypeServiceImp = questionTypeServiceImp;
    }

    @Autowired
    public void setMessage(MessageProperties message) {
        this.message = message;
    }
    @Autowired
    public void setTimeUtils(DateTimeUtils timeUtils) {
        this.timeUtils = timeUtils;
    }

/**********************************************************************
 * Original Author: Dien Sereyrith
 * Created Date: 21-07-2020
 * Description: create methods to work with questions table
 * Modification History : everyday
 *     Date:
 *  Developer: Dien Sereyrith
 *  TODO: - get all questions type from question type table of database
 *        - get question type by id from question type table of database
 *        - post question type to question type table of database
 *        - delete question type from question type table of database
 *        - update question type in question type table of database
 **********************************************************************/


    /**********************************************************************
     * This method is get all question types from question type table of database
     **********************************************************************/
    @GetMapping()
    public ResponseEntity<BaseAPIResponse<List<QuestionTypeDto>>> findAll() {
        BaseAPIResponse<List<QuestionTypeDto>> response = new BaseAPIResponse<>();
        try {
            if (questionTypeServiceImp.findAll() != null) {
                List<QuestionTypeDto> questionTypeDtoList = questionTypeServiceImp.findAll();
                response.setMessage(message.selected(Resources.QUESTION_TYPE.getName()));
                response.setData(questionTypeDtoList);
                response.setStatus(HttpStatus.OK);
                response.setTimestamp(timeUtils.getCurrentTime());
                return ResponseEntity.ok(response);
            } else {
                response.setMessage(message.selectedError(Resources.QUESTION_TYPE.getName()));
                response.setStatus(HttpStatus.INTERNAL_SERVER_ERROR);
                response.setTimestamp(timeUtils.getCurrentTime());
                return new ResponseEntity<>(response, HttpStatus.NO_CONTENT);
            }
        }catch (Exception e){
            response.setMessage(e.getMessage());
            response.setStatus(HttpStatus.BAD_REQUEST);
            response.setTimestamp(timeUtils.getCurrentTime());
            return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
        }
    }

    /**********************************************************************
     * This method is get a question type by id from question type table of database
     **********************************************************************/
    @GetMapping("/{id}")
    public ResponseEntity<BaseAPIResponse<QuestionTypeDto>> findOne(@PathVariable("id") int id){
        BaseAPIResponse<QuestionTypeDto> response = new BaseAPIResponse<>();
        try{
            if (questionTypeServiceImp.findOne(id)!=null){
                response.setMessage(message.selectedOne(Resources.QUESTION_TYPE.getName()));
                response.setData(questionTypeServiceImp.findOne(id));
                response.setStatus(HttpStatus.OK);
                response.setTimestamp(timeUtils.getCurrentTime());
                return ResponseEntity.ok(response);
            }else {
                response.setMessage(message.notRecord(Resources.QUESTION_TYPE.getName()));
                response.setStatus(HttpStatus.INTERNAL_SERVER_ERROR);
                response.setTimestamp(timeUtils.getCurrentTime());
                return new ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);
            }
        }catch (Exception e){
            response.setMessage(e.getMessage());
            response.setStatus(HttpStatus.BAD_REQUEST);
            response.setTimestamp(timeUtils.getCurrentTime());
            return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
        }
    }

    /**********************************************************************
     * This method is post a question types into question type table of database
     **********************************************************************/
    @PostMapping()
    public ResponseEntity<BaseAPIResponse<QuestionTypeRequestDto>> insert(@RequestBody QuestionTypeRequestDto questionTypeRequestDto) {
        BaseAPIResponse<QuestionTypeRequestDto> response = new BaseAPIResponse<>();
        ModelMapper mapper = new ModelMapper();
        QuestionTypeDto questionTypeDto = mapper.map(questionTypeRequestDto, QuestionTypeDto.class);
        try{
            if (!questionTypeDto.getQuestionType().trim().isEmpty()) {
                questionTypeDto.setQuestionType(questionTypeDto.getQuestionType().trim());
                questionTypeServiceImp.insert(questionTypeDto);
                response.setMessage(message.inserted(Resources.QUESTION_TYPE.getName()));
                QuestionTypeRequestDto questionTypeRequestDto1 = mapper.map(questionTypeDto, QuestionTypeRequestDto.class);
                response.setData(questionTypeRequestDto1);
                response.setStatus(HttpStatus.CREATED);
                response.setTimestamp(timeUtils.getCurrentTime());
                return new ResponseEntity<>(response, HttpStatus.CREATED);
            } else {
                response.setMessage(message.insertError(Resources.QUESTION_TYPE.getName()));
                response.setStatus(HttpStatus.INTERNAL_SERVER_ERROR);
                response.setTimestamp(timeUtils.getCurrentTime());
                return new ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);
            }
        }catch (Exception e){
            response.setMessage(e.getMessage());
            response.setStatus(HttpStatus.BAD_REQUEST);
            response.setTimestamp(timeUtils.getCurrentTime());
            return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
        }
    }

    /**********************************************************************
     * This method is update a question types in question type table of database
     **********************************************************************/
    @PutMapping("/{id}")
    public ResponseEntity<BaseAPIResponse<QuestionTypeRequestDto>> update(@PathVariable("id") int id, @RequestBody QuestionTypeRequestDto questionTypeRequestDto){
        BaseAPIResponse<QuestionTypeRequestDto> response = new BaseAPIResponse<>();
        ModelMapper mapper = new ModelMapper();
        QuestionTypeDto questionTypeDto = mapper.map(questionTypeRequestDto, QuestionTypeDto.class);
        try{
            if(questionTypeServiceImp.update(id,questionTypeDto)){
                response.setMessage(message.updated(Resources.QUESTION_TYPE.getName()));
                QuestionTypeRequestDto typeRequestDto = mapper.map(questionTypeDto, QuestionTypeRequestDto.class);
                response.setData(typeRequestDto);
                response.setStatus(HttpStatus.OK);
                response.setTimestamp(timeUtils.getCurrentTime());
                return ResponseEntity.ok(response);
            }else {
                response.setMessage(message.updatedError(Resources.QUESTION_TYPE.getName()));
                response.setStatus(HttpStatus.INTERNAL_SERVER_ERROR);
                response.setTimestamp(timeUtils.getCurrentTime());
                return new ResponseEntity<>(response,HttpStatus.INTERNAL_SERVER_ERROR);
            }
        }catch (Exception e){
            response.setMessage(e.getMessage());
            response.setStatus(HttpStatus.BAD_REQUEST);
            response.setTimestamp(timeUtils.getCurrentTime());
            return new ResponseEntity<>(response,HttpStatus.BAD_REQUEST);
        }
    }

    /**********************************************************************
     * This method is delete a question types in question type table of database
     **********************************************************************/
    @DeleteMapping("/{id}")
    public ResponseEntity<BaseAPIResponse<QuestionTypeDto>> delete(@PathVariable("id") int id){
        BaseAPIResponse<QuestionTypeDto> response = new BaseAPIResponse<>();
        ModelMapper mapper = new ModelMapper();
        if(id !=0) {
            QuestionTypeDto questionTypeDto = questionTypeServiceImp.findOne(id);
            if (questionTypeDto == null) {
                response.setMessage(message.deletedError(Resources.QUESTIONS.getName()));
                response.setStatus(HttpStatus.NO_CONTENT);
                response.setTimestamp(new Timestamp(System.currentTimeMillis()));
                return ResponseEntity.ok(response);
            } else {
                if (questionTypeServiceImp.delete(id)) {
                    response.setMessage(message.deleted(Resources.QUESTIONS.getName()));
                    response.setData(questionTypeDto);
                    response.setStatus(HttpStatus.OK);
                    response.setTimestamp(new Timestamp(System.currentTimeMillis()));
                    return ResponseEntity.ok(response);
                } else {
                    response.setMessage(message.deletedError(Resources.QUESTIONS.getName()));
                    response.setStatus(HttpStatus.NO_CONTENT);
                    response.setTimestamp(new Timestamp(System.currentTimeMillis()));
                    return new ResponseEntity<>(response, HttpStatus.NO_CONTENT);
                }
            }

        }else{
            response.setMessage(message.nullValue("ID"));
            response.setStatus(HttpStatus.NO_CONTENT);
            response.setTimestamp(new Timestamp(System.currentTimeMillis()));
            return ResponseEntity.ok(response);
        }
    }
}
