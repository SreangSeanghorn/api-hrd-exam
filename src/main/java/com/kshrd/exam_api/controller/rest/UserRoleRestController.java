package com.kshrd.exam_api.controller.rest;

import com.kshrd.exam_api.controller.response.messages.BaseMessage;
import com.kshrd.exam_api.repository.model.RoleDto;
import com.kshrd.exam_api.service.serviceImplement.UserRoleServiceImp;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;


/**********************************************************************
 * Original Author:Sreang Seanghorn
 * Created Date: 21-07-2020
 * Development Group: HRD_EXAM
 * Description:
 **********************************************************************/

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("${baseUrl}")
public class UserRoleRestController {
    private UserRoleServiceImp serviceImp;
    @Autowired
    public void setServiceImp(UserRoleServiceImp serviceImp) {
        this.serviceImp = serviceImp;
    }

    /**********************************************************************
     * Original Author:Sreang Seanghorn
     * Created Date: 21-07-2020
     * Description:
     * Modification History :
     *     Date:
     *  Developer:
     *  TODO:
     **********************************************************************/

    /**********************************************************************
     *
     **********************************************************************/
    @PutMapping("/user-role/{teacher-id}/{roleDto-id}")
    ResponseEntity<String> updateRole(@PathVariable("teacher-id") int teacher_id, @PathVariable("role-id") int role_id, @RequestBody RoleDto roleDto){
        try {
            serviceImp.updateRole(teacher_id,role_id, roleDto);
            return ResponseEntity.ok("message"+BaseMessage.Success.UPDATE_SUCCESS.getMessage());
        }catch (Exception ex){
            return ResponseEntity.ok("message"+BaseMessage.Error.UPDATE_ERROR.getMessage());
        }


    }

    /**********************************************************************
     *
     **********************************************************************/
    @PostMapping("/user-role/{teacher-id}")
    ResponseEntity<String> insert(@PathVariable("teacher-id") int teacher_id, @RequestBody RoleDto roleDto){
        try {
            serviceImp.insertUserRole(teacher_id, roleDto);
            return ResponseEntity.ok("message"+BaseMessage.Success.INSERT_SUCCESS.getMessage());
        }catch (Exception ex){
            return ResponseEntity.ok("message"+BaseMessage.Error.INSERT_ERROR.getMessage());
        }
    }

    /**********************************************************************
     *
     **********************************************************************/
    @DeleteMapping("/user-role/{teacher-id}/{role-id}")
    ResponseEntity<String> deleteRole(@PathVariable("teacher-id") int teacher_id, @PathVariable("role-id") int role_id){
        try {
            serviceImp.deleteUserRole(teacher_id,role_id);
            return ResponseEntity.ok("message"+BaseMessage.Success.DELETE_SUCCESS.getMessage());
        }catch (Exception ex){
            return ResponseEntity.ok("message"+BaseMessage.Error.DELETE_ERROR.getMessage());
        }


    }
}

