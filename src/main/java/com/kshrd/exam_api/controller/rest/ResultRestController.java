package com.kshrd.exam_api.controller.rest;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.kshrd.exam_api.configuration.JsonUploadFile;

import com.kshrd.exam_api.controller.request.*;
import com.kshrd.exam_api.controller.response.*;

import com.kshrd.exam_api.controller.request.ControllerQuestion;
import com.kshrd.exam_api.controller.request.InstructionDetailRequest;
import com.kshrd.exam_api.controller.request.ResultRequestDto;
import com.kshrd.exam_api.controller.request.ScoreRequest;
import com.kshrd.exam_api.controller.response.APIResponse;
import com.kshrd.exam_api.controller.response.BaseAPIResponse;
import com.kshrd.exam_api.controller.response.ResultReponse;
import com.kshrd.exam_api.controller.response.messages.MessageProperties;
import com.kshrd.exam_api.controller.response.messages.Resources;
import com.kshrd.exam_api.controller.utils.DateTimeUtils;
import com.kshrd.exam_api.controller.utils.ModelMapperUtil;
import com.kshrd.exam_api.controller.utils.Paging;
import com.kshrd.exam_api.repository.model.ResultDto;
import com.kshrd.exam_api.repository.model.ScoreDto;
import com.kshrd.exam_api.repository.rep.QuizRepository;
import com.kshrd.exam_api.repository.rep.ResultRepository;
import com.kshrd.exam_api.service.serviceImplement.ResultServiceImp;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**********************************************************************
 * Original Author: Lon Dara
 * Created Date: 21-07-2020
 * Development Group: HRD_EXAM
 * Description: This class control results when students submit.
 **********************************************************************/
@CrossOrigin(origins = "*")
@RestController
@RequestMapping("${baseUrl}/results")
public class ResultRestController {
    private ModelMapperUtil mapperUtil;
    private DateTimeUtils timeUtils;
    private ResultServiceImp service;
    private MessageProperties message;
    ModelMapper mapper=new ModelMapper();
    @Autowired
    private ResultRepository resultRepository;
    @Autowired
    private QuizRepository quizRepository;
    @Autowired
    public void setService(ResultServiceImp service) {
        this.service = service;
    }
    @Autowired
    public void setMapperUtil(ModelMapperUtil mapperUtil) {
        this.mapperUtil = mapperUtil;
    }
    @Autowired
    public void setTimeUtils(DateTimeUtils timeUtils) {
        this.timeUtils = timeUtils;
    }
    @Autowired
    public void setMessage(MessageProperties message) {
        this.message = message;
    }


    /**********************************************************************
     * Original Author: Lon Dara
     * Created Date: 21-07-2020
     * Description: post students to database
     * Modification History : all day
     *     Date:
     *  Developer:Lon Dara
     *  TODO: - post result students to Data base
     *        - get result result by Id of students
     *        - update result of students.
     *        - delete result from data base.
     **********************************************************************/

    /**********************************************************************
     * This method it used to submit result of students to database.
     **********************************************************************/


    @PostMapping()
    public ResponseEntity<BaseAPIResponse<ResultRequest>> insert(@RequestBody ResultRequest results){


        BaseAPIResponse<ResultRequest> response = new BaseAPIResponse<>();
        List<ResultDto> resultDtos=new ArrayList<>();
        boolean status=true;
        try{
            resultDtos=service.select();
            if(results.getStudent_id() == 0 || "".equals(results.getStudent_id())){
                response.setMessage(message.nullValue("StudentID"));
                response.setStatus(HttpStatus.BAD_REQUEST);
            }
            else{
                for (int i=0;i<resultDtos.size();i++){
                    if(results.getStudent_id()==resultDtos.get(i).getStudent().getStudentId()
                            &&results.getQuiz_id()==resultDtos.get(i).getQuiz_id()){
                        response.setMessage("Cannot submitted the result twice");
                        response.setStatus(HttpStatus.CONFLICT);
                        status=false;
                    }
                }
                if(status==true){
                    //System.out.println("result in rest:"+ results.getInstructionFilterResponses().getResponse().get(0).getAnswer());

                    if(service.insertResult(results)) {
                        response.setMessage(message.inserted(Resources.RESULTS.getName()));
                        response.setData(results);
                        response.setStatus(HttpStatus.OK);
                    }else{
                        response.setMessage(message.selectedError(Resources.RESULTS.getName()));
                        response.setStatus(HttpStatus.BAD_REQUEST);
                    }
                }

            }
            response.setTimestamp(timeUtils.getCurrentTime());
            return ResponseEntity.ok(response);
        }catch (Exception e){
            response.setMessage(e.getMessage());
            response.setStatus(HttpStatus.NOT_FOUND);
            response.setTimestamp(timeUtils.getCurrentTime());
            return ResponseEntity.ok(response);
        }

    }


    @GetMapping("/classname/{quiz_id}")
    public ResponseEntity<APIResponse<List<String>>> selectClass(@PathVariable int quiz_id){

        APIResponse<List<String>> response = new APIResponse<>();
        List<String> className=resultRepository.selectClassName(quiz_id);
        response.setData(className);
        response.setMessage("All class has been found successfully");
        response.setStatus(HttpStatus.OK);
        response.setTimestamp(DateTimeUtils.getCurrentTime());
        return ResponseEntity.ok(response);
    }


    /**********************************************************************
     * This method it quizz that have the result submitted.
     **********************************************************************/
    @GetMapping("/quizzes")
    public ResponseEntity<APIResponse<List<ResultReponse>>> selectQuizzes(Paging page){

        APIResponse<List<ResultReponse>> response = new APIResponse<>();
        List<ResultReponse> resultsDtos = new ArrayList<>();
        resultsDtos=service.selectAssignQuiz(page);
        if(resultsDtos.size() == 0){
            response.setMessage(message.selectedError(Resources.QUIZ.getName()));
            response.setStatus(HttpStatus.NOT_FOUND);
        }else{
            response.setMessage(message.selected(Resources.QUIZ.getName()));
            response.setData(resultsDtos);
            response.setPaging(page);
            response.setStatus(HttpStatus.OK);
        }
        response.setTimestamp(DateTimeUtils.getCurrentTime());
        return ResponseEntity.ok(response);
    }

    /**********************************************************************
     * This method it quizz that have the result submitted.
     **********************************************************************/


    /**********************************************************************
     * This method it used to get  results by Quiz
     **********************************************************************/
    @GetMapping("/quiz/{quiz_id}")
    public ResponseEntity<BaseAPIResponse<List<ResultDto>>> selectByQuiz(@PathVariable int quiz_id){

        BaseAPIResponse<List<ResultDto>> response = new BaseAPIResponse<>();
        List<ResultDto> resultsDtos = new ArrayList<>();
        List<ResultJsonContainer> resultStore = new ArrayList<>();


        JsonUploadFile file = new JsonUploadFile();
        ResultJsonContainer resultConvert = new ResultJsonContainer();
        ObjectMapper objectMapper = new ObjectMapper();

        boolean isSelected = true;

        try {
            List<String> results = resultRepository.resultByQuizId(quiz_id);
            resultsDtos = service.selectResultByQuiz(quiz_id);
            if(results.size()==0){
                isSelected = false;
            }else{
                file.deleted();
                for(Object res : results){
                    file.writeFile(res);
                }
                BufferedReader reader;
                try {
                    reader = new BufferedReader(new FileReader("filejson.txt"));
                    String line= reader.readLine();
                    while (line !=null){
                        resultConvert = objectMapper.readValue(line, ResultJsonContainer.class);
                        resultStore.add(resultConvert);
                        line = reader.readLine();
                    }
                    reader.close();
                } catch (FileNotFoundException e) {
                    System.out.println(e.getMessage());
                } catch (IOException e) {
                    System.out.println(e.getMessage());
                }
                for(int i=0;i<resultStore.size();i++){
                    resultsDtos.get(i).setResult(resultStore.get(i));
                }
                isSelected = true;
            }
        }catch (Exception e){
            System.out.println(e.getMessage());
        }

        if(isSelected){
            response.setMessage(message.selected(Resources.RESULTS.getName()));
            response.setData(resultsDtos);
            response.setStatus(HttpStatus.OK);
            response.setTimestamp(timeUtils.getCurrentTime());
            return ResponseEntity.ok(response);
        }else{
            response.setMessage(message.selectedError(Resources.RESULTS.getName()));
            response.setStatus(HttpStatus.OK);
            response.setTimestamp(timeUtils.getCurrentTime());
            return ResponseEntity.ok(response);
        }


    }
    @GetMapping("/validate/{quiz_id}")
    public ResponseEntity<BaseAPIResponse<String>> validate(@PathVariable int quiz_id,@RequestParam String card_id){
        List<Integer> quizs=resultRepository.selectQuizId();
        List<String> card=resultRepository.selectCardId(quiz_id);
        BaseAPIResponse<String> res=new BaseAPIResponse<>();
        for(int i=0;i<quizs.size();i++){
            if(quiz_id==quizs.get(i)){
                System.out.println("Quiz id:"+quiz_id);
               for(int j=0;j<card.size();j++){
                   System.out.println("card size:"+card.size());
                   System.out.println("card id["+j+"]"+card_id);
                   System.out.println("card ["+j+"]"+card.get(j));
                   if(card_id.equalsIgnoreCase(card.get(j))){
                      res.setMessage("Cannot take the exam twice");
                   }
                   else {
                       res.setMessage("Success...");
                   }

               }
            }
        }
        return ResponseEntity.ok(res);
    }
    /**********************************************************************
     * This method it used to get  results by class
     **********************************************************************/

    @GetMapping("/{quiz_id}")
    public ResponseEntity<BaseAPIResponse<List<ResultDto>>> selectByClass(@PathVariable int quiz_id,@RequestParam String class_name){

        BaseAPIResponse<List<ResultDto>> response = new BaseAPIResponse<>();
        List<ResultDto> resultsDtos = new ArrayList<>();
        List<ResultJsonContainer> resultStore = new ArrayList<>();


        JsonUploadFile file = new JsonUploadFile();
        ResultJsonContainer resultConvert = new ResultJsonContainer();
        ObjectMapper objectMapper = new ObjectMapper();

        boolean isSelected = false;

        try {
            List<String> results = resultRepository.selectResultByClass(quiz_id,class_name);
            System.out.println("result class Name:"+results);
            resultsDtos = service.getResultByClass(quiz_id,class_name);
            if (resultsDtos.size()>0){
                isSelected=true;
            }
           else isSelected=false;
            if(results.size()==0){
                isSelected = false;
            }else{
                file.deleted();
                for(Object res : results){
                    file.writeFile(res);
                }
                BufferedReader reader;
                try {
                    reader = new BufferedReader(new FileReader("filejson.txt"));
                    String line= reader.readLine();
                    while (line !=null){
                        resultConvert = objectMapper.readValue(line, ResultJsonContainer.class);
                        resultStore.add(resultConvert);
                        line = reader.readLine();
                    }
                    reader.close();
                } catch (FileNotFoundException e) {
                    System.out.println(e.getMessage());
                } catch (IOException e) {
                    System.out.println(e.getMessage());
                }
                for(int i=0;i<resultStore.size();i++){
                    resultsDtos.get(i).setResult(resultStore.get(i));
                }
                isSelected = true;
            }
        }catch (Exception e){
            System.out.println(e.getMessage());
        }

        if(isSelected){
            for(int x=0;x<resultsDtos.size();x++){
                resultsDtos.get(x).setStatus(resultsDtos.get(x).getStatus());
                System.out.println("status:"+resultsDtos.get(x).getStatus());
            }
            response.setMessage(message.selected(Resources.RESULTS.getName()));

            response.setData(resultsDtos);
            response.setStatus(HttpStatus.OK);
            response.setTimestamp(timeUtils.getCurrentTime());
            return ResponseEntity.ok(response);
        }else{
            response.setData(null);
            response.setMessage(message.selectedError(Resources.RESULTS.getName()));
            response.setStatus(HttpStatus.NOT_FOUND);
            response.setTimestamp(timeUtils.getCurrentTime());
            return ResponseEntity.ok(response);
        }


    }

    @GetMapping("/checking/{student_id}")
    public ResponseEntity<BaseAPIResponse<CheckingStudentResponse>> checkingResponse(int student_id,@RequestParam int quiz_id){
        BaseAPIResponse<CheckingStudentResponse> responseBaseAPIResponse=new BaseAPIResponse<>();
        //List<InstructionFilterResponse> answers=quizRepository.selectInstruction(quiz_id);
        System.out.println("rest in first");
        CheckingStudentResponse response=service.checkingResponse(student_id,quiz_id);
        //System.out.println("res:"+response);
        response.getResultList().get(0).setQuiz_id(quiz_id);
        responseBaseAPIResponse.setData(response);
        System.out.println("rest");
        return ResponseEntity.ok(responseBaseAPIResponse);
    }



    @GetMapping("/detail/{student_id}/{quiz_id}")
    public ResponseEntity<BaseAPIResponse<CheckingStudentResponse>> selectDetailResult(@PathVariable int student_id,int quiz_id){

        BaseAPIResponse<CheckingStudentResponse> response = new BaseAPIResponse<>();
        CheckingStudentResponse resultsDtos=null;
        List<ResultListResponse> responses=resultRepository.selectResult(student_id,quiz_id);
        List<ResultJsonContainer> resultStore=new ArrayList<>();
        JsonUploadFile file = new JsonUploadFile();
        ResultJsonContainer resultConvert = new ResultJsonContainer();
        ObjectMapper objectMapper = new ObjectMapper();

        boolean isSelected = false;

        try {
            List<String> results = resultRepository.resultByStudentId(student_id,quiz_id);
           // System.out.println("result:"+results);
            resultsDtos = service.selectDetailResult(student_id,quiz_id);
            if(results.size()==0){
                isSelected = false;
            }else{
                file.deleted();
                for(Object res : results){
                    System.out.println("res:"+res);
                   file.writeFile(res);

                }
                BufferedReader reader;
                try {
                    reader = new BufferedReader(new FileReader("filejson.txt"));
                    String line= reader.readLine();
                    while (line !=null){
                        resultConvert = objectMapper.readValue(line, ResultJsonContainer.class);
                        resultStore.add(resultConvert);
                        line = reader.readLine();
                    }
                    reader.close();
                } catch (FileNotFoundException e) {
                    System.out.println(e.getMessage());
                } catch (IOException e) {
                    System.out.println(e.getMessage());
                }
               // resultsDtos.setResultList(resultStore);
                for(int i=0;i<resultStore.size();i++){
                    responses.get(i).setResult(resultStore.get(i));
                    resultsDtos.setResultList(responses);
                    isSelected = true;
                }

            }
        }catch (Exception e){
            System.out.println(e.getMessage());
        }

        if(isSelected){
            response.setMessage(message.selected(Resources.RESULTS.getName()));
            resultsDtos.getResultList().get(0).setQuiz_id(quiz_id);
            response.setData(resultsDtos);
            response.setStatus(HttpStatus.OK);
            response.setTimestamp(timeUtils.getCurrentTime());
            return ResponseEntity.ok(response);
        }else{
            response.setData(null);
            response.setMessage(message.selectedError(Resources.RESULTS.getName()));
            response.setStatus(HttpStatus.NOT_FOUND);
            response.setTimestamp(timeUtils.getCurrentTime());
            return ResponseEntity.ok(response);
        }


    }

    @GetMapping("/sent/detail/{student_id}/{quiz_id}")
    public ResponseEntity<BaseAPIResponse<CheckingStudentResponse>> sentDetailResult(@PathVariable int student_id,int quiz_id){

        BaseAPIResponse<CheckingStudentResponse> response = new BaseAPIResponse<>();
        CheckingStudentResponse resultsDtos=null;
        List<ResultListResponse> responses=resultRepository.sentResult(student_id,quiz_id);
        List<ResultJsonContainer> resultStore=new ArrayList<>();
        JsonUploadFile file = new JsonUploadFile();
        ResultJsonContainer resultConvert = new ResultJsonContainer();
        ObjectMapper objectMapper = new ObjectMapper();

        boolean isSelected = false;

        try {
            List<String> results = resultRepository.sentByStudentId(student_id,quiz_id);
            // System.out.println("result:"+results);
            resultsDtos = service.selectDetailResult(student_id,quiz_id);
            if(results.size()==0){
                isSelected = false;
            }else{
                file.deleted();
                for(Object res : results){
                    System.out.println("res:"+res);
                    file.writeFile(res);

                }
                BufferedReader reader;
                try {
                    reader = new BufferedReader(new FileReader("filejson.txt"));
                    String line= reader.readLine();
                    while (line !=null){
                        resultConvert = objectMapper.readValue(line, ResultJsonContainer.class);
                        resultStore.add(resultConvert);
                        line = reader.readLine();
                    }
                    reader.close();
                } catch (FileNotFoundException e) {
                    System.out.println(e.getMessage());
                } catch (IOException e) {
                    System.out.println(e.getMessage());
                }
                // resultsDtos.setResultList(resultStore);
                for(int i=0;i<resultStore.size();i++){
                    responses.get(i).setResult(resultStore.get(i));
                    resultsDtos.setResultList(responses);
                    isSelected = true;
                }

            }
        }catch (Exception e){
            System.out.println(e.getMessage());
        }

        if(isSelected){
            response.setMessage(message.selected(Resources.RESULTS.getName()));
            resultsDtos.getResultList().get(0).setQuiz_id(quiz_id);
            response.setData(resultsDtos);
            response.setStatus(HttpStatus.OK);
            response.setTimestamp(timeUtils.getCurrentTime());
            return ResponseEntity.ok(response);
        }else{
            response.setData(null);
            List<Integer> stId=resultRepository.selectStudentId(quiz_id);
            boolean s=false;
            for(int i=0;i<stId.size();i++){
                if(student_id==stId.get(i)){
                    s=true;
                    response.setMessage("This Result hasn't completed the checking yet");
                    response.setStatus(HttpStatus.NOT_FOUND);
                    response.setTimestamp(timeUtils.getCurrentTime());

                }
               // return ResponseEntity.ok(response);
            }
          if(s==false){
              response.setMessage("No result to show. You didn't take the exam.");
              response.setStatus(HttpStatus.NOT_FOUND);
              response.setTimestamp(timeUtils.getCurrentTime());

          }
            return ResponseEntity.ok(response);
        }


    }

    /**********************************************************************
     * This method it used to get one result of students from database.
     **********************************************************************/


    @GetMapping("/{student_id}/{quiz_id}")
    public ResponseEntity<BaseAPIResponse<ResultDto>> select(@PathVariable int student_id,int quiz_id){

        BaseAPIResponse response =  new BaseAPIResponse();
        ResultDto resultsDto = new ResultDto();
        JsonUploadFile file = new JsonUploadFile();
        ResultJsonContainer resultConvert = new ResultJsonContainer();
        ObjectMapper objectMapper = new ObjectMapper();

        boolean isSelected = true;

        try {

           // List<String> requestResult = service.result();
            System.out.println("getResult service");
            resultsDto = service.getResultByStudentID(student_id,quiz_id);
            int resultId=resultsDto.getResultId();
            System.out.println("result in service:"+resultsDto);
           String requestResult = service.GetResultById(resultId);
            if(resultsDto==null){
                isSelected = false;
            }else{
                file.deleted();
                file.writeFile(requestResult);

                BufferedReader reader;
                try {
                    reader = new BufferedReader(new FileReader("filejson.txt"));
                    String line = reader.readLine();
                    while (line != null) {
                        resultConvert = objectMapper.readValue(line, ResultJsonContainer.class);
                        line = reader.readLine();
                    }
                    reader.close();
                } catch (FileNotFoundException e) {
                    response.setStatus(HttpStatus.NOT_FOUND);
                    response.setMessage("FileNotFoundException");
                } catch (IOException e) {
                    response.setStatus(HttpStatus.NOT_FOUND);
                    response.setMessage("IOException");
                }
                resultsDto.setResult(resultConvert);

                isSelected = true;
            }

        }catch (Exception e){
            response.setStatus(HttpStatus.BAD_REQUEST);
            response.setMessage(e.getMessage());
        }
        if(isSelected){
            response.setData(resultsDto);
            response.setStatus(HttpStatus.OK);
            response.setMessage(message.selectedOne(Resources.RESULTS.getName()));
            response.setTimestamp(timeUtils.getCurrentTime());
            return ResponseEntity.ok(response);
        }else{
            response.setStatus(HttpStatus.NOT_FOUND);
            response.setMessage(message.selectedError(Resources.RESULTS.getName()));
            response.setTimestamp(timeUtils.getCurrentTime());
            return ResponseEntity.ok(response);
        }

    }


    /**********************************************************************
     * This method it used to get result of students from database by class.
     **********************************************************************/
    /*@GetMapping("/class")
    public ResponseEntity<BaseAPIResponse<List<ResultDto>>> selectByClass(@RequestParam(required = false) String classname){

        BaseAPIResponse<List<ResultDto>> responses = new BaseAPIResponse<>();
        List<ResultDto> resultsDtos = new ArrayList<>();
        List<InstructionDetailRequest> resultStore = new ArrayList<>();


        JsonUploadFile file = new JsonUploadFile();
        InstructionDetailRequest resultConvert = new InstructionDetailRequest();
        ObjectMapper objectMapper = new ObjectMapper();

        boolean isSelected = true;

        try {
            List<String> results = service.GetResultByClassname(classname);
            resultsDtos = service.SelectByClassname(classname);
            if(results.size()==0){
                isSelected = false;
            }else{
                file.deleted();
                for(Object res : results){
                    file.writeFile(res);
                }
                BufferedReader reader;
                try {
                    reader = new BufferedReader(new FileReader("filejson.txt"));
                    String line= reader.readLine();
                    while (line !=null){
                        resultConvert = objectMapper.readValue(line, InstructionDetailRequest.class);
                        resultStore.add(resultConvert);
                        line = reader.readLine();
                    }
                    reader.close();
                } catch (FileNotFoundException e) {
                    System.out.println(e.getMessage());
                } catch (IOException e) {
                    System.out.println(e.getMessage());
                }
                for(int i=0;i<resultStore.size();i++){
                    resultsDtos.get(i).setResult(resultStore.get(i));
                }
                isSelected = true;
            }
        }catch (Exception e){
            System.out.println(e.getMessage());
        }
        if(isSelected){
            responses.setMessage(message.selected(Resources.RESULTS.getName()));
            responses.setData(resultsDtos);
            responses.setStatus(HttpStatus.OK);
            responses.setTimestamp(timeUtils.getCurrentTime());
            return ResponseEntity.ok(responses);
        }else{
            responses.setMessage(message.selectedError(Resources.RESULTS.getName()));
            responses.setStatus(HttpStatus.NOT_FOUND);
            responses.setTimestamp(timeUtils.getCurrentTime());
            return ResponseEntity.ok(responses);
        }

    }
*/

    /**********************************************************************
     * This method it used to update  result of students by id.
     **********************************************************************/
    @PutMapping("/{student_id}")
    public ResponseEntity<BaseAPIResponse<ResultCheckingRequestDto>> update(@PathVariable int student_id,@RequestParam int quiz_id, @RequestBody ResultCheckingRequestDto requestDto){
        BaseAPIResponse<ResultCheckingRequestDto> response = new BaseAPIResponse<>();
        boolean isUpdated = true;
        try {

            if(requestDto.getStudent_id()== 0 || "".equals(requestDto.getStudent_id())){
                response.setMessage(message.nullValue("StudentID"));
                response.setStatus(HttpStatus.BAD_REQUEST);
            }else {
                System.out.println("in rest:"+student_id+quiz_id+requestDto);
                boolean update = service.UpdateResult(student_id,quiz_id, requestDto);
                if (!update)
                    isUpdated = false;
            }
        }catch (Exception e){
            isUpdated = false;
            System.out.println(e.getMessage());
        }
        if(isUpdated) {
            response.setData(requestDto);
            response.setMessage(message.updated(Resources.RESULTS.getName()));
            response.setStatus(HttpStatus.OK);
            response.setTimestamp(timeUtils.getCurrentTime());
        }else {
            response.setMessage(message.updatedError(Resources.RESULTS.getName()));
            response.setStatus(HttpStatus.NOT_FOUND);
            response.setTimestamp(timeUtils.getCurrentTime());
        }
        return ResponseEntity.ok(response);
    }


    /**********************************************************************
     * This method it used to delete result of students from database.
     **********************************************************************/
    @DeleteMapping("/{id}")
    public ResponseEntity<BaseAPIResponse> delete(@PathVariable int id){


        BaseAPIResponse response = new BaseAPIResponse();

        try{

            ResultDto resultDto = service.selectByResultId(id);

                int deleteResult = service.DeleteResult(id);
                if(deleteResult!=0){
                    response.setMessage(message.deleted(Resources.RESULTS.getName()));
                    response.setStatus(HttpStatus.OK);
                }else{
                    response.setMessage(message.deletedError(Resources.RESULTS.getName()));
                    response.setStatus(HttpStatus.BAD_REQUEST);
                }

            response.setTimestamp(timeUtils.getCurrentTime());
            return ResponseEntity.ok(response);
        }catch (Exception e){
            response.setMessage(message.deletedError(Resources.RESULTS.getName()));
            response.setStatus(HttpStatus.BAD_REQUEST);
            response.setTimestamp(timeUtils.getCurrentTime());
            return ResponseEntity.ok(response);
        }
    }

    @PutMapping("/score/{id}")
    public ResponseEntity<BaseAPIResponse<ScoreDto>> updateScoreByResultId(@PathVariable int id,@RequestBody ScoreRequest scoreRequest){
        BaseAPIResponse response = new BaseAPIResponse();
        ScoreDto scoreDto = new ScoreDto();
        try{
//            if(scoreRequest.getScore() ===""){
//                response.setMessage(message.nullValue(Resources.RESULTS.getName()));
//                response.setStatus(HttpStatus.BAD_REQUEST);
//            }else{
                if(service.updateScore(id,scoreRequest)){
                    scoreDto.setId(id);
                    scoreDto.setScore(scoreRequest.getScore());
                    scoreDto.setStatus(scoreRequest.isStatus());
                    response.setMessage(message.updated(Resources.RESULTS.getName()));
                    response.setStatus(HttpStatus.OK);
                    response.setData(scoreDto);
                }
//            }
            response.setTimestamp(timeUtils.getCurrentTime());
            return ResponseEntity.ok(response);
        }catch (Exception e){
            response.setMessage(e.getMessage());
            response.setTimestamp(timeUtils.getCurrentTime());
            response.setStatus(HttpStatus.NOT_FOUND);
            return ResponseEntity.ok(response);
        }

    }
}
