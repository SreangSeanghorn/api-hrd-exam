package com.kshrd.exam_api.controller.rest;


import com.kshrd.exam_api.controller.request.*;


import com.kshrd.exam_api.controller.response.*;

import com.kshrd.exam_api.controller.response.messages.BaseMessage;
import com.kshrd.exam_api.controller.response.messages.MessageProperties;
import com.kshrd.exam_api.controller.response.messages.Resources;
import com.kshrd.exam_api.controller.utils.DateTimeUtils;
import com.kshrd.exam_api.controller.utils.ModelMapperUtil;

import com.kshrd.exam_api.controller.utils.Paging;
import com.kshrd.exam_api.repository.model.QuizDto;
import com.kshrd.exam_api.repository.rep.GenerationRepository;
import com.kshrd.exam_api.repository.rep.QuizRepository;
import com.kshrd.exam_api.service.serviceImplement.QuizServiceImp;
import com.kshrd.exam_api.service.serviceInterface.AnswerService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**********************************************************************
 * Original Author:
 * Created Date: 21-07-2020
 * Development Group: HRD_EXAM
 * Description: this class is created to store quizzes
 **********************************************************************/

@CrossOrigin(origins = "*")
@RestController
@RequestMapping("${baseUrl}/quizzes")
public class QuizRestController {
    ModelMapper mapper = new ModelMapper();
    QuizServiceImp quizServiceImp;
    @Autowired
    AnswerService answerService;

    @Autowired
    public QuizRestController(QuizServiceImp quizServiceImp) {
        this.quizServiceImp = quizServiceImp;
    }

    @Autowired
    GenerationRepository generationRepository;
    @Autowired
    QuizRepository quizRepository;
    private MessageProperties messages;

    @Autowired
    public void setMessages(MessageProperties messages) {
        this.messages = messages;
    }

    /**********************************************************************
     * Original Author: Lai Manith
     * Created Date: 21-07-2020
     * Description: crud generation
     * Modification History :
     *     Date:
     *  Developer:
     *  TODO: - post quiz into database.
     *        - get all and get by ID.
     *        - update quiz into database.
     *        - delete quiz from database.
     **********************************************************************/

    /**********************************************************************
     *This method is used to get quiz from database
     **********************************************************************/
    @GetMapping("/details")
    public ResponseEntity<BaseAPIResponse<List<QuizResponseFilter>>> select() {

        BaseAPIResponse<List<QuizResponseFilter>> response = new BaseAPIResponse<>();
        try {
            List<QuizResponseFilter> quizDto = quizServiceImp.select();
            List<QuizResponseFilter> quizRequestDto = new ArrayList<>();
            for (QuizResponseFilter read : quizDto) {
                quizRequestDto.add(ModelMapperUtil.mapper().map(read, QuizResponseFilter.class));
            }
            response.setMessage(messages.selected(Resources.QUIZZES.getName()));
            response.setStatus(HttpStatus.OK);
            response.setData(quizRequestDto);
            response.setTimestamp(DateTimeUtils.getCurrentTime());
            return ResponseEntity.ok(response);
        } catch (Exception ex) {
            response.setMessage(messages.selectedError(Resources.QUIZZES.getName()));
            response.setStatus(HttpStatus.NOT_FOUND);
            response.setData(null);
            response.setTimestamp(DateTimeUtils.getCurrentTime());
            return new ResponseEntity<>(response, HttpStatus.NOT_FOUND);
        }

    }

    /**********************************************************************
     *This method is used to get quiz by status
     **********************************************************************/
    @GetMapping("/status")
    public ResponseEntity<BaseAPIResponse<List<QuizResponseFilter>>> selectByStatus() {

        BaseAPIResponse<List<QuizResponseFilter>> response = new BaseAPIResponse<>();
        try {
            List<QuizResponseFilter> quizDto = quizServiceImp.selectByStatus();
            List<QuizResponseFilter> quizRequestDto = new ArrayList<>();
            for (QuizResponseFilter read : quizDto) {
                quizRequestDto.add(ModelMapperUtil.mapper().map(read, QuizResponseFilter.class));
            }

            response.setMessage(messages.selected(Resources.QUIZZES.getName()));
            response.setStatus(HttpStatus.OK);
            response.setData(quizRequestDto);
            response.setTimestamp(DateTimeUtils.getCurrentTime());
            return ResponseEntity.ok(response);
        } catch (Exception ex) {
            response.setMessage(messages.selectedError(Resources.QUIZZES.getName()));
            response.setStatus(HttpStatus.NOT_FOUND);
            response.setData(null);
            response.setTimestamp(DateTimeUtils.getCurrentTime());
            return new ResponseEntity<>(response, HttpStatus.NOT_FOUND);
        }

    }

    // change some code

    /**********************************************************************
     *This method is used to get quiz by status
     **********************************************************************/
    @GetMapping()
    public ResponseEntity<BaseAPIResponse<List<QuizRequestModel>>> selectAllQuiz(@RequestParam(value = "generationId", required = false, defaultValue = "0") int generationId) {

        BaseAPIResponse<List<QuizRequestModel>> response = new BaseAPIResponse<>();
        try {
            List<QuizDto> quizDto = quizServiceImp.selectQuiz(generationId);
            if (quizDto.size() == 0) {
                response.setMessage(messages.selectedError(Resources.QUIZZES.getName()));
                response.setStatus(HttpStatus.NOT_FOUND);
            } else {
                List<QuizRequestModel> quizRequestDto = new ArrayList<>();
                for (QuizDto read : quizDto) {
                    quizRequestDto.add(ModelMapperUtil.mapper().map(read, QuizRequestModel.class));
                }
                response.setMessage(messages.selected(Resources.QUIZZES.getName()));
                response.setStatus(HttpStatus.OK);
                response.setData(quizRequestDto);
            }
            response.setTimestamp(DateTimeUtils.getCurrentTime());
            return ResponseEntity.ok(response);
        } catch (Exception ex) {
            response.setMessage(messages.selectedError(Resources.QUIZZES.getName()));
            response.setStatus(HttpStatus.NOT_FOUND);
            response.setData(null);
            response.setTimestamp(DateTimeUtils.getCurrentTime());
            return new ResponseEntity<>(response, HttpStatus.NOT_FOUND);
        }

    }
    @GetMapping("/sentQuiz")
    public ResponseEntity<BaseAPIResponse<List<QuizRequestModel>>> sentQuiz() {

        BaseAPIResponse<List<QuizRequestModel>> response = new BaseAPIResponse<>();
        try {
            List<QuizDto> quizDto = quizRepository.selectSentQuiz();
            if (quizDto.size() == 0) {
                response.setMessage(messages.selectedError(Resources.QUIZZES.getName()));
                response.setStatus(HttpStatus.NOT_FOUND);
            } else {
                List<QuizRequestModel> quizRequestDto = new ArrayList<>();
                for (QuizDto read : quizDto) {
                    quizRequestDto.add(ModelMapperUtil.mapper().map(read, QuizRequestModel.class));
                }
                response.setMessage(messages.selected(Resources.QUIZZES.getName()));
                response.setStatus(HttpStatus.OK);
                response.setData(quizRequestDto);
            }
            response.setTimestamp(DateTimeUtils.getCurrentTime());
            return ResponseEntity.ok(response);
        } catch (Exception ex) {
            response.setMessage(messages.selectedError(Resources.QUIZZES.getName()));
            response.setStatus(HttpStatus.NOT_FOUND);
            response.setData(null);
            response.setTimestamp(DateTimeUtils.getCurrentTime());
            return new ResponseEntity<>(response, HttpStatus.NOT_FOUND);
        }

    }
    /**********************************************************************
     *This method is used to get quiz by ID from database
     **********************************************************************/
    @GetMapping("/{id}")
    public ResponseEntity<BaseAPIResponse<QuizRequestDto>> selectByID(@PathVariable int id) {

        BaseAPIResponse<QuizRequestDto> response = new BaseAPIResponse<>();
        try {
            QuizDto quizDto = quizServiceImp.selectByID(id);
            QuizRequestDto quizRequestDto = ModelMapperUtil.mapper().map(quizDto, QuizRequestDto.class);
            response.setMessage(messages.selectedOne(Resources.QUIZ.getName()));
            response.setStatus(HttpStatus.OK);
            response.setData(quizRequestDto);
            response.setTimestamp(DateTimeUtils.getCurrentTime());
        } catch (Exception ex) {
            response.setMessage(messages.selectedError(Resources.QUIZ.getName()));
            response.setStatus(HttpStatus.NOT_FOUND);
            response.setData(null);
            response.setTimestamp(DateTimeUtils.getCurrentTime());
        }
        return ResponseEntity.ok(response);

    }

    /**********************************************************************
     *This method is used to get detail quiz by ID
     **********************************************************************/
    @GetMapping("/details/{id}")
    public ResponseEntity<BaseAPIResponse<QuizResponseFilter>> selectQuizByID(@PathVariable int id) {

        BaseAPIResponse<QuizResponseFilter> response = new BaseAPIResponse<>();
        try {
            List<InstructionFilterResponse> selectInstruction=quizRepository.selectInstruction(id);
            if(selectInstruction.isEmpty()) {
                response.setMessage("Cannot View the Empty Quiz");
                response.setStatus(HttpStatus.NO_CONTENT);
            }
            else{
                QuizResponseFilter quizDto = quizServiceImp.selectQuizByID(id);
                QuizResponseFilter quizRequestDto = ModelMapperUtil.mapper().map(quizDto, QuizResponseFilter.class);
                response.setMessage(messages.selectedOne(Resources.QUIZ.getName()));
                response.setStatus(HttpStatus.OK);
                response.setData(quizRequestDto);
                response.setTimestamp(DateTimeUtils.getCurrentTime());
            }

        } catch (Exception ex) {
            response.setMessage(messages.selectedError(Resources.QUIZ.getName()));
            response.setStatus(HttpStatus.NOT_FOUND);
            response.setData(null);
            response.setTimestamp(DateTimeUtils.getCurrentTime());
        }
        return ResponseEntity.ok(response);

    }

    /**********************************************************************
     *This method is used to post quiz into database
     **********************************************************************/
    @PostMapping()
    public ResponseEntity<BaseAPIResponse<QuizResponse>> insert(@RequestBody QuizResquestNoStatus quiz) {

        BaseAPIResponse<QuizResponse> response = new BaseAPIResponse<>();
        int genId = generationRepository.getGenerationId();
        List<String> title = quizRepository.selectTitle();
        List<String> subject = quizRepository.selectSubject();
        boolean status = true;
        QuizResponse requestDto = mapper.map(quiz, QuizResponse.class);
        for (int i = 0; i < title.size(); i++) {
            if (quiz.getTitle().trim().equalsIgnoreCase(title.get(i)) &&
                    quiz.getSubject().trim().equalsIgnoreCase(subject.get(i))) {
                status = false;
            }
        }

        if (quiz.getDuration() == 0 || quiz.getSubject() == null || quiz.getSubject().equals("")|| quiz.getTitle() == null
                || quiz.getTitle().equals("")||quiz.getTitle().trim().isEmpty()||quiz.getSubject().trim().isEmpty()){
           if (quiz.getDuration() == 0){
               response.setMessage(messages.nullValue(Resources.Q_DURATION.getName()));
               response.setTimestamp(DateTimeUtils.getCurrentTime());
               response.setStatus(HttpStatus.NOT_FOUND);
           }
            if (quiz.getSubject() == null || quiz.getSubject().equals("")||quiz.getSubject().trim().isEmpty()){
                response.setMessage(messages.nullValue(Resources.Q_SUBJECT.getName()));
                response.setTimestamp(DateTimeUtils.getCurrentTime());
                response.setStatus(HttpStatus.NOT_FOUND);
            }

            if (quiz.getTitle().trim() == null || quiz.getTitle().equals("")||quiz.getTitle().trim().isEmpty()){
                response.setMessage(messages.nullValue(Resources.Q_TITLE.getName()));
                response.setTimestamp(DateTimeUtils.getCurrentTime());
                response.setStatus(HttpStatus.NOT_FOUND);
            }
        } else {
            if (status == true) {
                ModelMapper mapper = new ModelMapper();
                mapper.getConfiguration().setAmbiguityIgnored(true);
                requestDto.setGenerationId(genId);
                if (AuthenticationRestController.teacherId==0){
                    response.setMessage("Your token has been expired");
                }
                else {
                    requestDto.setTeacherId(AuthenticationRestController.teacherId);
                    QuizDto quizDto = mapper.map(requestDto, QuizDto.class);
                    quizDto.setDate(DateTimeUtils.getCurrentTime());
                    quizDto.setStatus(false);
                    quizDto.setTitle(quizDto.getTitle().trim());
                    quizDto.setSubject(quizDto.getSubject().trim());
                    QuizDto result = quizServiceImp.insert(quizDto);

                    int id = quizRepository.selectQuiz_id();
                    QuizResponse mapResult = mapper.map(result, QuizResponse.class);
                    mapResult.setQuizId(id);
                    response.setMessage(messages.inserted(Resources.QUIZZES.getName()));
                    response.setData(mapResult);
                    response.setTimestamp(DateTimeUtils.getCurrentTime());
                    response.setStatus(HttpStatus.OK);
                }

            } else {
                response.setMessage("Cannot create duplicate quiz");
                response.setTimestamp(DateTimeUtils.getCurrentTime());
                response.setStatus(HttpStatus.BAD_REQUEST);
            }
        }
        return ResponseEntity.ok(response);

    }

    /**********************************************************************
     *This method is used to be update quiz into database
     **********************************************************************/
    @PutMapping("/{id}")
    public ResponseEntity<BaseAPIResponse<QuizRequestModel>> update(@RequestBody QuizRequestNonDateStatusDto quizRequestDto, @PathVariable int id) {

        BaseAPIResponse<QuizRequestModel> response = new BaseAPIResponse<>();
        QuizDto quizDto = quizServiceImp.selectByID(id);
        List<String> title = quizRepository.selectExceptTitle(id);
        List<String> subject = quizRepository.selectExceptSubject(id);
        boolean status = true;
        for (int i = 0; i < title.size(); i++) {
            System.out.println("subject:"+subject.get(i)+" title:"+title.get(i));
            if (quizRequestDto.getTitle().trim().equalsIgnoreCase(title.get(i)) &&
                    quizRequestDto.getSubject().trim().equalsIgnoreCase(subject.get(i))) {
                status = false;
            }
        }
        if (quizRequestDto.getDuration() == 0 || quizRequestDto.getSubject() == null || quizRequestDto.getSubject().equals("") || quizRequestDto.getTitle() == null
                || quizRequestDto.getTitle().equals("")||quizRequestDto.getTitle().trim().isEmpty()||quizRequestDto.getSubject().trim().isEmpty()){
            if (quizRequestDto.getDuration() == 0){

                response.setMessage(messages.nullValue(Resources.Q_DURATION.getName()));
                response.setTimestamp(DateTimeUtils.getCurrentTime());
                response.setStatus(HttpStatus.NOT_FOUND);
            }

            if (quizRequestDto.getSubject() == null || quizRequestDto.getSubject().equals("")||quizRequestDto.getSubject().trim().isEmpty()){
                response.setMessage(messages.nullValue(Resources.Q_SUBJECT.getName()));
                response.setTimestamp(DateTimeUtils.getCurrentTime());
                response.setStatus(HttpStatus.NOT_FOUND);
            }

            if (quizRequestDto.getTitle() == null || quizRequestDto.getTitle().equals("")||quizRequestDto.getTitle().trim().isEmpty()){
                response.setMessage(messages.nullValue(Resources.Q_TITLE.getName()));
                response.setTimestamp(DateTimeUtils.getCurrentTime());
                response.setStatus(HttpStatus.NOT_FOUND);
            }
        } else {
            if(status == true){
                quizServiceImp.update(id, quizRequestDto);
                QuizRequestModel quizRequestDto1 = ModelMapperUtil.mapper().map(quizDto, QuizRequestModel.class);
                QuizRequestModel quizRequestDto2 = ModelMapperUtil.mapper().map(quizRequestDto, QuizRequestModel.class);
                quizRequestDto2.setId(quizRequestDto1.getId());
                quizRequestDto2.setDate(quizRequestDto1.getDate());
                quizRequestDto2.setGenerationId(quizRequestDto1.getGenerationId());
                quizRequestDto2.setTeacherId(quizRequestDto1.getTeacherId());
                response.setMessage(messages.updated(Resources.QUIZ.getName()));
                response.setStatus(HttpStatus.OK);
                response.setData(quizRequestDto2);
                response.setTimestamp(DateTimeUtils.getCurrentTime());
            }
            else {
                response.setMessage("Quiz is failed to update");
            }

        }
        return ResponseEntity.ok(response);

    }


    @PutMapping("/sent/{id}")
    public ResponseEntity<BaseAPIResponse<QuizRequestModel>> updateIssent(@RequestBody QuizSentDto quizRequestDto, @PathVariable int id) {

        BaseAPIResponse<QuizRequestModel> response = new BaseAPIResponse<>();
        boolean status =quizServiceImp.updateQuizSent(id,quizRequestDto.getSent());
        if(quizRequestDto.getSent()==false){
            response.setMessage("The result of this quiz is not available to see by student anymore...");
        }
        else {
            if(status==true){
                response.setMessage("The result of this quiz is available to send now");
            }

            else {
                response.setMessage("There's result hasn't checked yet in this quiz!");
            }
        }



        return ResponseEntity.ok(response);

    }

    /**********************************************************************
     *This method is used to delete quiz from database
     **********************************************************************/

    @DeleteMapping("/{id}")
    public ResponseEntity<BaseAPIResponse<QuizRequestDto>> delete(@PathVariable int id) {
        BaseAPIResponse<QuizRequestDto> response = new BaseAPIResponse<>();
        QuizDto quizDto = quizServiceImp.selectByID(id);
        if (quizDto != null && quizDto.getStatus()) {
            response.setTimestamp(DateTimeUtils.getCurrentTime());
            response.setMessage("Unable to delete the active quiz...");
            //response.setMessage(messages.ViewStatus(Resources.QUIZ.getName(),"status"));
            response.setStatus(HttpStatus.NOT_FOUND);
            return ResponseEntity.ok(response);
        } else {
            if (quizServiceImp.isDeleted(id)) {
                QuizRequestDto quizRequestDto = ModelMapperUtil.mapper().map(quizDto, QuizRequestDto.class);
                response.setMessage(messages.deleted(Resources.QUIZ.getName()));
                response.setStatus(HttpStatus.OK);
                response.setTimestamp(DateTimeUtils.getCurrentTime());
                response.setData(quizRequestDto);
                return new ResponseEntity<>(response, HttpStatus.OK);
            } else {
                response.setTimestamp(DateTimeUtils.getCurrentTime());
                response.setMessage(messages.deletedError(Resources.QUIZ.getName()));
                response.setStatus(HttpStatus.NOT_FOUND);
                return new ResponseEntity<>(response, HttpStatus.NOT_FOUND);
            }
        }
    }

    /**********************************************************************
     *This method is used to update status of quiz by id
     **********************************************************************/
    @PutMapping()
    public ResponseEntity<BaseAPIResponse<QuizRequestDto>> updateStatus(@RequestParam int id, @RequestBody QuizUpdateStatus status) {
        BaseAPIResponse<QuizRequestDto> response = new BaseAPIResponse<>();
        if (status.getStatus().toString().equalsIgnoreCase("true")) {
            List<InstructionFilterResponse> selectInstruction=quizRepository.selectInstruction(id);
            if(selectInstruction.isEmpty()){
                response.setMessage("Cannot Active the Empty Quiz");
            }
            else{
                if (quizServiceImp.updateQuizStatus(id, status.getStatus())) {
                    quizServiceImp.updateQuizStatusFalse(id);
                    response.setMessage("The Quiz has been active successfully");
                    response.setStatus(HttpStatus.OK);
                } else {
                    response.setMessage("The quiz cannot be active");
                    response.setStatus(HttpStatus.CONFLICT);
                }
            }


        } else if (status.getStatus().toString().equalsIgnoreCase("false")) {
            quizServiceImp.updateQuizStatus(id, status.getStatus());
            response.setMessage("The Quiz has been deactive successfully");
            response.setStatus(HttpStatus.OK);
        }

        return ResponseEntity.ok(response);
    }

    /**********************************************************************
     *This method is used to generate page
     **********************************************************************/
    @GetMapping("/details/page")
    public ResponseEntity<APIResponse<List<QuizResponseFilter>>> selectByPagination(Paging page) {

        APIResponse<List<QuizResponseFilter>> response = new APIResponse<>();
        try {
            List<QuizResponseFilter> quizDto = quizServiceImp.selectAllByStatus(page);
            if (quizDto.size() == 0) {
                response.setMessage(messages.selectedError(Resources.QUIZZES.getName()));
                response.setStatus(HttpStatus.NOT_FOUND);
            } else {
                List<QuizResponseFilter> quizRequestDto = new ArrayList<>();
                for (QuizResponseFilter read : quizDto) {
                    quizRequestDto.add(ModelMapperUtil.mapper().map(read, QuizResponseFilter.class));
                }
                response.setMessage(messages.selected(Resources.QUIZZES.getName()));
                response.setStatus(HttpStatus.OK);
                response.setData(quizRequestDto);
                response.setPaging(page);
            }
            response.setTimestamp(DateTimeUtils.getCurrentTime());
            return ResponseEntity.ok(response);
        } catch (Exception ex) {
            response.setMessage(messages.selectedError(Resources.QUIZZES.getName()));
            response.setStatus(HttpStatus.NOT_FOUND);
            response.setData(null);
            response.setTimestamp(DateTimeUtils.getCurrentTime());
            return new ResponseEntity<>(response, HttpStatus.NOT_FOUND);
        }
    }
}
