package com.kshrd.exam_api.controller.rest;

import com.kshrd.exam_api.repository.model.FileInfo;
import com.kshrd.exam_api.service.upload.FileStorageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.method.annotation.MvcUriComponentsBuilder;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**********************************************************************
 * Original Author: Lon Dara
 * Created Date: 21-07-2020
 * Development Group: HRD_EXAM
 * Description: This class used to upload image file.
 **********************************************************************/

@CrossOrigin(origins = "*")
@RestController
@RequestMapping("api/v1/image")
public class FileRestController {
    @Value(value = "src/main/resources/image/")
    private String serverPath;

    @Value("${file.base.url}")

    private String imageUrl;

    private FileStorageService storageService;

    @Autowired
    public void setStorageService(FileStorageService storageService) {
        this.storageService = storageService;
    }


    /**********************************************************************
     * Original Author: Lon dara
     * Created Date: 21-07-2020
     * Description: Used to upload image file
     * Modification History :
     *     Date:
     *  Developer:Lon Dara
     *  TODO: - upload image
     *        - get image
     *        - get image by file name
     **********************************************************************/

    /**********************************************************************
     * Method for upload image file to AIP folder Spring Boot Directory.
     **********************************************************************/
    @PostMapping("/upload")
    public ResponseEntity<Map<String,Object>> uploadFile(@RequestParam("file") MultipartFile file){
        Map<String,Object> response = new HashMap<>();
        try {
            String fileName = storageService.save(file);
            response.put("message", "Uploaded the file successfully");
            response.put("status", true);
            response.put("data", (imageUrl + fileName));
            return ResponseEntity.status(HttpStatus.OK).body(response);
        }catch (Exception e){
            response.put("message","Could not upload the file:");
            response.put("status",false);
            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(response);
        }
    }

    /**********************************************************************
     * Method for get image file from API .
     **********************************************************************/
    @GetMapping("/files")
    public ResponseEntity<List<FileInfo>> getListFiles(){
        List<FileInfo> fileInfos = storageService.loadAll().map(path -> {
            String filename = path.getFileName().toString();
            String url = MvcUriComponentsBuilder
                    .fromMethodName(FileRestController.class,"getFile",path.getFileName().toString()).build().toString();
            return new FileInfo(filename,url);
        }).collect(Collectors.toList());
        return ResponseEntity.status(HttpStatus.OK).body(fileInfos);
    }

    /**********************************************************************
     * Method for get image file form API by file name.
     **********************************************************************/
    @GetMapping("/files/{filename:.+}")
    public ResponseEntity<Resource> getFile(@PathVariable String filename){
        Resource file = storageService.load(filename);
        return ResponseEntity.ok().header(HttpHeaders.CONTENT_DISPOSITION,"attachment; filename=\"" + file.getFilename() + "\"").body(file);
    }
}
