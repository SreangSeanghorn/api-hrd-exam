package com.kshrd.exam_api.controller.rest;

import com.kshrd.exam_api.configuration.JwtConfig.JwtTokenUtil;
import com.kshrd.exam_api.controller.request.JwtRequestDto;
import com.kshrd.exam_api.controller.response.BaseAPIResponse;
import com.kshrd.exam_api.controller.response.JwtResponse;
import com.kshrd.exam_api.repository.rep.TeacherRepository;
import com.kshrd.exam_api.service.serviceInterface.TeacherService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.dao.DataAccessException;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;

/**********************************************************************
 * Original Author:
 * Created Date:
 * Development
 * Created Date: 21-07-2020
 * Development Group: HRD_EXAM
 * Description:
 **********************************************************************/

@CrossOrigin(origins = "*")
@RestController


public class AuthenticationRestController {

    @Autowired
    private AuthenticationManager authenticationManager;
    @Autowired
    TeacherService teacherService;
    @Autowired
    TeacherRepository teacherRepository;
    @Autowired
    JwtTokenUtil jwtTokenUtil;
    @Autowired
    BCryptPasswordEncoder encoder;

    public static int teacherId;
    /**********************************************************************
     * Original Author:
     * Created Date: 21-07-2020
     * Description:
     * Modification History :
     *     Date:
     *  Developer:Lon Dara
     *  TODO:
     **********************************************************************/
    @PostMapping("${baseUrl}/authenticate")

    public ResponseEntity<?> createAuthenticationToken(@RequestBody JwtRequestDto request)throws Exception,DataAccessException{
        Boolean st=teacherRepository.getStatus(request.getUsername());

        List<String> teacherName=teacherRepository.getUsername();
        Boolean state=false;
        for(int i=0;i<teacherName.size();i++){
            if(teacherName.get(i).equalsIgnoreCase(request.getUsername())) {
                state=true;
            }
        }
        if(state==true){
            if (st == false) {
                return ResponseEntity.ok(new JwtResponse("Username doesn't match any account",""));
            } else {
                try {
                    authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(request.getUsername(), request.getPassword()));
                } catch (DataAccessException ex) {
                    throw new Exception("Error Connecting", ex);
                } catch (BadCredentialsException e) {
                    throw new Exception("Incorrect username or password", e);
                }
                final UserDetails userDetails = teacherService.loadUserByUsername(request.getUsername());
                int id = teacherRepository.selectTeacherID(request.getUsername());
                final String token = jwtTokenUtil.generateToken(userDetails);

                teacherId = id;

                return ResponseEntity.ok(new JwtResponse("get token success", token));
            }

        }
        else {
            return ResponseEntity.ok(new JwtResponse("Username doesn't match any account" ,""));
        }
        }


}
