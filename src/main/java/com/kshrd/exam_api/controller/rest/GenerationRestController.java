package com.kshrd.exam_api.controller.rest;

import com.kshrd.exam_api.controller.request.*;
import com.kshrd.exam_api.controller.response.BaseAPIResponse;
import com.kshrd.exam_api.controller.response.messages.BaseMessage;
import com.kshrd.exam_api.controller.response.messages.GenerationException;
import com.kshrd.exam_api.controller.response.messages.MessageProperties;
import com.kshrd.exam_api.controller.response.messages.Resources;
import com.kshrd.exam_api.controller.utils.DateTimeUtils;
import com.kshrd.exam_api.controller.utils.ModelMapperUtil;
import com.kshrd.exam_api.repository.model.GenerationDto;
import com.kshrd.exam_api.repository.rep.GenerationRepository;
import com.kshrd.exam_api.service.serviceImplement.GenerationServiceIpml;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;


/**********************************************************************
 * Original Author: Lai Manith
 * Created Date: 21-07-2020
 * Development Group: HRD_EXAM
 * Description: this class is created to store Generations of school
 **********************************************************************/

@CrossOrigin(origins = "*")
@RestController
@RequestMapping("${baseUrl}")
public class GenerationRestController {

    GenerationServiceIpml generationServiceIpml;
    private MessageProperties messages;
    @Autowired
    private GenerationRepository generationRepository;
    @Autowired
    public GenerationRestController(GenerationServiceIpml generationServiceIpml) {
        this.generationServiceIpml = generationServiceIpml;
    }

    @Autowired
    public void setMessages(MessageProperties messages) {
        this.messages = messages;
    }

    /**********************************************************************
     * Original Author: Lai Manith
     * Created Date: 21-07-2020
     * Description: crud generation
     * Modification History :
     *     Date:
     *  Developer:
     *  TODO: - post generation of school.
     *        - get all and get by ID.
     *        - update generation of school.
     *        - delete generation from database.
     **********************************************************************/

    /**********************************************************************
     *This method is used to post generation into database
     **********************************************************************/
    @PostMapping("/generations")
    public ResponseEntity<BaseAPIResponse<GenerationNonStatusRequestModel>> insert(@RequestBody GenerationNonIDStatusRequestModel generation) {

        BaseAPIResponse<GenerationNonStatusRequestModel> response = new BaseAPIResponse<>();
        Boolean state=true;
        /**
         * validate for posting null value
         */
        if (generation.getGeneration() == null || generation.getGeneration().equals("")) {
            response.setMessage(messages.nullValue(Resources.GENERATION_ONE.getName()));
            response.setTimestamp(DateTimeUtils.getCurrentTime());
            response.setStatus(HttpStatus.NOT_FOUND);
            return new ResponseEntity<>(response, HttpStatus.NOT_FOUND);
        }
        else {
            List<String> gen=generationRepository.getGeneration();
            for(int i=0;i<gen.size();i++){
                if(generation.getGeneration().equalsIgnoreCase(gen.get(i))){
                    state=false;
                }
            }
            if(state==true){
                generationRepository.setGenerationStatus();
                GenerationNonIDStatusRequestModel result = generationServiceIpml.insert(generation);
                GenerationNonStatusRequestModel mapResult = ModelMapperUtil.mapper().map(result, GenerationNonStatusRequestModel.class);
                mapResult.setId(generationServiceIpml.selectID());
                response.setMessage(messages.inserted(Resources.GENERATION_ONE.getName()));
                response.setData(mapResult);
                response.setStatus(HttpStatus.OK);
                response.setTimestamp(DateTimeUtils.getCurrentTime());
                return ResponseEntity.ok(response);
            }
            else {
                response.setMessage("Cannot create duplicate generation");
                response.setStatus(HttpStatus.NOT_ACCEPTABLE);
                response.setData(null);
                return ResponseEntity.ok(response);
            }

        }

    }


    /**********************************************************************
     *This method is used to get all generations from database
     **********************************************************************/
    @GetMapping("/generations")
    public ResponseEntity<BaseAPIResponse<List<GenerationRequestDto>>> select() {

        BaseAPIResponse<List<GenerationRequestDto>> response = new BaseAPIResponse<>();
        List<GenerationDto> generationDtos = generationServiceIpml.select();
        List<GenerationRequestDto> generationRequestDtos = new ArrayList<>();
        for (GenerationDto read : generationDtos) {
            generationRequestDtos.add(ModelMapperUtil.mapper().map(read, GenerationRequestDto.class));
        }
        response.setMessage(messages.selected(Resources.GENERATION.getName()));
        response.setData(generationRequestDtos);
        response.setTimestamp(DateTimeUtils.getCurrentTime());
        response.setStatus(HttpStatus.OK);
        return ResponseEntity.ok(response);

    }

    /*********************************************************************************
     *This method is used to get generation by ID from database
     *********************************************************************************/
    @GetMapping("/generations/{id}")
    public ResponseEntity<BaseAPIResponse<GenerationRequestDto>> selectByID(@PathVariable int id) {

        BaseAPIResponse<GenerationRequestDto> response = new BaseAPIResponse<>();
        try{
            GenerationDto generationDtos = generationServiceIpml.selectByID(id);
            GenerationRequestDto generationRequestDtos = ModelMapperUtil.mapper().map(generationDtos, GenerationRequestDto.class);
            response.setMessage(messages.selectedOne(Resources.GENERATION_ONE.getName()));
            response.setStatus(HttpStatus.OK);
            response.setData(generationRequestDtos);
            response.setTimestamp(DateTimeUtils.getCurrentTime());
        }catch (Exception ex){
            response.setMessage(messages.selectedError(Resources.GENERATION_ONE.getName()));
            response.setStatus(HttpStatus.NOT_FOUND);
            response.setData(null);
            response.setTimestamp(DateTimeUtils.getCurrentTime());
        }
        return ResponseEntity.ok(response);

    }

    /**********************************************************************
     *This method is used to be update generation from database
     **********************************************************************/
    @PutMapping("/generations/{id}")
    public ResponseEntity<BaseAPIResponse<GenerationRequestDto>> update(@PathVariable int id, @RequestBody GenerationNonIDStatusRequestModel generationRequestDto) {

        BaseAPIResponse<GenerationRequestDto> response = new BaseAPIResponse<>();
        GenerationDto generationDto = generationServiceIpml.selectByID(id);
        System.out.println("id in rest:"+id);

        if (generationRequestDto.getGeneration() == null || generationRequestDto.getGeneration().equals("")) {
            response.setMessage(messages.nullValue(Resources.GENERATION_ONE.getName()));
            response.setTimestamp(DateTimeUtils.getCurrentTime());
            response.setStatus(HttpStatus.NOT_FOUND);
            return new ResponseEntity<>(response, HttpStatus.NOT_FOUND);
        }
        else {
            generationServiceIpml.update(generationRequestDto, id);
            GenerationRequestDto generationRequestDto1 = ModelMapperUtil.mapper().map(generationRequestDto, GenerationRequestDto.class);
            generationRequestDto1.setId(generationDto.getGenerationId());
            generationRequestDto1.setStatus(generationDto.getStatus());
            response.setMessage(messages.updated(Resources.GENERATION_ONE.getName()));
            response.setStatus(HttpStatus.OK);
            response.setData(generationRequestDto1);
            response.setTimestamp(DateTimeUtils.getCurrentTime());
            return ResponseEntity.ok(response);
        }

    }

    /**********************************************************************
     *This method is used to delete generation from database
     **********************************************************************/
    @DeleteMapping("/generations/{id}")
    public ResponseEntity<BaseAPIResponse<GenerationNonStatusRequestModel>> delete(@PathVariable int id) {

        BaseAPIResponse<GenerationNonStatusRequestModel> response = new BaseAPIResponse<>();
        GenerationDto generationDto = generationServiceIpml.selectByID(id);
        if (generationServiceIpml.delete(id)) {
            GenerationNonStatusRequestModel generationRequestDto = ModelMapperUtil.mapper().map(generationDto, GenerationNonStatusRequestModel.class);
            response.setMessage(messages.deleted(Resources.GENERATION_ONE.getName()));
            response.setStatus(HttpStatus.OK);
            response.setTimestamp(DateTimeUtils.getCurrentTime());
            response.setData(generationRequestDto);
            return ResponseEntity.ok(response);
        } else {
            response.setTimestamp(DateTimeUtils.getCurrentTime());
            response.setMessage(messages.deletedError(Resources.GENERATION_ONE.getName()));
            response.setStatus(HttpStatus.NOT_FOUND);
            return new ResponseEntity<>(response, HttpStatus.NOT_FOUND);
        }

    }

    @PutMapping("/generations/status/{id}")
    public ResponseEntity<BaseAPIResponse<GenerationDto>> updateStatus(@PathVariable int id, @RequestBody GenerationRequestOnlyStatus status){
        BaseAPIResponse<GenerationDto> response = new BaseAPIResponse();
        try{
           if(status != null){
               List<GenerationDto> generation = generationServiceIpml.select();
               for(int i=0;i<generation.size();i++){
//                   if(generation.get(i).getGenerationId() == id){
                       if(generation.get(i).getGenerationId()  != id){
                           GenerationRequestOnlyStatus status1 = new GenerationRequestOnlyStatus();
                           generation.get(i).setStatus(false);
                           int ik = generation.get(i).getGenerationId();
                           status1.setStatus(generation.get(i).getStatus());
                           generationServiceIpml.UpdateStatus(ik,status1);
                       }
//                   }else{
//                       response.setMessage(messages.updatedError(Resources.RESULTS.getName()));
//                       response.setStatus(HttpStatus.BAD_REQUEST);
//                   }
               }
               if(generationServiceIpml.UpdateStatus(id,status)){
                   GenerationDto generationDtos = generationServiceIpml.selectByID(id);
                   response.setMessage(messages.updated(Resources.GENERATION_ONE.getName()));
                   response.setData(generationDtos);
                   response.setStatus(HttpStatus.OK);
               }else {
                   response.setMessage(messages.updatedError(Resources.GENERATION_ONE.getName()));
                   response.setStatus(HttpStatus.BAD_REQUEST);
               }
           }else{
               response.setMessage(messages.nullValue("Status"));
               response.setStatus(HttpStatus.BAD_REQUEST);
           }
       }catch (Exception ex){
            response.setMessage(ex.getMessage());
            response.setStatus(HttpStatus.BAD_REQUEST);
       }
        response.setTimestamp(DateTimeUtils.getCurrentTime());
        return ResponseEntity.ok(response);
    }
}
