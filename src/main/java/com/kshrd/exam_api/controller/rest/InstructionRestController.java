package com.kshrd.exam_api.controller.rest;


import com.kshrd.exam_api.controller.request.InstructionNonIDRequestModel;
import com.kshrd.exam_api.controller.request.InstructionNonIDsRequestModel;
import com.kshrd.exam_api.controller.request.InstructionRequestDto;
import com.kshrd.exam_api.controller.request.InstructionResponse;
import com.kshrd.exam_api.controller.response.BaseAPIResponse;
import com.kshrd.exam_api.controller.response.messages.BaseMessage;
import com.kshrd.exam_api.controller.response.messages.MessageProperties;
import com.kshrd.exam_api.controller.response.messages.Resources;
import com.kshrd.exam_api.controller.utils.DateTimeUtils;
import com.kshrd.exam_api.controller.utils.ModelMapperUtil;
import com.kshrd.exam_api.repository.model.InstructionDto;
import com.kshrd.exam_api.repository.rep.InstructionRepository;
import com.kshrd.exam_api.service.serviceImplement.InstructionSeviceImp;
import io.swagger.models.auth.In;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;


/**********************************************************************
 * Original Author:Lai Manith
 * Created Date: 21-07-2020
 * Development Group: HRD_EXAM
 * Description: this class is use to store in instruction of quiz
 **********************************************************************/

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("${baseUrl}")
public class InstructionRestController {

    InstructionSeviceImp instructionSeviceImp;
    @Autowired
    InstructionRepository instructionRepository;
    ModelMapper mapper=new ModelMapper();
    @Autowired
    public InstructionRestController(InstructionSeviceImp instructionSeviceImp) {
        this.instructionSeviceImp = instructionSeviceImp;
    }
    private MessageProperties messages;
    @Autowired
    public void setMessages(MessageProperties messages) {
        this.messages = messages;
    }


    /**********************************************************************
     * Original Author: Lai Manith
     * Created Date: 21-07-2020
     * Description: crud instruction
     * Modification History : all day
     *     Date:
     *  Developer: Lai Manith
     *  TODO: - post instruction of quiz.
     *        - get all and get by ID.
     *        - update instruction of quiz.
     *        - delete instruction from database.
     **********************************************************************/

    /**********************************************************************
     *This method is used to get all instructions from database
     **********************************************************************/
    @GetMapping("/instructions")
    public ResponseEntity<BaseAPIResponse<List<InstructionRequestDto>>> select() {

        BaseAPIResponse<List<InstructionRequestDto>> response = new BaseAPIResponse<>();
        List<InstructionDto> instructionDto = instructionSeviceImp.select();
        List<InstructionRequestDto> instructionRequestDto = new ArrayList<>();
        for (InstructionDto read : instructionDto) {
            instructionRequestDto.add(ModelMapperUtil.mapper().map(read, InstructionRequestDto.class));
        }
        response.setMessage(BaseMessage.Success.SELECT_ALL_RECORD_SUCCESS.getMessage());
        response.setStatus(HttpStatus.OK);
        response.setData(instructionRequestDto);
        response.setTimestamp(DateTimeUtils.getCurrentTime());
        return ResponseEntity.ok(response);

    }

    /**********************************************************************
     *This method is used to get generation by ID from database
     **********************************************************************/
    @GetMapping("/instructions/{id}")
    public ResponseEntity<BaseAPIResponse<InstructionRequestDto>> selectByID(@PathVariable int id) {

        BaseAPIResponse<InstructionRequestDto> response = new BaseAPIResponse<>();
        try{
            InstructionDto instructionDto = instructionSeviceImp.selectByID(id);
            InstructionRequestDto instructionRequestDto = ModelMapperUtil.mapper().map(instructionDto, InstructionRequestDto.class);
            response.setMessage((BaseMessage.Success.SELECT_ONE_RECORD_SUCCESS.getMessage()));
            response.setStatus(HttpStatus.OK);
            response.setData(instructionRequestDto);
            response.setTimestamp(DateTimeUtils.getCurrentTime());
            return ResponseEntity.ok(response);
        }catch (Exception ex){
            response.setMessage(BaseMessage.Error.SELECT_ERROR.getMessage());
            response.setStatus(HttpStatus.NOT_FOUND);
            response.setData(null);
            response.setTimestamp(DateTimeUtils.getCurrentTime());
            return new ResponseEntity<>(response, HttpStatus.NOT_FOUND);
        }

    }


    @GetMapping("/instructions/getByQuiz/{id}")
    public ResponseEntity<BaseAPIResponse<List<InstructionResponse>>> getInstructionByQuizId(@PathVariable int id) {

        BaseAPIResponse<List<InstructionResponse>> response = new BaseAPIResponse<>();
        try{
            List<InstructionResponse> instruction = instructionSeviceImp.getInstructionByQuiz(id);
            response.setMessage((BaseMessage.Success.SELECT_ONE_RECORD_SUCCESS.getMessage()));
            response.setStatus(HttpStatus.OK);
            response.setData(instruction);
            response.setTimestamp(DateTimeUtils.getCurrentTime());
            return ResponseEntity.ok(response);
        }catch (Exception ex){
            response.setMessage(BaseMessage.Error.SELECT_ERROR.getMessage());
            response.setStatus(HttpStatus.NOT_FOUND);
            response.setData(null);
            response.setTimestamp(DateTimeUtils.getCurrentTime());
            return new ResponseEntity<>(response, HttpStatus.NOT_FOUND);
        }

    }

    /**********************************************************************
     *This method is used to post instruction into database
     **********************************************************************/
    @PostMapping("/instructions")
    public ResponseEntity<BaseAPIResponse<InstructionRequestDto>> insert(@RequestBody InstructionNonIDRequestModel instruction) {

        BaseAPIResponse<InstructionRequestDto> response = new BaseAPIResponse<>();
        boolean status=true;
        if (instruction.getInstruction() == null || instruction.getInstruction().equals("") || instruction.getQuizId() == 0) {

            if (instruction.getInstruction() == null || instruction.getInstruction().equals("")) {
                response.setMessage(BaseMessage.Error.INSERT_ERROR.getMessage());
                response.setData(null);
                response.setTimestamp(DateTimeUtils.getCurrentTime());
                response.setStatus(HttpStatus.NOT_FOUND);
            }
            if (instruction.getQuizId() == 0) {
                response.setMessage(BaseMessage.Error.INSERT_ERROR.getMessage());
                response.setData(null);
                response.setTimestamp(DateTimeUtils.getCurrentTime());
                response.setStatus(HttpStatus.NOT_FOUND);
            }
            return new ResponseEntity<>(response, HttpStatus.NOT_FOUND);
        }
        else {
            List<String> section=instructionRepository.selectSection(instruction.getQuizId());
            for(int i=0;i<section.size();i++){
                if(instruction.getInstruction().trim().equalsIgnoreCase(section.get(i))){
                    System.out.println(section.get(i));
                    status=false;
                }
            }
            if(status==true){
                InstructionNonIDRequestModel result = instructionSeviceImp.insert(instruction);
                int id=instructionRepository.selectInstructionId();
                InstructionRequestDto instructionDto=mapper.map(result, InstructionRequestDto.class);
                instructionDto.setId(id);
                response.setMessage(BaseMessage.Success.INSERT_SUCCESS.getMessage());
                response.setData(instructionDto);
                response.setTimestamp(DateTimeUtils.getCurrentTime());
                response.setStatus(HttpStatus.OK);

            }
            else{
                response.setMessage("Cannot create duplictae section");
                response.setStatus(HttpStatus.NO_CONTENT);
                response.setTimestamp(DateTimeUtils.getCurrentTime());
            }
            return ResponseEntity.ok(response);
        }

    }



    /**********************************************************************
     *This method is used to be update instruction into database
     **********************************************************************/
    @PutMapping("/instructions/{id}")
    public ResponseEntity<BaseAPIResponse<InstructionRequestDto>> update(@RequestBody InstructionNonIDsRequestModel instructionRequestDto, @PathVariable int id) {

        BaseAPIResponse<InstructionRequestDto> response = new BaseAPIResponse<>();
        InstructionDto instructionDto = instructionSeviceImp.selectByID(id);
        InstructionRequestDto instructionRequestDto1 = ModelMapperUtil.mapper().map(instructionDto, InstructionRequestDto.class);
        if(instructionRequestDto.getInstruction() == null || instructionRequestDto.getInstruction().equals("")) {
            response.setTimestamp(DateTimeUtils.getCurrentTime());
            response.setMessage(BaseMessage.Error.UPDATE_ERROR.getMessage());
            response.setStatus(HttpStatus.NOT_FOUND);
            return new ResponseEntity<>(response, HttpStatus.NOT_FOUND);
        }
        else {
            InstructionRequestDto instructionRequestDto2 = ModelMapperUtil.mapper().map(instructionRequestDto, InstructionRequestDto.class);
            instructionRequestDto2.setId(instructionRequestDto1.getId());
            instructionRequestDto2.setQuizId(instructionRequestDto1.getQuizId());
            instructionSeviceImp.update(instructionRequestDto, id);
            response.setTimestamp(DateTimeUtils.getCurrentTime());
            response.setMessage(BaseMessage.Success.UPDATE_SUCCESS.getMessage());
            response.setData(instructionRequestDto2);
            response.setStatus(HttpStatus.OK);
            return new ResponseEntity<>(response, HttpStatus.OK);
        }

    }



    /**********************************************************************
     *This method is used to delete instruction from database
     **********************************************************************/
    @DeleteMapping("/instructions/{id}")
    public ResponseEntity<BaseAPIResponse<InstructionNonIDRequestModel>> delete(@PathVariable int id) {

        BaseAPIResponse<InstructionNonIDRequestModel> response = new BaseAPIResponse<>();
        InstructionDto instructionDto = instructionSeviceImp.selectByID(id);
        if (instructionSeviceImp.delete(id)){
            InstructionNonIDRequestModel instructionRequestDto = ModelMapperUtil.mapper().map(instructionDto, InstructionNonIDRequestModel.class);
            response.setTimestamp(DateTimeUtils.getCurrentTime());
            response.setMessage((BaseMessage.Success.DELETE_SUCCESS.getMessage()));
            response.setStatus(HttpStatus.OK);
            response.setData(instructionRequestDto);
            return new ResponseEntity<>(response, HttpStatus.OK);
        }
        else {
            response.setTimestamp(DateTimeUtils.getCurrentTime());
            response.setMessage(BaseMessage.Error.DELETE_ERROR.getMessage());
            response.setStatus(HttpStatus.NOT_FOUND);
            return new ResponseEntity<>(response, HttpStatus.NOT_FOUND);
        }

    }
}
