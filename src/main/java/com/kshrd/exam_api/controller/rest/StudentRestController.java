package com.kshrd.exam_api.controller.rest;

import com.kshrd.exam_api.controller.request.StudentRequestDto;
import com.kshrd.exam_api.controller.request.StudentRequestFromUser;
import com.kshrd.exam_api.controller.response.BaseAPIResponse;
import com.kshrd.exam_api.controller.response.messages.MessageProperties;
import com.kshrd.exam_api.controller.response.messages.Resources;
import com.kshrd.exam_api.controller.utils.DateTimeUtils;
import com.kshrd.exam_api.controller.utils.ModelMapperUtil;
import com.kshrd.exam_api.repository.model.StudentDto;
import com.kshrd.exam_api.repository.rep.GenerationRepository;
import com.kshrd.exam_api.repository.rep.StudentRepository;
import com.kshrd.exam_api.service.serviceImplement.StudentServiceImp;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.sql.Timestamp;
import java.util.List;

/**********************************************************************
 * Original Author: Lon Dara
 * Created Date: 21-07-2020
 * Development Group: HRD_EXAM
 * Description: This class create for control information of students.
 **********************************************************************/
@CrossOrigin(origins = "*")
@RestController
@RequestMapping("${baseUrl}")
public class StudentRestController {
    private StudentServiceImp studentServiceImp;

    private ModelMapperUtil mapperUtil;

    private DateTimeUtils timeUtils;

    private MessageProperties messages;
    @Autowired
    GenerationRepository generationRepository;
    @Autowired
    public void setMessages(MessageProperties messages) {
        this.messages = messages;
    }
    @Autowired
    private StudentRepository studentRepository;
    @Autowired
    public void setStudentServiceImp(StudentServiceImp studentServiceImp) {
        this.studentServiceImp = studentServiceImp;
    }

    @Autowired
    public void setMapperUtil(ModelMapperUtil mapperUtil) {
        this.mapperUtil = mapperUtil;
    }

    @Autowired
    public void setTimeUtils(DateTimeUtils timeUtils) {
        this.timeUtils = timeUtils;
    }

    /**********************************************************************
     * Original Author: Lon Dara
     * Created Date: 21-07-2020
     * Description: post students to database
     * Modification History : all day
     *     Date:
     *  Developer:Lon Dara
     *  TODO: - post information students to Database
     *        - get all and get by ID and get By ClassName and By generation and By CardId
     *        - update information students.
     *        - delete information students from data base.
     **********************************************************************/

    /**********************************************************************
     * This method it used to post information of students to database.
     **********************************************************************/
    @PostMapping("/students")
    public ResponseEntity<BaseAPIResponse<StudentRequestDto>> insert(@RequestBody StudentRequestFromUser requestDto){
        BaseAPIResponse response = new BaseAPIResponse();
        StudentRequestDto studentRequestDto = new StudentRequestDto();
        try{
            int genId=generationRepository.getGenerationId();
            studentRequestDto = mapperUtil.mapper().map(requestDto,StudentRequestDto.class);
            studentRequestDto.setGenerationId(genId);
            if(studentRequestDto.getClassName() == null || requestDto.getClassName().isEmpty()){
                response.setMessage(messages.nullValue("ClassName"));
                response.setStatus(HttpStatus.BAD_REQUEST);
            }else if(studentRequestDto.getCardID().equalsIgnoreCase(" ")){
                response.setMessage(messages.nullValue("CardID"));
                response.setStatus(HttpStatus.BAD_REQUEST);
            }else if(studentRequestDto.getGender() == null || requestDto.getGender().isEmpty()){
                response.setMessage(messages.nullValue("Gender"));
                response.setStatus(HttpStatus.BAD_REQUEST);
            }else if(studentRequestDto.getName() == null || requestDto.getName().isEmpty()||requestDto.getName().trim().isEmpty()){
                response.setMessage(messages.nullValue("Name"));
                response.setStatus(HttpStatus.BAD_REQUEST);
            }else{
                List<String> card_id=studentRepository.selectCardId();
                Boolean state=true;
                for (int i=0;i<card_id.size();i++){
                    if(card_id.get(i).equalsIgnoreCase(studentRequestDto.getCardID())){
                        state=false;
                        response.setMessage("Student has been registered already");
                        response.setStatus(HttpStatus.NOT_ACCEPTABLE);
                    }
                }
                int len=studentRequestDto.getCardID().length();
                for(int k=0;k<len;k++){
                    if (studentRequestDto.getCardID().contains(" ")) {
                        state=false;
                        response.setMessage("CardID cannot contains white space");
                        response.setStatus(HttpStatus.NOT_ACCEPTABLE);
                    }
                }

                if (state==true){
                    studentRequestDto.setName(studentRequestDto.getName().trim());
                    requestDto.setName(requestDto.getName().trim());
                    studentRequestDto.setClassName(studentRequestDto.getClassName().toUpperCase());
                    requestDto.setClassName(studentRequestDto.getClassName());
                    if(studentServiceImp.insert(studentRequestDto)) {
                        response.setMessage(messages.inserted(Resources.STUDENT.getName()));
                        response.setData(requestDto);
                        response.setStatus(HttpStatus.OK);
                    }else{
                        response.setMessage(messages.inserted(Resources.STUDENT.getName()));
                        response.setStatus(HttpStatus.BAD_REQUEST);
                    }
                }

            }
            response.setTimestamp(timeUtils.getCurrentTime());
            return ResponseEntity.ok(response);
        }catch (Exception ex){
            response.setMessage(messages.insertError(Resources.STUDENTS.getName()));
            response.setStatus(HttpStatus.BAD_REQUEST);
            response.setTimestamp(timeUtils.getCurrentTime());
            return ResponseEntity.ok(response);
        }

    }



    /**********************************************************************
     *This method it used to get all information of students from database.
     **********************************************************************/
    @GetMapping("/students")
    public ResponseEntity<BaseAPIResponse<List<StudentDto>>> select(@RequestParam(value="generationId",required = false,defaultValue="0") int generationId){
        BaseAPIResponse<List<StudentDto>> response = new BaseAPIResponse<>();
        try {
            List<StudentDto> studentDtoList = studentServiceImp.select(generationId);
            if(studentDtoList.size()!=0) {
                response.setMessage(messages.selected(Resources.STUDENTS.getName()));
                response.setData(studentDtoList);
                response.setStatus(HttpStatus.OK);
            }else {
                response.setMessage(messages.selectedError(Resources.STUDENTS.getName()));
                response.setStatus(HttpStatus.BAD_REQUEST);
            }
            response.setTimestamp(timeUtils.getCurrentTime());
            return ResponseEntity.ok(response);
        }catch (Exception e){
            response.setMessage(e.getMessage());
            response.setStatus(HttpStatus.BAD_REQUEST);
            response.setTimestamp(timeUtils.getCurrentTime());
            return ResponseEntity.ok(response);
        }

    }


    @GetMapping("/students/count")
    public ResponseEntity<BaseAPIResponse<Integer>> countStudentRecords() {

        BaseAPIResponse<Integer> response = new BaseAPIResponse<>();
        try{
            int num=studentRepository.countStudentRecords();
            response.setData(num);
            response.setMessage("The Number of Student has been count successfully");
            response.setStatus(HttpStatus.OK);
        }catch (Exception ex){
            response.setMessage(messages.selectedError(Resources.QUIZ.getName()));
            response.setStatus(HttpStatus.NOT_FOUND);
            response.setData(null);
            response.setTimestamp(DateTimeUtils.getCurrentTime());
        }
        return ResponseEntity.ok(response);

    }

    /**********************************************************************
     *This method it used to get information of students by id from database.
     **********************************************************************/
    @GetMapping("/students/{id}")
    public ResponseEntity<BaseAPIResponse<StudentDto>> selectById(@PathVariable int id){
        BaseAPIResponse<StudentDto> response = new BaseAPIResponse<>();
        try {
            if(id == 0){
                response.setMessage(messages.nullValue("ID"));
                response.setStatus(HttpStatus.NOT_FOUND);
            }else {
                StudentDto studentDto = studentServiceImp.selectById(id);
                if (studentDto != null) {
                    response.setMessage(messages.selectedOne(Resources.STUDENT.getName()));
                    response.setData(studentDto);
                    response.setStatus(HttpStatus.OK);
                } else {
                    response.setMessage(messages.selectedError(Resources.STUDENT.getName()));
                    response.setStatus(HttpStatus.NOT_FOUND);
                }
            }
            response.setTimestamp(timeUtils.getCurrentTime());
            return ResponseEntity.ok(response);
        }catch (Exception e){
            response.setMessage(e.getMessage());
            response.setStatus(HttpStatus.BAD_REQUEST);
            response.setTimestamp(timeUtils.getCurrentTime());
            return ResponseEntity.ok(response);
        }
    }


    /**********************************************************************
     *This method it used to get information of students by class from database.
     **********************************************************************/
    @GetMapping("/students/class")
    public ResponseEntity<BaseAPIResponse<List<StudentDto>>> selectByClass(@RequestParam(required = false) String classname,@RequestParam(value="generationId",required = false,defaultValue="0") int generationId){
        BaseAPIResponse<List<StudentDto>> response = new BaseAPIResponse<>();
        System.out.println("rest:"+classname+generationId);
        System.out.println("student:"+studentServiceImp.selectByClass(classname,generationId));
        try {
            if(classname != null && classname.isEmpty() != true) {
                List<StudentDto> studentDto = studentServiceImp.selectByClass(classname,generationId);
                if (studentDto.size() != 0) {
                    response.setMessage(messages.selectedOne(Resources.STUDENTS.getName()));
                    response.setData(studentDto);
                    response.setStatus(HttpStatus.OK);
                } else {
                    response.setMessage(messages.selectedError(Resources.STUDENTS.getName()));
                    response.setStatus(HttpStatus.NOT_FOUND);
                }
            }else{
                response.setMessage(messages.nullValue("ClassName"));
                response.setStatus(HttpStatus.NOT_FOUND);
            }
            response.setTimestamp(timeUtils.getCurrentTime());
            return ResponseEntity.ok(response);
        }catch (Exception e){
            response.setMessage(e.getMessage());
            response.setStatus(HttpStatus.BAD_REQUEST);
            response.setTimestamp(timeUtils.getCurrentTime());
            return ResponseEntity.ok(response);
        }
    }

    /**********************************************************************
     *  This method it used to get information of students by generation from database.
     **********************************************************************/
    @GetMapping("/students/gn")
    public ResponseEntity<BaseAPIResponse<List<StudentDto>>> selectByGeneration(@RequestParam(required = false, defaultValue = "0") String generation){
        BaseAPIResponse<List<StudentDto>> response = new BaseAPIResponse<>();
        try {
            if(generation != null){
                List<StudentDto> studentDto = studentServiceImp.selectByGeneration(generation);
                if(studentDto.size()!=0){
                    response.setMessage(messages.selectedOne(Resources.STUDENTS.getName()));
                    response.setData(studentDto);
                    response.setStatus(HttpStatus.OK);
                }else{
                    response.setMessage(messages.selectedError(Resources.STUDENTS.getName()));
                    response.setStatus(HttpStatus.NOT_FOUND);
                }
            }else{
                response.setMessage(messages.nullValue("Generation"));
                response.setStatus(HttpStatus.NOT_FOUND);
            }
            response.setTimestamp(timeUtils.getCurrentTime());
            return ResponseEntity.ok(response);
        }catch (Exception e){
            response.setMessage(e.getMessage());
            response.setStatus(HttpStatus.BAD_REQUEST);
            response.setTimestamp(timeUtils.getCurrentTime());
            return ResponseEntity.ok(response);
        }
    }


    @GetMapping("/students/getClassName")
    public ResponseEntity<BaseAPIResponse<List<String>>> selectClassName(@RequestParam(value="generationId",required = false,defaultValue="0") int generationId){
        BaseAPIResponse<List<String>> res=new BaseAPIResponse<>();
       List<String> list=studentRepository.selectClassName(generationId);
       res.setStatus(HttpStatus.OK);
       res.setMessage("All Class Name has been found");
       res.setData(list);
       return ResponseEntity.ok(res);
    }




    /**********************************************************************
     *This method it used to delete information of students by id from database.
     **********************************************************************/
    @DeleteMapping("/students/{id}")
    public ResponseEntity<BaseAPIResponse<StudentDto>> delete(@PathVariable int id){
        BaseAPIResponse<StudentDto> response = new BaseAPIResponse();
        try {
            StudentDto studentDto=studentServiceImp.selectById(id);

            if(studentDto==null){
                response.setMessage(messages.deletedError(Resources.STUDENT.getName()));
                response.setStatus(HttpStatus.NOT_FOUND);
            }else{
                int i = studentServiceImp.delete(id);
                response.setData(studentDto);
                response.setMessage(messages.deleted(Resources.STUDENT.getName()));
                response.setStatus(HttpStatus.OK);
            }
            response.setTimestamp(timeUtils.getCurrentTime());
            return ResponseEntity.ok(response);
        }catch (Exception e){
            response.setMessage(e.getMessage());
            response.setStatus(HttpStatus.BAD_REQUEST);
            response.setTimestamp(timeUtils.getCurrentTime());
            return ResponseEntity.ok(response);
        }
    }

    /**********************************************************************
     *This method it used to update information of students by student id.
     **********************************************************************/
    @PutMapping("/students/{id}")
    @ApiOperation(httpMethod = "PUT",value = "Updated")
    public ResponseEntity<BaseAPIResponse<StudentRequestFromUser>> update(@PathVariable int id, @RequestBody StudentRequestFromUser requestDto){
        BaseAPIResponse<StudentRequestFromUser> response = new BaseAPIResponse();
        System.out.println(requestDto);
        try {
            if(requestDto.getClassName() == null || requestDto.getClassName().isEmpty()){
                response.setMessage(messages.nullValue("ClassName"));
                response.setStatus(HttpStatus.BAD_REQUEST);
            }else if(requestDto.getName() == null || requestDto.getName().isEmpty()||requestDto.getName().trim().isEmpty()){
                response.setMessage(messages.nullValue("Name"));
                response.setStatus(HttpStatus.BAD_REQUEST);
            }else if(requestDto.getGender() == null || requestDto.getGender().isEmpty()){
                response.setMessage(messages.nullValue("Gender"));
                response.setStatus(HttpStatus.BAD_REQUEST);
            }else if(requestDto.getCardID().equalsIgnoreCase("")){
                response.setMessage(messages.nullValue("CardID"));
                response.setStatus(HttpStatus.BAD_REQUEST);
            }else{
                List<String> card_id=studentRepository.selectCardId();
                System.out.println("card:"+card_id);
                Boolean state=true;
                for (int i=0;i<card_id.size();i++){
                    if(card_id.get(i).equalsIgnoreCase(requestDto.getCardID().trim())){
                        state=false;
                        System.out.println("Card Id:"+requestDto.getCardID());
                        response.setMessage("Record is Failed to Update");
                        response.setStatus(HttpStatus.NOT_ACCEPTABLE);
                    }
                }
                int len=requestDto.getCardID().length();
                for(int k=0;k<len;k++){
                    if (requestDto.getCardID().contains(" ")) {
                        state=false;
                        response.setMessage("CardID cannot contains white space");
                        response.setStatus(HttpStatus.NOT_ACCEPTABLE);
                    }
                }
                boolean request = false;
                if(state==true){
                    request= studentServiceImp.update(id,requestDto);
                }
                
                if(request){
                    System.out.println(request);
                    response.setMessage(messages.updated(Resources.STUDENT.getName()));
                    response.setData(requestDto);
                    response.setStatus(HttpStatus.OK);
                }
            }
            response.setTimestamp(timeUtils.getCurrentTime());
            return ResponseEntity.ok(response);
        }catch (Exception e){
            response.setMessage(messages.updatedError("Record"));
            response.setStatus(HttpStatus.BAD_REQUEST);
            response.setTimestamp(timeUtils.getCurrentTime());
            return ResponseEntity.ok(response);
        }
    }

    /**********************************************************************
     *This method it used to login.
     **********************************************************************/
    @GetMapping("/students/login")
    public ResponseEntity<BaseAPIResponse<StudentDto>> login(@RequestParam(required = true) String CardId){
        BaseAPIResponse<StudentDto> response = new BaseAPIResponse();
        try {
              if(CardId.isEmpty()){
                  response.setMessage(messages.nullValue("CardID"));
                  response.setStatus(HttpStatus.BAD_REQUEST);
              }else {
                  StudentDto login = studentServiceImp.login(CardId);
                  if (login != null) {
                      response.setMessage(messages.LoginSuccess(Resources.STUDENT.getName()));
                      response.setData(login);
                      response.setStatus(HttpStatus.OK);

                  } else {
                      response.setMessage(messages.LoginFail(Resources.STUDENT.getName()));
                      response.setStatus(HttpStatus.NOT_FOUND);
                  }
              }

        }catch (Exception e){
            response.setMessage(messages.nullValue("CardID"));
            response.setStatus(HttpStatus.NOT_FOUND);

        }
        response.setTimestamp(new Timestamp(System.currentTimeMillis()));
        return ResponseEntity.ok(response);
    }

}
