package com.kshrd.exam_api.controller.rest;

import com.kshrd.exam_api.controller.request.QuestionRequestDto;
import com.kshrd.exam_api.controller.response.BaseAPIResponse;
import com.kshrd.exam_api.controller.response.QuestionResponseDto;
import com.kshrd.exam_api.controller.response.messages.MessageProperties;
import com.kshrd.exam_api.controller.response.messages.Resources;
import com.kshrd.exam_api.controller.utils.DateTimeUtils;
import com.kshrd.exam_api.controller.utils.ModelMapperUtil;
import com.kshrd.exam_api.repository.model.QuestionDto;
import com.kshrd.exam_api.repository.rep.QuestionRepository;
import com.kshrd.exam_api.service.serviceImplement.QuestionServiceImp;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import java.util.ArrayList;
import java.util.List;

/**********************************************************************
 * Original Author: Dien Sereyrith
 * Created Date: 21-07-2020
 * Development Group: HRD_EXAM
 * Description: This class is described a bout how to work with questions
 **********************************************************************/
@CrossOrigin(origins = "*")
@RestController
@RequestMapping("api/v1/questions")
public class QuestionRestController {

    private QuestionServiceImp questionServiceImp;
    private MessageProperties message;
    private ModelMapperUtil mapperUtil;
    private DateTimeUtils timeUtils;
    @Autowired
    private QuestionRepository questionRepository;
    @Autowired
    public QuestionRestController(QuestionServiceImp questionServiceImp) {
        this.questionServiceImp = questionServiceImp;
    }

    @Autowired
    public void setMessage(MessageProperties message) {
        this.message = message;
    }

    @Autowired
    public void setMapperUtil(ModelMapperUtil mapperUtil) {
        this.mapperUtil = mapperUtil;
    }

    @Autowired
    public void setTimeUtils(DateTimeUtils timeUtils) {
        this.timeUtils = timeUtils;
    }


    /**********************************************************************
     * Original Author: Dien Sereyrith
     * Created Date: 21-07-2020
     * Description: create methods to work with questions table
     * Modification History : everyday
     *     Date:
     *  Developer: Dien Sereyrith
     *  TODO: - get all questions from question table of database
     *        - get question by id from question table of database
     *        - post question to question table of database
     *        - delete question from question table of database
     *        - update question in question table of database
     **********************************************************************/

    /**********************************************************************
     * This method is get all questions from question table of database
     **********************************************************************/
    @GetMapping()
    public ResponseEntity<BaseAPIResponse<List<QuestionDto>>> findAll(){

        BaseAPIResponse<List<QuestionDto>> response = new BaseAPIResponse<>();
        ModelMapper mapper = new ModelMapper();

        try {

            List<QuestionDto> questionDtoList = questionServiceImp.findAll();

            if (questionDtoList==null){

                response.setMessage(message.nullValue(Resources.QUESTIONS.getName()));
                response.setStatus(HttpStatus.NO_CONTENT);
                response.setTimestamp(timeUtils.getCurrentTime());
                return new ResponseEntity<>(response,HttpStatus.NO_CONTENT);

            }else {

                response.setMessage(message.selected(Resources.QUESTIONS.getName()));
                List<QuestionDto> requestDtoList = new ArrayList<>();

                for (QuestionDto questionDto : questionDtoList){
                    requestDtoList.add(mapper.map(questionDto, QuestionDto.class));
                }

                response.setData(requestDtoList);
                response.setStatus(HttpStatus.OK);
                response.setTimestamp(timeUtils.getCurrentTime());
                return ResponseEntity.ok(response);

            }

        }catch (Exception e){

            response.setMessage(e.getMessage());
            response.setStatus(HttpStatus.BAD_REQUEST);
            response.setTimestamp(timeUtils.getCurrentTime());
            return new ResponseEntity(response, HttpStatus.BAD_REQUEST);

        }

    }

    /**********************************************************************
     * This method is get a question by id from question table of database
     **********************************************************************/
    @GetMapping("/{id}")
    public ResponseEntity<BaseAPIResponse<QuestionDto>> findOne(@PathVariable int id){

        BaseAPIResponse<QuestionDto> response = new BaseAPIResponse<>();
        ModelMapper mapper = new ModelMapper();
        System.out.println("id ======>"+id);
        try {

            QuestionDto questionDto = questionServiceImp.findOne(id);
            System.out.println("::::::::::::::"+questionDto);
            if (questionDto!=null){
                response.setMessage(message.selectedOne(Resources.QUESTIONS.getName()));
                QuestionDto requestDto = mapper.map(questionDto, QuestionDto.class);
                response.setData(requestDto);
                response.setStatus(HttpStatus.OK);
                response.setTimestamp(timeUtils.getCurrentTime());
                return ResponseEntity.ok(response);

            }else {

                response.setMessage(message.notRecord(Resources.QUESTIONS.getName()));
                response.setStatus(HttpStatus.NO_CONTENT);
                response.setTimestamp(timeUtils.getCurrentTime());
                return new ResponseEntity<>(response,HttpStatus.NO_CONTENT);

            }
        }catch (Exception e){

            response.setMessage(e.getMessage());
            response.setStatus(HttpStatus.BAD_REQUEST);
            response.setTimestamp(timeUtils.getCurrentTime());
            return new ResponseEntity<>(response,HttpStatus.BAD_REQUEST);
        }

    }

    /**********************************************************************
     * This method is get a question by quizid from question table of database
     **********************************************************************/
    @GetMapping("quizID/{id}")
    public ResponseEntity<BaseAPIResponse<List<QuestionDto>>> findQuizId(@PathVariable int id){

        BaseAPIResponse<List<QuestionDto>> response = new BaseAPIResponse<>();
        ModelMapper mapper = new ModelMapper();

        try {

            List<QuestionDto> questionDto = questionServiceImp.findByQuizId(id);

            if (questionDto!=null){
                response.setMessage(message.selectedOne(Resources.QUESTIONS.getName()));
//                QuestionDto requestDto = mapper.map(questionDto, QuestionDto.class);
                response.setData(questionDto);
                response.setStatus(HttpStatus.OK);
                response.setTimestamp(timeUtils.getCurrentTime());
                return ResponseEntity.ok(response);

            }else {

                response.setMessage(message.notRecord(Resources.QUESTIONS.getName()));
                response.setStatus(HttpStatus.NO_CONTENT);
                response.setTimestamp(timeUtils.getCurrentTime());
                return new ResponseEntity<>(response,HttpStatus.NO_CONTENT);

            }
        }catch (Exception e){

            response.setMessage(e.getMessage());
            response.setStatus(HttpStatus.BAD_REQUEST);
            response.setTimestamp(timeUtils.getCurrentTime());
            return new ResponseEntity<>(response,HttpStatus.BAD_REQUEST);
        }

    }

    /**********************************************************************
     * This method is post a question into question table of database
     **********************************************************************/
    @PostMapping()
    public ResponseEntity<BaseAPIResponse<QuestionRequestDto>> insert(@RequestBody QuestionResponseDto questionResponseDto){
        BaseAPIResponse<QuestionRequestDto> response = new BaseAPIResponse<>();
        ModelMapper mapper = new ModelMapper();
        try {
//            if (!questionResponseDto.getQuestion().trim().isEmpty()){
                QuestionDto questionDto = mapper.map(questionResponseDto, QuestionDto.class);
                questionDto.setQuestion(questionDto.getQuestion().trim());
                if(questionDto.getAnswer().getAnswerId()==0){
                    questionRepository.insertNoAnswer(questionDto);
                }
                else {
                    questionServiceImp.insert(questionDto);
                }

            int id=questionRepository.selectMaxQuestionid();

            questionDto.setQuestionId(id);
            QuestionRequestDto requestDto = mapper.map(questionDto, QuestionRequestDto.class);
            System.out.println(requestDto.getQuestionId());
                response.setMessage(message.inserted(Resources.QUESTIONS.getName()));
                response.setData(requestDto);
                response.setStatus(HttpStatus.CREATED);
                response.setTimestamp(timeUtils.getCurrentTime());
                return ResponseEntity.ok(response);
//            }else {
//                response.setMessage(message.insertError(Resources.QUESTIONS.getName()));
//                response.setStatus(HttpStatus.INTERNAL_SERVER_ERROR);
//                response.setTimestamp(timeUtils.getCurrentTime());
//                return new ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);
//            }
        }catch (Exception e){
            response.setMessage(e.getMessage());
            response.setStatus(HttpStatus.BAD_REQUEST);
            response.setTimestamp(timeUtils.getCurrentTime());
            return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
        }
    }

    /**********************************************************************
     * This method is delete a question by id in question table of database
     **********************************************************************/

    @DeleteMapping("/{id}")
    public ResponseEntity<BaseAPIResponse<QuestionResponseDto>> delete(@PathVariable("id") int id){

        BaseAPIResponse<QuestionResponseDto> response = new BaseAPIResponse<>();

        try {
            QuestionDto questionDto= questionServiceImp.findOne(id);
           ModelMapper mapper=new ModelMapper();
            QuestionResponseDto responseDto=mapper.map(questionDto,QuestionResponseDto.class);
            if (questionServiceImp.delete(id)) {
                response.setData(responseDto);
                response.setMessage(message.deleted(Resources.QUESTIONS.getName()));
                response.setStatus(HttpStatus.OK);
                response.setTimestamp(timeUtils.getCurrentTime());
                return ResponseEntity.ok(response);

            }else {

                response.setMessage(message.deletedError(Resources.QUESTIONS.getName()));
                response.setStatus(HttpStatus.NO_CONTENT);
                response.setTimestamp(timeUtils.getCurrentTime());
                return new ResponseEntity<>(response,HttpStatus.NO_CONTENT);

            }

        }catch (Exception ex){

            response.setMessage(ex.getMessage());
            response.setStatus(HttpStatus.BAD_REQUEST);
            response.setTimestamp(timeUtils.getCurrentTime());
            return new ResponseEntity<>(response,HttpStatus.BAD_REQUEST);

        }
    }

    /**********************************************************************
     * This method is update a question by id in question table of database
     **********************************************************************/

    @PutMapping("/{id}")
    public ResponseEntity<BaseAPIResponse<QuestionResponseDto>> update(@PathVariable("id") int id, @RequestBody QuestionResponseDto responseDto){

        BaseAPIResponse<QuestionResponseDto> response = new BaseAPIResponse<>();
        ModelMapper mapper = new ModelMapper();
        System.out.println("id:"+responseDto.getAnswerId());
        QuestionDto questionDto = mapper.map(responseDto, QuestionDto.class);


        try{
            if(responseDto.getAnswerId()==0){
                questionRepository.updateNoAnswer(id,questionDto);
                QuestionResponseDto requestDto1 = mapper.map(questionDto, QuestionResponseDto.class);
                response.setMessage(message.updated(Resources.QUESTIONS.getName()));
                response.setData(requestDto1);
                response.setStatus(HttpStatus.CREATED);
                response.setTimestamp(timeUtils.getCurrentTime());
                return ResponseEntity.ok(response);
            }
            else {
                if (questionServiceImp.update(id, questionDto)){

                    QuestionResponseDto requestDto1 = mapper.map(questionDto, QuestionResponseDto.class);
                    response.setMessage(message.updated(Resources.QUESTIONS.getName()));
                    response.setData(requestDto1);
                    response.setStatus(HttpStatus.CREATED);
                    response.setTimestamp(timeUtils.getCurrentTime());
                    return ResponseEntity.ok(response);

                }else {

                    response.setMessage(message.updatedError(Resources.QUESTIONS.getName()));
                    response.setStatus(HttpStatus.NO_CONTENT);
                    response.setTimestamp(timeUtils.getCurrentTime());
                    return new ResponseEntity<>(response,HttpStatus.NO_CONTENT);

                }
            }


        }catch (Exception e){

            response.setMessage(e.getMessage());
            response.setStatus(HttpStatus.BAD_REQUEST);
            response.setTimestamp(timeUtils.getCurrentTime());
            return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
        }

    }

}
