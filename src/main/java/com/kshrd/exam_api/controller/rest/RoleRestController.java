package com.kshrd.exam_api.controller.rest;

import com.kshrd.exam_api.controller.request.RoleRequestDto;
import com.kshrd.exam_api.controller.response.ResponseAPI;
import com.kshrd.exam_api.controller.response.RoleResponse;
import com.kshrd.exam_api.controller.response.TeacherResponse;
import com.kshrd.exam_api.controller.response.messages.BaseMessage;
import com.kshrd.exam_api.repository.model.RoleDto;
import com.kshrd.exam_api.service.serviceImplement.RoleServiceImp;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;


/**********************************************************************
 * Original Author:
 * Created Date: 21-07-2020
 * Development Group: HRD_EXAM
 * Description:
 **********************************************************************/

@RestController
@RequestMapping("${baseUrl}/roles")
public class RoleRestController {
    private RoleServiceImp roleServiceImp;
    ModelMapper mapper = new ModelMapper();
    @Autowired
    public void setRoleServiceImp(RoleServiceImp roleServiceImp) {
        this.roleServiceImp = roleServiceImp;
    }


    /**********************************************************************
     * Original Author:
     * Created Date: 21-07-2020
     * Description:
     * Modification History :
     *     Date:
     *  Developer:Lon Dara
     *  TODO:
     **********************************************************************/

    /**********************************************************************
     *
     **********************************************************************/
   @GetMapping()
    public ResponseEntity<ResponseAPI<List<RoleResponse>>> findAll() {
        List<RoleResponse> roleDto = roleServiceImp.findAll();
        ResponseAPI<List<RoleResponse>> responseAPI = new ResponseAPI<>();
        responseAPI.setMessage(BaseMessage.Success.SELECT_ALL_RECORD_SUCCESS.getMessage());
        responseAPI.setStatus(HttpStatus.OK);
        responseAPI.setData(roleDto);
        return ResponseEntity.ok(responseAPI);
    }


    /**********************************************************************
     * Used to get Role by ID
     **********************************************************************/
    @GetMapping("/{id}")
    public ResponseEntity<ResponseAPI<RoleResponse>> findOne(@PathVariable int id) {
        ResponseAPI<RoleResponse> responseAPI = new ResponseAPI<>();
        RoleResponse roleDto = mapper.map(roleServiceImp.findOne(id),RoleResponse.class);
        try {
            responseAPI.setMessage(BaseMessage.Success.SELECT_ONE_RECORD_SUCCESS.getMessage());
            responseAPI.setStatus(HttpStatus.OK);
            responseAPI.setData(roleDto);
        }catch (Exception ex){
            responseAPI.setMessage(BaseMessage.Error.SELECT_ERROR.getMessage());
            responseAPI.setStatus(HttpStatus.CONFLICT);
            responseAPI.setData(null);
        }
        return ResponseEntity.ok(responseAPI);
    }

    /**********************************************************************
     * Upload new Role in database
     **********************************************************************/
    @PostMapping()
    public ResponseEntity<ResponseAPI<RoleDto>> insertRole(@RequestBody RoleRequestDto requestModel) {
        ResponseAPI<RoleDto> response = new ResponseAPI<>();
        try {
            RoleDto roleDto = mapper.map(requestModel, RoleDto.class);
            if(roleDto.getRole().equalsIgnoreCase("")){
                response.setStatus(HttpStatus.NOT_FOUND);
                response.setData(null);
                response.setMessage(BaseMessage.Error.INSERT_ERROR.getMessage());
            }
            else{
                roleDto.setRole(roleDto.getRole().trim());
                roleServiceImp.insertRole(roleDto);
                int id=roleServiceImp.selectRoleId();
                response.setStatus(HttpStatus.OK);
                roleDto.setRoleId(id);
                response.setData(roleDto);
                response.setMessage(BaseMessage.Success.INSERT_SUCCESS.getMessage());
            }

        }catch (Exception ex) {
            response.setMessage(BaseMessage.Error.INSERT_ERROR.getMessage());
            response.setStatus(HttpStatus.CONFLICT);
            response.setData(null);
        }
        return ResponseEntity.ok(response);
    }


    /**********************************************************************
     *Update Role in database
     **********************************************************************/
    @PutMapping("/{id}")
    public ResponseEntity<ResponseAPI<RoleDto>> update(@PathVariable int id, @RequestBody RoleRequestDto roleDto) {
        ResponseAPI<RoleDto> responseAPI = new ResponseAPI<>();
        try {
            RoleDto res = mapper.map(roleDto, RoleDto.class);
            res.setRoleId(id);
            System.out.println(roleDto);
            roleServiceImp.updateRole(id, res);
            responseAPI.setMessage(BaseMessage.Success.UPDATE_SUCCESS.getMessage());
            responseAPI.setStatus(HttpStatus.OK);
            responseAPI.setData(res);
        }catch (Exception ex){
            responseAPI.setMessage(ex.getCause().getMessage());
            responseAPI.setStatus(HttpStatus.CONFLICT);
            responseAPI.setData(null);
        }

        return ResponseEntity.ok(responseAPI);
    }


    /**********************************************************************
     *Delete Role in database
     **********************************************************************/
    @DeleteMapping("/{id}")
    public ResponseEntity<ResponseAPI<RoleResponse>> delete(@PathVariable int id) {
        ResponseAPI<RoleResponse> res=new ResponseAPI<>();
        List<RoleResponse> list=roleServiceImp.findAll();
        String mess=null;
        for(int i=0;i<list.size();i++){
            if(id==list.get(i).getRoleId()){
                RoleResponse roleResponse=roleServiceImp.selectRoleById(id);
                roleServiceImp.deleteRole(id);
                res.setMessage(BaseMessage.Success.DELETE_SUCCESS.getMessage());
                res.setStatus(HttpStatus.OK);
                res.setData(roleResponse);
            }
            else {
                res.setMessage(BaseMessage.Error.DELETE_ERROR.getMessage());
                res.setStatus(HttpStatus.NOT_FOUND);
                res.setData(null);
            }
        }
        return ResponseEntity.ok(res);

    }

}