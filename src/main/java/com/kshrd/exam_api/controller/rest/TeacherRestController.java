package com.kshrd.exam_api.controller.rest;

import com.kshrd.exam_api.controller.request.TeacherRequestDto;
import com.kshrd.exam_api.controller.request.TeacherUpdateDto;
import com.kshrd.exam_api.controller.request.TeacherUpdatePasswordDto;
import com.kshrd.exam_api.controller.response.ResponseAPI;
import com.kshrd.exam_api.controller.response.TeacherResponse;
import com.kshrd.exam_api.controller.response.TeacherUpdateResponse;
import com.kshrd.exam_api.controller.response.messages.BaseMessage;
import com.kshrd.exam_api.controller.response.messages.MessageProperties;
import com.kshrd.exam_api.controller.response.messages.Resources;
import com.kshrd.exam_api.repository.model.TeacherDto;
import com.kshrd.exam_api.repository.rep.TeacherRepository;
import com.kshrd.exam_api.service.serviceImplement.TeacherServiceImp;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;
import java.util.regex.Pattern;

/**********************************************************************
 * Original Author:Sreang Seanghorn
 * Created Date: 21-07-2020
 * Development Group: HRD_EXAM
 * Description:
 **********************************************************************/
@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("${baseUrl}/teachers")
public class TeacherRestController {
    private TeacherServiceImp teacherServiceImp;
    ModelMapper mapper=new ModelMapper();
    private BCryptPasswordEncoder encoder;
    private MessageProperties messages;
    @Autowired
    private TeacherRepository teacherRepository;
    @Autowired
    public void setEncoder(BCryptPasswordEncoder encoder) {
        this.encoder = encoder;
    }

    @Autowired
    public void setTeacherServiceImp(TeacherServiceImp teacherServiceImp) {
        this.teacherServiceImp = teacherServiceImp;
    }

    @Autowired
    public void setMessages(MessageProperties messages) {
        this.messages = messages;
    }

    /**********************************************************************
     * Original Author:Sreang Seanghorn
     * Created Date: 21-07-2020
     * Description:
     * Modification History :
     *     Date:
     *  Developer:
     *  TODO:
     **********************************************************************/

    /**********************************************************************
     * select all teachers
     **********************************************************************/

    @GetMapping()
    public ResponseEntity<ResponseAPI<List<TeacherResponse>>> findAll(){
        ResponseAPI<List<TeacherResponse>> response=new ResponseAPI<>();
            List<TeacherResponse> teachers = teacherServiceImp.findAll();
            if(teachers.size() == 0){
                response.setMessage(messages.selectedError(Resources.TEACHERS.getName()));
                response.setStatus(HttpStatus.NOT_FOUND);
            }else{
                response.setMessage(messages.selected(Resources.TEACHERS.getName()));
                response.setStatus(HttpStatus.OK);
                response.setData(teachers);
            }
        return ResponseEntity.ok(response);
    }
    @PutMapping("updatepassword")
    public ResponseEntity<ResponseAPI<TeacherResponse>> updatePassword(@RequestBody TeacherUpdatePasswordDto updatePasswordDto){
        ResponseAPI<TeacherResponse> response=new ResponseAPI<>();
        int id=AuthenticationRestController.teacherId;
        System.out.println(id);
        TeacherResponse teacherResponse=teacherRepository.findOne(id);
        String password=teacherRepository.selectPassword(id);
        if(updatePasswordDto.getOldPassword().isEmpty()||updatePasswordDto.getNewPassword().isEmpty()){
            response.setMessage("new or old password cannot be blank");
            response.setStatus(HttpStatus.NOT_ACCEPTABLE);
        }
        else{
            if(encoder.matches(updatePasswordDto.getOldPassword(),password)){
                updatePasswordDto.setNewPassword(encoder.encode(updatePasswordDto.getNewPassword()));
                teacherServiceImp.updatePassword(id,updatePasswordDto);
                response.setData(teacherResponse);
                response.setMessage("Password has been updated successfully");
                response.setStatus(HttpStatus.OK);
            }
            else{
                response.setData(null);
                response.setMessage("The old password is in correct");
                response.setStatus(HttpStatus.NOT_ACCEPTABLE);
            }
        }

        return ResponseEntity.ok(response);
    }


    /**********************************************************************
     * get teacher by id
     **********************************************************************/
    @GetMapping("/{id}")
    public ResponseEntity<ResponseAPI<TeacherResponse>> findOne(@PathVariable int id){
        ResponseAPI<TeacherResponse> responseAPI=new ResponseAPI<>();
        try {
            if (id != 0){
                TeacherResponse teacher=teacherServiceImp.findOne(id);
                String password=teacherRepository.selectPassword(id);
               // System.out.println("password:"+encoder.matches(password));
                if (teacher != null){
                    responseAPI.setMessage(messages.selected(Resources.TEACHER.getName()));
                    responseAPI.setStatus(HttpStatus.OK);
                    responseAPI.setData(teacher);
                }else{
                    responseAPI.setMessage(messages.selectedError(Resources.TEACHER.getName()));
                    responseAPI.setStatus(HttpStatus.NOT_FOUND);
                }
            }else{
                responseAPI.setMessage(messages.nullValue("ID"));
                responseAPI.setStatus(HttpStatus.NOT_FOUND);

            }
        }catch (Exception ex){
            responseAPI.setMessage(BaseMessage.Error.SELECT_ERROR.getMessage());
            responseAPI.setStatus(HttpStatus.CONFLICT);
            responseAPI.setData(null);
        }
        return ResponseEntity.ok(responseAPI);
    }

    /**********************************************************************
     * add new a teacher
     **********************************************************************/
    @PostMapping()
    public ResponseEntity<ResponseAPI<TeacherRequestDto>> insert(@RequestBody TeacherRequestDto teacherRequestModel){
        ResponseAPI<TeacherRequestDto> response=new ResponseAPI<>();
        TeacherDto teacher=mapper.map(teacherRequestModel, TeacherDto.class);
        System.out.println("nme:"+teacherRequestModel.getName());
        boolean state=true;
        try {
            if (teacher.getName() == null || teacher.getName().isEmpty()){
                response.setMessage(messages.nullValue("Name"));
                response.setStatus(HttpStatus.BAD_REQUEST);
            }
            else{

              int len=teacherRequestModel.getName().length();
               for(int j=0;j<len;j++){
                   if (teacherRequestModel.getName().contains(" ")) {
                       state=false;
                      response.setMessage("Username cannot contains white space");
                      response.setStatus(HttpStatus.NOT_ACCEPTABLE);
                   }
               }
//

                List<String> username=teacherRepository.getUsername();
                System.out.println("name:"+teacherRequestModel.getName());
                for(int i=0;i<username.size();i++){
                    System.out.println(username.get(i));
                    if(teacherRequestModel.getName().equalsIgnoreCase(username.get(i))){
                        response.setMessage("cannot create duplicate username");
                        response.setData(null);
                        response.setStatus(HttpStatus.NOT_ACCEPTABLE);
                        state=false;
                    }
                }
                char ch;
                for(int k=0;k<teacherRequestModel.getName().length();k++){
                    ch=teacherRequestModel.getName().charAt(k);
                    if(Character.isUpperCase(ch)){
                        state=false;
                        response.setMessage("Username cannot contain Capital letters");
                    }
                }

                if(state==true){
                    teacher.setPassword(encoder.encode(teacher.getPassword()));
                    teacher.setStatus(true);
                    teacherServiceImp.insertTeacher(teacher);
                    response.setMessage(messages.inserted(Resources.TEACHERS.getName()));
                    response.setStatus(HttpStatus.OK);
                    response.setData(teacherRequestModel);
                }

            }

        }catch (Exception ex){
            response.setMessage(ex.getCause().getMessage());
            response.setStatus(HttpStatus.CONFLICT);
            response.setData(null);
        }
        return ResponseEntity.ok(response);
    }

    /**********************************************************************
     * update teacher by id
     **********************************************************************/
    @PutMapping("/{id}")
    public ResponseEntity<ResponseAPI<TeacherUpdateResponse>> update(@PathVariable int id, @RequestBody TeacherUpdateDto teacher) {
        ResponseAPI<TeacherUpdateResponse> res = new ResponseAPI<>();
        List<TeacherResponse> list = teacherServiceImp.findAll();
        Boolean isUpdated=false;
        for (int i = 0; i < list.size(); i++) {
            if (id == list.get(i).getId()) {
                teacher.setPassword(encoder.encode(teacher.getPassword()));
                teacherServiceImp.updateTeacher(id, teacher);
                isUpdated=true;
                res.setMessage(BaseMessage.Success.UPDATE_SUCCESS.getMessage());
                res.setStatus(HttpStatus.OK);
                TeacherUpdateResponse response = mapper.map(teacher, TeacherUpdateResponse.class);
                response.setTeacherId(id);
                res.setData(response);
            }

        }
        if(isUpdated==false){
            res.setMessage(BaseMessage.Error.UPDATE_ERROR.getMessage());
            res.setStatus(HttpStatus.CONFLICT);
            res.setData(null);
        }
        return ResponseEntity.ok(res);
    }

    /**********************************************************************
     * delete teacher by id
     **********************************************************************/
    @DeleteMapping("/{id}")
    public ResponseEntity<ResponseAPI<TeacherResponse>> delete(@PathVariable int id){
        ResponseAPI<TeacherResponse> responseAPI=new ResponseAPI<>();
       List<TeacherResponse> list=teacherServiceImp.findAll();
       Boolean isdelted=false;
       for(int i=0;i<list.size();i++){
            if(id==list.get(i).getId()){
                TeacherResponse t=teacherServiceImp.selectTeacherById(id);
                teacherRepository.updated(id);
                isdelted=true;
                //teacherServiceImp.deleteTeacher(id);
                responseAPI.setMessage(BaseMessage.Success.DELETE_SUCCESS.getMessage());
                responseAPI.setData(t);
                responseAPI.setStatus(HttpStatus.OK);
            }
        }
        if(isdelted==false){
            responseAPI.setMessage(BaseMessage.Error.DELETE_ERROR.getMessage());
            responseAPI.setData(null);
            responseAPI.setStatus(HttpStatus.NOT_FOUND);
        }
        return ResponseEntity.ok(responseAPI);

    }

}
